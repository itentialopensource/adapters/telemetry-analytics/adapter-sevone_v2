## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for SevOneV2. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for SevOneV2.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the SevOne v2. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getObjectAttachment(id, callback)</td>
    <td style="padding:15px">getObjectAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/objects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectAttachmentResources(id, callback)</td>
    <td style="padding:15px">getObjectAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/objects/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateObjectAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/objects/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getObjectAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/objects/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateObjectAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/objects/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getObjectAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/objects/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateObjectAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/objects/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateObjectAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateObjectAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/objects/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createObjectAttachment(id, body, callback)</td>
    <td style="padding:15px">createObjectAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTypes(page, size, includeCount, sortBy, fields, includeObjectTypes, callback)</td>
    <td style="padding:15px">getDeviceTypes</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceType(body, callback)</td>
    <td style="padding:15px">createDeviceType</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAncestorsForDeviceTypes(ids, callback)</td>
    <td style="padding:15px">getAncestorsForDeviceTypes</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes/ancestors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterDeviceTypes(includeDevices, includeObjectTypes, page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterDeviceTypes</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTypeForDeviceById(deviceId, sortBy, callback)</td>
    <td style="padding:15px">getDeviceTypeForDeviceById</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes/members/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTypeById(id, callback)</td>
    <td style="padding:15px">getDeviceTypeById</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTypeById(id, callback)</td>
    <td style="padding:15px">deleteDeviceTypeById</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAncestorsForDeviceType(id, callback)</td>
    <td style="padding:15px">getAncestorsForDeviceType</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes/{pathv1}/ancestors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMemberByIdToType(id, deviceId, callback)</td>
    <td style="padding:15px">addMemberByIdToType</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTypeMemberById(id, deviceId, callback)</td>
    <td style="padding:15px">deleteDeviceTypeMemberById</td>
    <td style="padding:15px">{base_path}/{version}/devicetypes/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterObjects(includeIndicators, includeExtendedInfo, page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterObjects</td>
    <td style="padding:15px">{base_path}/{version}/devices/objects/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjects(deviceId, includeIndicators, includeIndicatorMetadata, includeExtendedInfo, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getObjects</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createObject(deviceId, body, callback)</td>
    <td style="padding:15px">createObject</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectById(deviceId, id, includeIndicators, includeIndicatorMetadata, includeExtendedInfo, callback)</td>
    <td style="padding:15px">getObjectById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectById(deviceId, id, body, callback)</td>
    <td style="padding:15px">updateObjectById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectById(deviceId, id, callback)</td>
    <td style="padding:15px">deleteObjectById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateObjectById(deviceId, id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateObjectById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusAttachment(id, callback)</td>
    <td style="padding:15px">getStatusAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/statusmap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusMapAttachmentResources(id, callback)</td>
    <td style="padding:15px">getStatusMapAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/statusmap/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStatusMapAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateStatusMapAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/statusmap/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStatusMapAttachment(id, body, callback)</td>
    <td style="padding:15px">createStatusMapAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/statusmap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiKeys(callback)</td>
    <td style="padding:15px">getApiKeys</td>
    <td style="padding:15px">{base_path}/{version}/users/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiKey(body, callback)</td>
    <td style="padding:15px">createApiKey</td>
    <td style="padding:15px">{base_path}/{version}/users/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiKeys(callback)</td>
    <td style="padding:15px">deleteApiKeys</td>
    <td style="padding:15px">{base_path}/{version}/users/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApiKey(apiKey, body, callback)</td>
    <td style="padding:15px">updateApiKey</td>
    <td style="padding:15px">{base_path}/{version}/users/api-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiKey(apiKey, callback)</td>
    <td style="padding:15px">deleteApiKey</td>
    <td style="padding:15px">{base_path}/{version}/users/api-keys/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiKeysForUser(id, callback)</td>
    <td style="padding:15px">getApiKeysForUser</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiKeyForUser(id, body, callback)</td>
    <td style="padding:15px">createApiKeyForUser</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiKeysForUser(id, callback)</td>
    <td style="padding:15px">deleteApiKeysForUser</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/api-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApiKeyForUser(id, apiKey, body, callback)</td>
    <td style="padding:15px">updateApiKeyForUser</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/api-keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiKeyForUser(apiKey, id, callback)</td>
    <td style="padding:15px">deleteApiKeyForUser</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/api-keys/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesInDiscovery(includeMembers, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getDevicesInDiscovery</td>
    <td style="padding:15px">{base_path}/{version}/discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterDevicesInDiscovery(includeMembers, includeObjects, includeIndicators, page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterDevicesInDiscovery</td>
    <td style="padding:15px">{base_path}/{version}/discovery/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runDiscoveryAllDevices(callback)</td>
    <td style="padding:15px">runDiscoveryAllDevices</td>
    <td style="padding:15px">{base_path}/{version}/discovery/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceStatusById(id, includeMembers, callback)</td>
    <td style="padding:15px">getDeviceStatusById</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevicePriority(id, body, callback)</td>
    <td style="padding:15px">updateDevicePriority</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runDiscoverDevice(id, callback)</td>
    <td style="padding:15px">runDiscoverDevice</td>
    <td style="padding:15px">{base_path}/{version}/discovery/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNViews(name, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getTopNViews</td>
    <td style="padding:15px">{base_path}/{version}/topnviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTopNView(body, callback)</td>
    <td style="padding:15px">createTopNView</td>
    <td style="padding:15px">{base_path}/{version}/topnviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNView(id, callback)</td>
    <td style="padding:15px">getTopNView</td>
    <td style="padding:15px">{base_path}/{version}/topnviews/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopNViewById(id, body, callback)</td>
    <td style="padding:15px">updateTopNViewById</td>
    <td style="padding:15px">{base_path}/{version}/topnviews/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopNViewById(id, callback)</td>
    <td style="padding:15px">deleteTopNViewById</td>
    <td style="padding:15px">{base_path}/{version}/topnviews/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRoles(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getAllRoles</td>
    <td style="padding:15px">{base_path}/{version}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRole(body, callback)</td>
    <td style="padding:15px">createRole</td>
    <td style="padding:15px">{base_path}/{version}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterRoles(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterRoles</td>
    <td style="padding:15px">{base_path}/{version}/roles/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRole(id, callback)</td>
    <td style="padding:15px">getRole</td>
    <td style="padding:15px">{base_path}/{version}/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoleById(id, callback)</td>
    <td style="padding:15px">deleteRoleById</td>
    <td style="padding:15px">{base_path}/{version}/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRole(id, body, callback)</td>
    <td style="padding:15px">patchRole</td>
    <td style="padding:15px">{base_path}/{version}/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyAttachment(id, callback)</td>
    <td style="padding:15px">getTopologyAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyAttachmentFilters(id, callback)</td>
    <td style="padding:15px">getTopologyAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopologyAttachmentFilters(id, body, callback)</td>
    <td style="padding:15px">updateTopologyAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyAttachmentResources(id, callback)</td>
    <td style="padding:15px">getTopologyAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopologyAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateTopologyAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getTopologyAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopologyAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateTopologyAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partialUpdateTopologyAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">partialUpdateTopologyAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getTopologyAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopologyAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateTopologyAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTopologyAttachment(id, body, callback)</td>
    <td style="padding:15px">createTopologyAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentFilterSchema(callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentFilterSchema</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/filters/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachment(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentFilters(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentFilters(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentResources(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateFlowFalconAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateFlowFalconAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getFlowFalconAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlowFalconAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateFlowFalconAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateFlowFalconAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateFlowFalconAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFlowFalconAttachment(id, body, callback)</td>
    <td style="padding:15px">createFlowFalconAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/flow-falcon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNAttachment(id, callback)</td>
    <td style="padding:15px">getTopNAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNAttachmentResources(id, callback)</td>
    <td style="padding:15px">getTopNAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopNAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateTopNAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getTopNAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopNAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateTopNAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateTopNAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateTopNAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getTopNAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopNAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateTopNAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopNAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getTopNAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopNAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateTopNAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateTopNAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateTopNAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTopNAttachment(id, body, callback)</td>
    <td style="padding:15px">createTopNAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/topn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkHours(page, size, includeCount, sortBy, fields, name, callback)</td>
    <td style="padding:15px">getWorkHours</td>
    <td style="padding:15px">{base_path}/{version}/workhours?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWorkHours(body, callback)</td>
    <td style="padding:15px">createWorkHours</td>
    <td style="padding:15px">{base_path}/{version}/workhours?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkHoursByGroup(id, callback)</td>
    <td style="padding:15px">getWorkHoursByGroup</td>
    <td style="padding:15px">{base_path}/{version}/workhours/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWorkHoursByGroupId(id, body, callback)</td>
    <td style="padding:15px">updateWorkHoursByGroupId</td>
    <td style="padding:15px">{base_path}/{version}/workhours/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkHoursGroupById(id, callback)</td>
    <td style="padding:15px">deleteWorkHoursGroupById</td>
    <td style="padding:15px">{base_path}/{version}/workhours/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPeersUsingGET(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getPeersUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/peers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterSettings(filter, callback)</td>
    <td style="padding:15px">getClusterSettings</td>
    <td style="padding:15px">{base_path}/{version}/peers/clusterSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentPeerUsingGET(callback)</td>
    <td style="padding:15px">getCurrentPeerUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/peers/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncorporateModeUsingGET(callback)</td>
    <td style="padding:15px">getIncorporateModeUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/peers/incorporateMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editIncorporateModeUsingPATCH(body, callback)</td>
    <td style="padding:15px">editIncorporateModeUsingPATCH</td>
    <td style="padding:15px">{base_path}/{version}/peers/incorporateMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPeerUsingGET(id, callback)</td>
    <td style="padding:15px">getPeerUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/peers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettings(id, filter, callback)</td>
    <td style="padding:15px">getSettings</td>
    <td style="padding:15px">{base_path}/{version}/peers/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholds(type = 'other', page, size, includeCount, callback)</td>
    <td style="padding:15px">getThresholds</td>
    <td style="padding:15px">{base_path}/{version}/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createThreshold(deviceId, body, callback)</td>
    <td style="padding:15px">createThreshold</td>
    <td style="padding:15px">{base_path}/{version}/thresholds/flow/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholdById(deviceId, thresholdId, callback)</td>
    <td style="padding:15px">getThresholdById</td>
    <td style="padding:15px">{base_path}/{version}/thresholds/flow/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateThreshold(deviceId, thresholdId, body, callback)</td>
    <td style="padding:15px">updateThreshold</td>
    <td style="padding:15px">{base_path}/{version}/thresholds/flow/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThresholdById(deviceId, thresholdId, callback)</td>
    <td style="padding:15px">deleteThresholdById</td>
    <td style="padding:15px">{base_path}/{version}/thresholds/flow/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createThresholdCondition(deviceId, thresholdId, triggerType = 'clear', body, callback)</td>
    <td style="padding:15px">createThresholdCondition</td>
    <td style="padding:15px">{base_path}/{version}/thresholds/flow/{pathv1}/{pathv2}/{pathv3}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateThresholdConditionById(deviceId, thresholdId, triggerType = 'clear', conditionId, body, callback)</td>
    <td style="padding:15px">updateThresholdConditionById</td>
    <td style="padding:15px">{base_path}/{version}/thresholds/flow/{pathv1}/{pathv2}/{pathv3}/conditions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThresholdConditionById(deviceId, thresholdId, triggerType = 'clear', conditionId, callback)</td>
    <td style="padding:15px">deleteThresholdConditionById</td>
    <td style="padding:15px">{base_path}/{version}/thresholds/flow/{pathv1}/{pathv2}/{pathv3}/conditions/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportAttachment(id, callback)</td>
    <td style="padding:15px">getReportAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportAttachmentById(id, body, callback)</td>
    <td style="padding:15px">updateReportAttachmentById</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportAttachment(id, callback)</td>
    <td style="padding:15px">deleteReportAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllReportAttachmentEndpoints(type = 'Device', callback)</td>
    <td style="padding:15px">getAllReportAttachmentEndpoints</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllReportAttachments(id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getAllReportAttachments</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupsRules(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getObjectGroupsRules</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createObjectGroupRule(body, callback)</td>
    <td style="padding:15px">createObjectGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupRule(id, callback)</td>
    <td style="padding:15px">getObjectGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectGroupRule(id, body, callback)</td>
    <td style="padding:15px">updateObjectGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectGroupRule(id, callback)</td>
    <td style="padding:15px">deleteObjectGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupRules(id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getObjectGroupRules</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyObjectGroupRules(id, callback)</td>
    <td style="padding:15px">applyObjectGroupRules</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}/rules/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsers(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getAllUsers</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserUsingPOST(body, callback)</td>
    <td style="padding:15px">createUserUsingPOST</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterUsers(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterUsers</td>
    <td style="padding:15px">{base_path}/{version}/users/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forcePasswordChange(body, callback)</td>
    <td style="padding:15px">forcePasswordChange</td>
    <td style="padding:15px">{base_path}/{version}/users/force-password-change?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentUser(callback)</td>
    <td style="padding:15px">getCurrentUser</td>
    <td style="padding:15px">{base_path}/{version}/users/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserPreferencesUsingPATCH(body, callback)</td>
    <td style="padding:15px">updateUserPreferencesUsingPATCH</td>
    <td style="padding:15px">{base_path}/{version}/users/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePassword(body, callback)</td>
    <td style="padding:15px">changePassword</td>
    <td style="padding:15px">{base_path}/{version}/users/mypreferences/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserRoles(callback)</td>
    <td style="padding:15px">getUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/users/myroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserById(id, callback)</td>
    <td style="padding:15px">getUserById</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserUsingDELETE(id, callback)</td>
    <td style="padding:15px">deleteUserUsingDELETE</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateUserUsingPATCH(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateUserUsingPATCH</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpecificUserRoles(id, callback)</td>
    <td style="padding:15px">getSpecificUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeSpecificUserRoles(id, body, callback)</td>
    <td style="padding:15px">removeSpecificUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSpecificUserRoles(id, body, callback)</td>
    <td style="padding:15px">addSpecificUserRoles</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNamespaces(name, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getNamespaces</td>
    <td style="padding:15px">{base_path}/{version}/metadata/namespace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNamespace(body, callback)</td>
    <td style="padding:15px">createNamespace</td>
    <td style="padding:15px">{base_path}/{version}/metadata/namespace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNamespace(id, callback)</td>
    <td style="padding:15px">getNamespace</td>
    <td style="padding:15px">{base_path}/{version}/metadata/namespace/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNamespaceById(id, body, callback)</td>
    <td style="padding:15px">updateNamespaceById</td>
    <td style="padding:15px">{base_path}/{version}/metadata/namespace/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNamespaceById(id, callback)</td>
    <td style="padding:15px">deleteNamespaceById</td>
    <td style="padding:15px">{base_path}/{version}/metadata/namespace/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupAttachment(id, callback)</td>
    <td style="padding:15px">getObjectGroupAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/object-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupAttachmentResources(id, callback)</td>
    <td style="padding:15px">getObjectGroupAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/object-groups/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectGroupAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateObjectGroupAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/object-groups/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getObjectGroupAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/object-groups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectGroupAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateObjectGroupAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/object-groups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateObjectGroupAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateObjectGroupAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/object-groups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createObjectGroupAttachment(id, body, callback)</td>
    <td style="padding:15px">createObjectGroupAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/object-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusMaps(page, size, includeCount, sortBy, fields, includeMembers, callback)</td>
    <td style="padding:15px">getStatusMaps</td>
    <td style="padding:15px">{base_path}/{version}/maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStatusMap(body, callback)</td>
    <td style="padding:15px">createStatusMap</td>
    <td style="padding:15px">{base_path}/{version}/maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusMapById(id, includeMembers, callback)</td>
    <td style="padding:15px">getStatusMapById</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStatusMapById(id, body, callback)</td>
    <td style="padding:15px">updateStatusMapById</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStatusMapById(id, callback)</td>
    <td style="padding:15px">deleteStatusMapById</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateStatusMapById(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateStatusMapById</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectionAlerts(id, connectionId, callback)</td>
    <td style="padding:15px">getConnectionAlerts</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connection/{pathv2}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnections(id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getConnections</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConnection(id, body, callback)</td>
    <td style="padding:15px">createConnection</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConnectionByStatusMapId(id, connectionId, body, callback)</td>
    <td style="padding:15px">updateConnectionByStatusMapId</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connections/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConnectionById(id, connectionId, callback)</td>
    <td style="padding:15px">deleteConnectionById</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/connections/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeAlerts(id, nodeId, callback)</td>
    <td style="padding:15px">getNodeAlerts</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/node/{pathv2}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusMapNodes(id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getStatusMapNodes</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNode(id, body, callback)</td>
    <td style="padding:15px">createNode</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNodeById(id, nodeId, body, callback)</td>
    <td style="padding:15px">updateNodeById</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodeById(id, nodeId, callback)</td>
    <td style="padding:15px">deleteNodeById</td>
    <td style="padding:15px">{base_path}/{version}/maps/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceIndicatorData(body, callback)</td>
    <td style="padding:15px">createDeviceIndicatorData</td>
    <td style="padding:15px">{base_path}/{version}/device-indicators/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicators(deviceId, objectId, includeExtendedInfo, includeIndicatorMetadata, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getIndicators</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}/indicators?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicator(deviceId, objectId, indicatorId, includeExtendedInfo, callback)</td>
    <td style="padding:15px">getIndicator</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}/indicators/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicatorData(deviceId, objectId, indicatorId, startTime, endTime, rawData, callback)</td>
    <td style="padding:15px">getIndicatorData</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}/indicators/{pathv3}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachmentFilterSchema(callback)</td>
    <td style="padding:15px">getDeviceAttachmentFilterSchema</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/filters/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachment(id, callback)</td>
    <td style="padding:15px">getDeviceAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachmentFilters(id, callback)</td>
    <td style="padding:15px">getDeviceAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceAttachmentFilters(id, body, callback)</td>
    <td style="padding:15px">updateDeviceAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiV2ReportsAttachmentsDevicesIdResources(id, callback)</td>
    <td style="padding:15px">getDeviceAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceAttachment(id, body, callback)</td>
    <td style="padding:15px">updateDeviceAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getDeviceAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateDeviceAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAttachmentVisualization(id, callback)</td>
    <td style="padding:15px">getDeviceAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceAttachmentVisualization(id, body, callback)</td>
    <td style="padding:15px">updateDeviceAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devices/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceAttachment(id, body, callback)</td>
    <td style="padding:15px">createDeviceAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnitInfos(callback)</td>
    <td style="padding:15px">getUnitInfos</td>
    <td style="padding:15px">{base_path}/{version}/utils/unitinfos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessPermissions(roleId, callback)</td>
    <td style="padding:15px">getAccessPermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessPermissions(roleId, body, callback)</td>
    <td style="padding:15px">updateAccessPermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupPermissions(roleId, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">updateDeviceGroupPermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/devicegroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiV2PermissionsRoleIdDevicegroupId(roleId, id, callback)</td>
    <td style="padding:15px">updateDeviceGroupPermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/devicegroup/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApiV2PermissionsRoleIdDevicegroupId(roleId, id, body, callback)</td>
    <td style="padding:15px">updateDeviceGroupPermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/devicegroup/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPagePermissions(roleId, callback)</td>
    <td style="padding:15px">getPagePermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePagePermissions(roleId, body, callback)</td>
    <td style="padding:15px">updatePagePermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRolePermissions(roleId, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getRolePermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiV2PermissionsRoleIdRoleTargetRoleId(roleId, targetRoleId, callback)</td>
    <td style="padding:15px">getRolePermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putApiV2PermissionsRoleIdRoleTargetRoleId(roleId, targetRoleId, body, callback)</td>
    <td style="padding:15px">getRolePermissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/{pathv1}/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupAttachment(id, callback)</td>
    <td style="padding:15px">getDeviceGroupAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupsAttachment(id, callback)</td>
    <td style="padding:15px">getDeviceGroupsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devicegroups/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupsAttachment(id, body, callback)</td>
    <td style="padding:15px">updateDeviceGroupsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devicegroups/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupsAttachmentVisualization(id, callback)</td>
    <td style="padding:15px">getDeviceGroupsAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devicegroups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupsAttachmentVisualization(id, body, callback)</td>
    <td style="padding:15px">updateDeviceGroupsAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/devicegroups/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceGroupsAttachment(id, body, callback)</td>
    <td style="padding:15px">createDeviceGroupsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceMappings(page, size, includeCount, sortBy, fields, includeAutomatic, callback)</td>
    <td style="padding:15px">getDeviceMappings</td>
    <td style="padding:15px">{base_path}/{version}/netflow/deviceMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceMappings(body, callback)</td>
    <td style="padding:15px">createDeviceMappings</td>
    <td style="padding:15px">{base_path}/{version}/netflow/deviceMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceMappingById(mappingId, callback)</td>
    <td style="padding:15px">deleteDeviceMappingById</td>
    <td style="padding:15px">{base_path}/{version}/netflow/deviceMappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceMappings(mappingId, body, callback)</td>
    <td style="padding:15px">updateDeviceMappings</td>
    <td style="padding:15px">{base_path}/{version}/netflow/deviceMappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetFlowDevices(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getNetFlowDevices</td>
    <td style="padding:15px">{base_path}/{version}/netflow/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterNetFlowDevices(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterNetFlowDevices</td>
    <td style="padding:15px">{base_path}/{version}/netflow/devices/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterNetFlowDeviceInterfaces(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterNetFlowDeviceInterfaces</td>
    <td style="padding:15px">{base_path}/{version}/netflow/devices/interfaces/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaces(netflowDeviceId, callback)</td>
    <td style="padding:15px">getInterfaces</td>
    <td style="padding:15px">{base_path}/{version}/netflow/devices/{pathv1}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirections(netflowDeviceId, interfaceId, callback)</td>
    <td style="padding:15px">getDirections</td>
    <td style="padding:15px">{base_path}/{version}/netflow/devices/{pathv1}/interfaces/{pathv2}/directions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInterface(netflowDeviceId, netFlowInterfaceId, callback)</td>
    <td style="padding:15px">deleteInterface</td>
    <td style="padding:15px">{base_path}/{version}/netflow/devices/{pathv1}/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInterface(netflowDeviceId, netFlowInterfaceId, body, callback)</td>
    <td style="padding:15px">updateInterface</td>
    <td style="padding:15px">{base_path}/{version}/netflow/devices/{pathv1}/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetflowFields(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getNetflowFields</td>
    <td style="padding:15px">{base_path}/{version}/netflow/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterNetflowFields(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterNetflowFields</td>
    <td style="padding:15px">{base_path}/{version}/netflow/fields/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilters(callback)</td>
    <td style="padding:15px">getFilters</td>
    <td style="padding:15px">{base_path}/{version}/netflow/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFilter(body, callback)</td>
    <td style="padding:15px">createFilter</td>
    <td style="padding:15px">{base_path}/{version}/netflow/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilterById(id, callback)</td>
    <td style="padding:15px">getFilterById</td>
    <td style="padding:15px">{base_path}/{version}/netflow/filters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFilter(id, callback)</td>
    <td style="padding:15px">deleteFilter</td>
    <td style="padding:15px">{base_path}/{version}/netflow/filters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilterEntitiesById(id, callback)</td>
    <td style="padding:15px">getFilterEntitiesById</td>
    <td style="padding:15px">{base_path}/{version}/netflow/filters/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFilterEntities(id, body, callback)</td>
    <td style="padding:15px">createFilterEntities</td>
    <td style="padding:15px">{base_path}/{version}/netflow/filters/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFilterEntity(id, ruleId, callback)</td>
    <td style="padding:15px">deleteFilterEntity</td>
    <td style="padding:15px">{base_path}/{version}/netflow/filters/{pathv1}/rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMappings(page, size, includeCount, sortBy, fields, includeAutomatic, callback)</td>
    <td style="padding:15px">getMappings</td>
    <td style="padding:15px">{base_path}/{version}/netflow/objectMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMappings(body, callback)</td>
    <td style="padding:15px">createMappings</td>
    <td style="padding:15px">{base_path}/{version}/netflow/objectMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMappingByIndicator(page, size, includeCount, sortBy, fields, includeAutomatic, body, callback)</td>
    <td style="padding:15px">getMappingByIndicator</td>
    <td style="padding:15px">{base_path}/{version}/netflow/objectMappings/filterByIndicator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMappingByInterfaces(page, size, includeCount, sortBy, fields, includeAutomatic, body, callback)</td>
    <td style="padding:15px">getMappingByInterfaces</td>
    <td style="padding:15px">{base_path}/{version}/netflow/objectMappings/filterByInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectMappingById(id, callback)</td>
    <td style="padding:15px">deleteObjectMappingById</td>
    <td style="padding:15px">{base_path}/{version}/netflow/objectMappings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProtocols(callback)</td>
    <td style="padding:15px">getProtocols</td>
    <td style="padding:15px">{base_path}/{version}/netflow/protocols?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSegments(callback)</td>
    <td style="padding:15px">getNetworkSegments</td>
    <td style="padding:15px">{base_path}/{version}/netflow/segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubnetCategory(body, callback)</td>
    <td style="padding:15px">createSubnetCategory</td>
    <td style="padding:15px">{base_path}/{version}/netflow/segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnets(callback)</td>
    <td style="padding:15px">getSubnets</td>
    <td style="padding:15px">{base_path}/{version}/netflow/segments/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletSubnetById(id, callback)</td>
    <td style="padding:15px">deletSubnetById</td>
    <td style="padding:15px">{base_path}/{version}/netflow/segments/subnets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetCategoryById(id, callback)</td>
    <td style="padding:15px">getSubnetCategoryById</td>
    <td style="padding:15px">{base_path}/{version}/netflow/segments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletSubnetCategoryById(id, callback)</td>
    <td style="padding:15px">deletSubnetCategoryById</td>
    <td style="padding:15px">{base_path}/{version}/netflow/segments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetsByCategoryId(id, callback)</td>
    <td style="padding:15px">getSubnetsByCategoryId</td>
    <td style="padding:15px">{base_path}/{version}/netflow/segments/{pathv1}/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubnet(id, body, callback)</td>
    <td style="padding:15px">createSubnet</td>
    <td style="padding:15px">{base_path}/{version}/netflow/segments/{pathv1}/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesByPort(callback)</td>
    <td style="padding:15px">getServicesByPort</td>
    <td style="padding:15px">{base_path}/{version}/netflow/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplication(body, callback)</td>
    <td style="padding:15px">createApplication</td>
    <td style="padding:15px">{base_path}/{version}/netflow/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplication(id, callback)</td>
    <td style="padding:15px">deleteApplication</td>
    <td style="padding:15px">{base_path}/{version}/netflow/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplication(id, body, callback)</td>
    <td style="padding:15px">updateApplication</td>
    <td style="padding:15px">{base_path}/{version}/netflow/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetFlowModes(callback)</td>
    <td style="padding:15px">getNetFlowModes</td>
    <td style="padding:15px">{base_path}/{version}/netflow/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViews(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getViews</td>
    <td style="padding:15px">{base_path}/{version}/netflow/views?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetflowCategories(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getNetflowCategories</td>
    <td style="padding:15px">{base_path}/{version}/netflow/views/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterNetflowViews(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterNetflowViews</td>
    <td style="padding:15px">{base_path}/{version}/netflow/views/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViewIndicators(viewId, includeMembers, callback)</td>
    <td style="padding:15px">getViewIndicators</td>
    <td style="padding:15px">{base_path}/{version}/netflow/views/{pathv1}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterViewIndicators(viewId, page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterViewIndicators</td>
    <td style="padding:15px">{base_path}/{version}/netflow/views/{pathv1}/fields/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportColumnsUsingGET(viewId, callback)</td>
    <td style="padding:15px">getReportColumnsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/netflow/views/{pathv1}/reportColumns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenanceWindows(page, size, sortBy, callback)</td>
    <td style="padding:15px">getMaintenanceWindows</td>
    <td style="padding:15px">{base_path}/{version}/maintenancewindows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMaintenanceWindow(body, callback)</td>
    <td style="padding:15px">createMaintenanceWindow</td>
    <td style="padding:15px">{base_path}/{version}/maintenancewindows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiV2MaintenancewindowsDeviceGroup(body, callback)</td>
    <td style="padding:15px">createMaintenanceWindow</td>
    <td style="padding:15px">{base_path}/{version}/maintenancewindows/device-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupMaintenanceWindowUsingPUT(id, body, callback)</td>
    <td style="padding:15px">updateGroupMaintenanceWindowUsingPUT</td>
    <td style="padding:15px">{base_path}/{version}/maintenancewindows/device-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterMaintenanceWindows(page, size, sortBy, body, callback)</td>
    <td style="padding:15px">filterMaintenanceWindows</td>
    <td style="padding:15px">{base_path}/{version}/maintenancewindows/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApiV2MaintenancewindowsFilterall(page, size, sortBy, body, callback)</td>
    <td style="padding:15px">filterMaintenanceWindows</td>
    <td style="padding:15px">{base_path}/{version}/maintenancewindows/filterall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenanceWindowById(id, callback)</td>
    <td style="padding:15px">getMaintenanceWindowById</td>
    <td style="padding:15px">{base_path}/{version}/maintenancewindows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMaintenanceWindowById(id, body, callback)</td>
    <td style="padding:15px">updateMaintenanceWindowById</td>
    <td style="padding:15px">{base_path}/{version}/maintenancewindows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMaintenanceWindowById(id, callback)</td>
    <td style="padding:15px">deleteMaintenanceWindowById</td>
    <td style="padding:15px">{base_path}/{version}/maintenancewindows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetadataAttachment(id, callback)</td>
    <td style="padding:15px">getMetadataAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetadataAttachmentResources(id, callback)</td>
    <td style="padding:15px">getMetadataAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/metadata/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMetadataAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updateMetadataAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/metadata/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetadataAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getMetadataAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/metadata/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMetadataAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateMetadataAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/metadata/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateMetadataAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateMetadataAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/metadata/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMetadataAttachment(id, body, callback)</td>
    <td style="padding:15px">createMetadataAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttributes(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getAttributes</td>
    <td style="padding:15px">{base_path}/{version}/metadata/attribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAttribute(body, callback)</td>
    <td style="padding:15px">createAttribute</td>
    <td style="padding:15px">{base_path}/{version}/metadata/attribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterAttributes(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterAttributes</td>
    <td style="padding:15px">{base_path}/{version}/metadata/attribute/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttribute(id, callback)</td>
    <td style="padding:15px">getAttribute</td>
    <td style="padding:15px">{base_path}/{version}/metadata/attribute/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAttributeById(id, body, callback)</td>
    <td style="padding:15px">updateAttributeById</td>
    <td style="padding:15px">{base_path}/{version}/metadata/attribute/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAttributeById(id, callback)</td>
    <td style="padding:15px">deleteAttributeById</td>
    <td style="padding:15px">{base_path}/{version}/metadata/attribute/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConstraints(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getConstraints</td>
    <td style="padding:15px">{base_path}/{version}/topology/constraints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConstraint(body, callback)</td>
    <td style="padding:15px">createConstraint</td>
    <td style="padding:15px">{base_path}/{version}/topology/constraints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConstraint(id, callback)</td>
    <td style="padding:15px">getConstraint</td>
    <td style="padding:15px">{base_path}/{version}/topology/constraints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConstraintById(id, body, callback)</td>
    <td style="padding:15px">updateConstraintById</td>
    <td style="padding:15px">{base_path}/{version}/topology/constraints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConstraintById(id, callback)</td>
    <td style="padding:15px">deleteConstraintById</td>
    <td style="padding:15px">{base_path}/{version}/topology/constraints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyFullLinkByDeviceId(deviceId, callback)</td>
    <td style="padding:15px">getTopologyFullLinkByDeviceId</td>
    <td style="padding:15px">{base_path}/{version}/topology/devices/{pathv1}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterTopologyLinks(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterTopologyLinks</td>
    <td style="padding:15px">{base_path}/{version}/topology/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTopologyLink(body, callback)</td>
    <td style="padding:15px">createTopologyLink</td>
    <td style="padding:15px">{base_path}/{version}/topology/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyFullLink(connectionId, callback)</td>
    <td style="padding:15px">getTopologyFullLink</td>
    <td style="padding:15px">{base_path}/{version}/topology/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyFullLink(connectionId, callback)</td>
    <td style="padding:15px">deleteTopologyFullLink</td>
    <td style="padding:15px">{base_path}/{version}/topology/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyHalfLink(connectionId, deviceId, callback)</td>
    <td style="padding:15px">deleteTopologyHalfLink</td>
    <td style="padding:15px">{base_path}/{version}/topology/links/{pathv1}/halflinks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runAlertReport(body, callback)</td>
    <td style="padding:15px">runAlertReport</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runAlertAttachment(id, callback)</td>
    <td style="padding:15px">runAlertAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFlowFalconPM(body, callback)</td>
    <td style="padding:15px">createFlowFalconPM</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/performance-metrics/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runFlowFalconReport(body, callback)</td>
    <td style="padding:15px">runFlowFalconReport</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFlowFalconTopN(body, callback)</td>
    <td style="padding:15px">createFlowFalconTopN</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/topn/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runFlowFalconAttachment(id, callback)</td>
    <td style="padding:15px">runFlowFalconAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/flow-falcon/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runGroupMetricsAttachment(body, callback)</td>
    <td style="padding:15px">runGroupMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runGroupMetricsAttachmentById(id, callback)</td>
    <td style="padding:15px">runGroupMetricsAttachmentById</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runPerformanceMetricsAttachment(skipNullPoints, graphWidth, maxIndicators, body, callback)</td>
    <td style="padding:15px">runPerformanceMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runPerformanceMetricsAttachmentById(id, skipNullPoints, graphWidth, maxIndicators, callback)</td>
    <td style="padding:15px">runPerformanceMetricsAttachmentById</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runTopNAttachment(excludeChildIndicatorTypes, body, callback)</td>
    <td style="padding:15px">runTopNAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiV2ReportsAttachmentsTopnIdRun(id, excludeChildIndicatorTypes, callback)</td>
    <td style="padding:15px">runTopNAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topn/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runTopologyReport(includeLayout, includeDeviceData, body, callback)</td>
    <td style="padding:15px">runTopologyReport</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runTopologyAttachment(id, includeLayout, includeDeviceData, callback)</td>
    <td style="padding:15px">runTopologyAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/topology/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformanceMetricsAttachment(id, callback)</td>
    <td style="padding:15px">getPerformanceMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformanceMetricsAttachmentResources(id, callback)</td>
    <td style="padding:15px">getPerformanceMetricsAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePerformanceMetricsAttachmentResources(id, body, callback)</td>
    <td style="padding:15px">updatePerformanceMetricsAttachmentResources</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformanceMetricsAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getPerformanceMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePerformanceMetricsAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updatePerformanceMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdatePerformanceMetricsAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdatePerformanceMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformanceMetricsAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getPerformanceMetricsAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePerformanceMetricsAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updatePerformanceMetricsAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPerformanceMetricsAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getPerformanceMetricsAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePerformanceMetricsAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updatePerformanceMetricsAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/performance-metrics/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPerformanceMetricsAttachment(id, body, callback)</td>
    <td style="padding:15px">createPerformanceMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/performance-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupMetricsAttachment(id, callback)</td>
    <td style="padding:15px">getGroupMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiV2ReportsAttachmentsGroupMetricsIdResources(id, callback)</td>
    <td style="padding:15px">getGroupMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupMetricsAttachment(id, body, callback)</td>
    <td style="padding:15px">updateGroupMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupMetricsAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getGroupMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupMetricsAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateGroupMetricsAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupMetricsAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getGroupMetricsAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupMetricsAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateGroupMetricsAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupMetricsAttachmentVisualization(id, callback)</td>
    <td style="padding:15px">getGroupMetricsAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}/visualization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupMetricsAttachmentVisualization(id, body, callback)</td>
    <td style="padding:15px">updateGroupMetricsAttachmentVisualization</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/group-metrics/{pathv1}/visualization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroupMetricsAttachment(id, body, callback)</td>
    <td style="padding:15px">createGroupMetricsAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/group-metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveSessions(token, callback)</td>
    <td style="padding:15px">getActiveSessions</td>
    <td style="padding:15px">{base_path}/{version}/authentication/active-sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keepAlive(callback)</td>
    <td style="padding:15px">keepAlive</td>
    <td style="padding:15px">{base_path}/{version}/authentication/keep-alive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signIn(nmsLogin, body, callback)</td>
    <td style="padding:15px">signIn</td>
    <td style="padding:15px">{base_path}/{version}/authentication/signin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signOut(callback)</td>
    <td style="padding:15px">signOut</td>
    <td style="padding:15px">{base_path}/{version}/authentication/signout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signOutOthers(callback)</td>
    <td style="padding:15px">signOutOthers</td>
    <td style="padding:15px">{base_path}/{version}/authentication/signout-others?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">signOutUser(userId, callback)</td>
    <td style="padding:15px">signOutUser</td>
    <td style="padding:15px">{base_path}/{version}/authentication/signout/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTags(name, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getTags</td>
    <td style="padding:15px">{base_path}/{version}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiV2TagsNameIndicatortypes(name, page, size, callback)</td>
    <td style="padding:15px">getTags</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}/indicatortypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroups(includeMembers, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getDeviceGroups</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceGroup(body, callback)</td>
    <td style="padding:15px">createDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAncestorsForDeviceGroups(ids, callback)</td>
    <td style="padding:15px">getAncestorsForDeviceGroups</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/ancestors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterDeviceGroup(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupById(id, includeMembers, callback)</td>
    <td style="padding:15px">getDeviceGroupById</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupById(id, body, callback)</td>
    <td style="padding:15px">updateDeviceGroupById</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroupById(id, callback)</td>
    <td style="padding:15px">deleteDeviceGroupById</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateDeviceGroupById(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateDeviceGroupById</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAncestorsForDeviceGroup(id, callback)</td>
    <td style="padding:15px">getAncestorsForDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/ancestors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMemberByIdToGroup(id, deviceId, callback)</td>
    <td style="padding:15px">addMemberByIdToGroup</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroupMemberById(id, deviceId, callback)</td>
    <td style="padding:15px">deleteDeviceGroupMemberById</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTagsById(id, callback)</td>
    <td style="padding:15px">getDeviceTagsById</td>
    <td style="padding:15px">{base_path}/{version}/devicetags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectMetadataValue(deviceId, objectId, callback)</td>
    <td style="padding:15px">getObjectMetadataValue</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMetadata(deviceId, objectId, body, callback)</td>
    <td style="padding:15px">updateMetadata</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partialUpdateMetadata(deviceId, objectId, body, callback)</td>
    <td style="padding:15px">partialUpdateMetadata</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMetadataByAttribute(deviceId, objectId, attributeKey, body, callback)</td>
    <td style="padding:15px">updateMetadataByAttribute</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/objects/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetadataValue(entityType = 'devices', entityId, callback)</td>
    <td style="padding:15px">getMetadataValue</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putApiV2EntityTypeEntityIdMetadata(entityType = 'devices', entityId, body, callback)</td>
    <td style="padding:15px">updateMetadata</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApiV2EntityTypeEntityIdMetadata(entityType = 'devices', entityId, body, callback)</td>
    <td style="padding:15px">partialUpdateMetadata</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putApiV2EntityTypeEntityIdMetadataAttributeKey(entityType = 'devices', entityId, attributeKey, body, callback)</td>
    <td style="padding:15px">updateMetadataByAttribute</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/metadata/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackgroundTasks(callback)</td>
    <td style="padding:15px">getBackgroundTasks</td>
    <td style="padding:15px">{base_path}/{version}/background/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBackgroundTask(endpoint, method, bodyQuery, callback)</td>
    <td style="padding:15px">createBackgroundTask</td>
    <td style="padding:15px">{base_path}/{version}/background/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackgroundTask(taskId, callback)</td>
    <td style="padding:15px">getBackgroundTask</td>
    <td style="padding:15px">{base_path}/{version}/background/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentFilterSchema(callback)</td>
    <td style="padding:15px">getAlertAttachmentFilterSchema</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/filters/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachment(id, callback)</td>
    <td style="padding:15px">getAlertAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentAggregation(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentAggregation</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentAggregation(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentAggregation</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentFilters(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentFilters(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentFilters</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/filters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentResource(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentResource</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentResource(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentResource</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getAlertAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateAlertAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateAlertAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateAlertAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/alerts/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlertAttachment(id, body, callback)</td>
    <td style="padding:15px">createAlertAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPlugins(name, objectName, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getAllPlugins</td>
    <td style="padding:15px">{base_path}/{version}/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicePluginInfoSchema(callback)</td>
    <td style="padding:15px">getDevicePluginInfoSchema</td>
    <td style="padding:15px">{base_path}/{version}/plugins/device/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicatorExtendedInfoSchema(pluginId, callback)</td>
    <td style="padding:15px">getIndicatorExtendedInfoSchema</td>
    <td style="padding:15px">{base_path}/{version}/plugins/indicator/schema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPluginIndicatorTypes(includeExtendedInfo, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getAllPluginIndicatorTypes</td>
    <td style="padding:15px">{base_path}/{version}/plugins/indicatortypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPluginIndicatorType(body, callback)</td>
    <td style="padding:15px">createPluginIndicatorType</td>
    <td style="padding:15px">{base_path}/{version}/plugins/indicatortypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterPluginIndicatorTypes(includeExtendedInfo, page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterPluginIndicatorTypes</td>
    <td style="padding:15px">{base_path}/{version}/plugins/indicatortypes/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchemaForAllPluginIndicatorTypes(pluginId, callback)</td>
    <td style="padding:15px">getSchemaForAllPluginIndicatorTypes</td>
    <td style="padding:15px">{base_path}/{version}/plugins/indicatortypes/schema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePluginIndicatorType(id, body, callback)</td>
    <td style="padding:15px">updatePluginIndicatorType</td>
    <td style="padding:15px">{base_path}/{version}/plugins/indicatortypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectExtendedInfoSchema(pluginId, callback)</td>
    <td style="padding:15px">getObjectExtendedInfoSchema</td>
    <td style="padding:15px">{base_path}/{version}/plugins/object/schema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPluginObjectTypes(includeExtendedInfo, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getAllPluginObjectTypes</td>
    <td style="padding:15px">{base_path}/{version}/plugins/objecttypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPluginObjectType(body, callback)</td>
    <td style="padding:15px">createPluginObjectType</td>
    <td style="padding:15px">{base_path}/{version}/plugins/objecttypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterPluginObjectTypes(includeExtendedInfo, page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterPluginObjectTypes</td>
    <td style="padding:15px">{base_path}/{version}/plugins/objecttypes/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchemaForAllPluginObjectTypes(pluginId, callback)</td>
    <td style="padding:15px">getSchemaForAllPluginObjectTypes</td>
    <td style="padding:15px">{base_path}/{version}/plugins/objecttypes/schema/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePluginObjectType(id, body, callback)</td>
    <td style="padding:15px">updatePluginObjectType</td>
    <td style="padding:15px">{base_path}/{version}/plugins/objecttypes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAlerts(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getAllAlerts</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlert(body, callback)</td>
    <td style="padding:15px">createAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAlerts(alertStatus = 'open', id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getDeviceAlerts</td>
    <td style="padding:15px">{base_path}/{version}/alerts/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterAlerts(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterAlerts</td>
    <td style="padding:15px">{base_path}/{version}/alerts/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlertForced(body, callback)</td>
    <td style="padding:15px">createAlertForced</td>
    <td style="padding:15px">{base_path}/{version}/alerts/force?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowFalconDeviceAlerts(id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getFlowFalconDeviceAlerts</td>
    <td style="padding:15px">{base_path}/{version}/alerts/netflow-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaxSeverityAlertForObjects(body, callback)</td>
    <td style="padding:15px">getMaxSeverityAlertForObjects</td>
    <td style="padding:15px">{base_path}/{version}/alerts/objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlert(id, callback)</td>
    <td style="padding:15px">getAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlert(id, body, callback)</td>
    <td style="padding:15px">updateAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlertById(id, callback)</td>
    <td style="padding:15px">deleteAlertById</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAlert(id, body, callback)</td>
    <td style="padding:15px">patchAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAlert(id, username, callback)</td>
    <td style="padding:15px">assignAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/assign/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAlert(id, body, callback)</td>
    <td style="padding:15px">clearAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ignoreAlert(id, ignoreTime, body, callback)</td>
    <td style="padding:15px">ignoreAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/ignore/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimezonesByCountries(callback)</td>
    <td style="padding:15px">getTimezonesByCountries</td>
    <td style="padding:15px">{base_path}/{version}/countries/timezones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicies(type = 'other', page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getPolicies</td>
    <td style="padding:15px">{base_path}/{version}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(body, callback)</td>
    <td style="padding:15px">createPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterPolicies(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterPolicies</td>
    <td style="padding:15px">{base_path}/{version}/policies/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyFolders(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getPolicyFolders</td>
    <td style="padding:15px">{base_path}/{version}/policies/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyFolder(body, callback)</td>
    <td style="padding:15px">createPolicyFolder</td>
    <td style="padding:15px">{base_path}/{version}/policies/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyFolder(id, body, callback)</td>
    <td style="padding:15px">updatePolicyFolder</td>
    <td style="padding:15px">{base_path}/{version}/policies/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyFolderById(id, callback)</td>
    <td style="padding:15px">deletePolicyFolderById</td>
    <td style="padding:15px">{base_path}/{version}/policies/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicy(id, callback)</td>
    <td style="padding:15px">getPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicy(id, body, callback)</td>
    <td style="padding:15px">updatePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyById(id, callback)</td>
    <td style="padding:15px">deletePolicyById</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActionsUsingGET(policyId, callback)</td>
    <td style="padding:15px">getActionsUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyAction(policyId, body, callback)</td>
    <td style="padding:15px">updatePolicyAction</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyActionById(policyId, callback)</td>
    <td style="padding:15px">deletePolicyActionById</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findAllUsingGET(page, size, includeCount, sortBy, fields, policyId, callback)</td>
    <td style="padding:15px">findAllUsingGET</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyCondition(policyId, body, callback)</td>
    <td style="padding:15px">createPolicyCondition</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/conditions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyConditionById(policyId, conditionId, callback)</td>
    <td style="padding:15px">getPolicyConditionById</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/conditions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyCondition(policyId, conditionId, body, callback)</td>
    <td style="padding:15px">updatePolicyCondition</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/conditions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyConditionById(policyId, conditionId, callback)</td>
    <td style="padding:15px">deletePolicyConditionById</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/conditions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllReports(page, size, includeCount, sortBy, fields, userId, name, callback)</td>
    <td style="padding:15px">getAllReports</td>
    <td style="padding:15px">{base_path}/{version}/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReport(body, callback)</td>
    <td style="padding:15px">createReport</td>
    <td style="padding:15px">{base_path}/{version}/reports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllReportFolders(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getAllReportFolders</td>
    <td style="padding:15px">{base_path}/{version}/reports/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReportFolder(body, callback)</td>
    <td style="padding:15px">createReportFolder</td>
    <td style="padding:15px">{base_path}/{version}/reports/folders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportFolderById(id, body, callback)</td>
    <td style="padding:15px">updateReportFolderById</td>
    <td style="padding:15px">{base_path}/{version}/reports/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportFolderById(id, callback)</td>
    <td style="padding:15px">deleteReportFolderById</td>
    <td style="padding:15px">{base_path}/{version}/reports/folders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReport(id, callback)</td>
    <td style="padding:15px">getReport</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReportById(id, body, callback)</td>
    <td style="padding:15px">updateReportById</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReportById(id, callback)</td>
    <td style="padding:15px">deleteReportById</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelephonyAttachment(id, callback)</td>
    <td style="padding:15px">getTelephonyAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/telephony/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelephonyAttachmentAggregation(id, callback)</td>
    <td style="padding:15px">getTelephonyAttachmentAggregation</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/telephony/{pathv1}/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTelephonyAttachmentAggregation(id, body, callback)</td>
    <td style="padding:15px">updateTelephonyAttachmentAggregation</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/telephony/{pathv1}/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelephonyAttachmentSettings(id, callback)</td>
    <td style="padding:15px">getTelephonyAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/telephony/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTelephonyAttachmentSettings(id, body, callback)</td>
    <td style="padding:15px">updateTelephonyAttachmentSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/telephony/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelephonyAttachmentTimeSettings(id, callback)</td>
    <td style="padding:15px">getTelephonyAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/telephony/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTelephonyAttachmentTimeSettings(id, body, callback)</td>
    <td style="padding:15px">updateTelephonyAttachmentTimeSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/telephony/{pathv1}/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelephonyAttachmentVisualizationSettings(id, callback)</td>
    <td style="padding:15px">getTelephonyAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/telephony/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTelephonyAttachmentVisualizationSettings(id, body, callback)</td>
    <td style="padding:15px">updateTelephonyAttachmentVisualizationSettings</td>
    <td style="padding:15px">{base_path}/{version}/reports/attachments/telephony/{pathv1}/visualizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTelephonyAttachment(id, body, callback)</td>
    <td style="padding:15px">createTelephonyAttachment</td>
    <td style="padding:15px">{base_path}/{version}/reports/{pathv1}/attachments/telephony?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerDynamicPluginManager(body, callback)</td>
    <td style="padding:15px">registerDynamicPluginManager</td>
    <td style="padding:15px">{base_path}/{version}/pluginmanager/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerDynamicPlugin(id, body, callback)</td>
    <td style="padding:15px">registerDynamicPlugin</td>
    <td style="padding:15px">{base_path}/{version}/pluginmanager/{pathv1}/plugin/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroups(includeMembers, localOnly, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getObjectGroups</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createObjectGroup(body, callback)</td>
    <td style="padding:15px">createObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAncestors(ids, callback)</td>
    <td style="padding:15px">getAncestors</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/ancestors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findObjectGroup(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">findObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterObjectGroupMappings(page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterObjectGroupMappings</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/members/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupById(id, includeMembers, localOnly, callback)</td>
    <td style="padding:15px">getObjectGroupById</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateObjectGroupById(id, body, callback)</td>
    <td style="padding:15px">updateObjectGroupById</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectGroupById(id, callback)</td>
    <td style="padding:15px">deleteObjectGroupById</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateObjectGroupById(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateObjectGroupById</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiV2ObjectgroupsIdAncestors(id, callback)</td>
    <td style="padding:15px">getAncestors</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}/ancestors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectGroupMappings(id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getObjectGroupMappings</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addObjectGroupMember(id, body, callback)</td>
    <td style="padding:15px">addObjectGroupMember</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeObjectGroupMember(id, body, callback)</td>
    <td style="padding:15px">removeObjectGroupMember</td>
    <td style="padding:15px">{base_path}/{version}/objectgroups/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getDevices</td>
    <td style="padding:15px">{base_path}/{version}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDevice(body, callback)</td>
    <td style="padding:15px">createDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceData(body, callback)</td>
    <td style="padding:15px">createDeviceData</td>
    <td style="padding:15px">{base_path}/{version}/devices/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterDevice(includeObjects, includeIndicators, includeExtendedInfo, localOnly, page, size, includeCount, sortBy, fields, body, callback)</td>
    <td style="padding:15px">filterDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceById(id, includeObjects, includeIndicators, includeExtendedInfo, callback)</td>
    <td style="padding:15px">getDeviceById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceById(id, body, callback)</td>
    <td style="padding:15px">updateDeviceById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceById(id, callback)</td>
    <td style="padding:15px">deleteDeviceById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">partiallyUpdateDeviceById(id, body, callback)</td>
    <td style="padding:15px">partiallyUpdateDeviceById</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyDeviceRules(id, callback)</td>
    <td style="padding:15px">applyDeviceRules</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/rules/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapImages(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getMapImages</td>
    <td style="padding:15px">{base_path}/{version}/mapimages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMapImageById(id, callback)</td>
    <td style="padding:15px">getMapImageById</td>
    <td style="padding:15px">{base_path}/{version}/mapimages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLoggingLevel(body, callback)</td>
    <td style="padding:15px">postLoggingLevel</td>
    <td style="padding:15px">{base_path}/{version}/application/logger?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPublicMetrics(filter, callback)</td>
    <td style="padding:15px">getPublicMetrics</td>
    <td style="padding:15px">{base_path}/{version}/application/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersion(callback)</td>
    <td style="padding:15px">getVersion</td>
    <td style="padding:15px">{base_path}/{version}/application/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupsRules(page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getDeviceGroupsRules</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceGroupRule(body, callback)</td>
    <td style="padding:15px">createDeviceGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyDeviceGroupRules(callback)</td>
    <td style="padding:15px">applyDeviceGroupRules</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/rules/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupRule(id, callback)</td>
    <td style="padding:15px">getDeviceGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroupRule(id, body, callback)</td>
    <td style="padding:15px">updateDeviceGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroupRule(id, callback)</td>
    <td style="padding:15px">deleteDeviceGroupRule</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupRules(id, page, size, includeCount, sortBy, fields, callback)</td>
    <td style="padding:15px">getDeviceGroupRules</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyDeviceGroupRulesByGroupId(id, callback)</td>
    <td style="padding:15px">applyDeviceGroupRulesByGroupId</td>
    <td style="padding:15px">{base_path}/{version}/devicegroups/{pathv1}/rules/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
