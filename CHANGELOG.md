
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:35PM

See merge request itentialopensource/adapters/adapter-sevone_v2!13

---

## 0.4.3 [09-17-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-sevone_v2!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:49PM

See merge request itentialopensource/adapters/adapter-sevone_v2!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:02PM

See merge request itentialopensource/adapters/adapter-sevone_v2!9

---

## 0.4.0 [05-20-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone_v2!8

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_13:18PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone_v2!7

---

## 0.3.3 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone_v2!6

---

## 0.3.2 [03-12-2024]

* Changes made at 2024.03.12_11:00AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone_v2!5

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:36AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone_v2!4

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone_v2!3

---
