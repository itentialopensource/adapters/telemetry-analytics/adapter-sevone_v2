# SevOne v2

Vendor: SevOne v2
Homepage: https://SevOne v2.com/

Product: SevOne
Product Page: https://www.ibm.com/products/sevone-network-performance-management

## Introduction
We classify SevOne into the Service Assurance domain as SevOne provides information on Events and Monitoring of Networks. 

"Designed for modern networks, IBM® SevOne® Network Performance Management (IBM SevOne NPM) provides application-centric, network observability to help NetOps spot, address, and prevent network performance issues in hybrid environments." 

## Why Integrate
The SevOne adapter from Itential is used to integrate the Itential Automation Platform (IAP) with SevOne to offer a Monitoring, Events and Analytics. 

With this adapter you have the ability to perform operations with SevOne such as:

- Policies
- Alerts
- Devices
- Device Groups
- Topology
- Netflow

## Additional Product Documentation
The [SevOne REST API](https://developer.ibm.com/apis/catalog/sevonenpm--sevone-api/Introduction)
