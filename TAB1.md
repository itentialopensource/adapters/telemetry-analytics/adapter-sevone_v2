# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Sevone_v2 System. The API that was used to build the adapter for Sevone_v2 is usually available in the report directory of this adapter. The adapter utilizes the Sevone_v2 API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The SevOne adapter from Itential is used to integrate the Itential Automation Platform (IAP) with SevOne to offer a Monitoring, Events and Analytics. 

With this adapter you have the ability to perform operations with SevOne such as:

- Policies
- Alerts
- Devices
- Device Groups
- Topology
- Netflow

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
