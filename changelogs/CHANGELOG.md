
## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone_v2!2

---

## 0.1.2 [06-30-2021]

- Fixes for number formats not supported by IAP (remove int64 and int32 formats)

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sevone_v2!1

---

## 0.1.1 [03-31-2021]

- Initial Commit

See commit 570f0b3

---
