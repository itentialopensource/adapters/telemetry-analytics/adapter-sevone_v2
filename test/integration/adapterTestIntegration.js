/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-sevone_v2',
      type: 'SevoneV2',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const SevoneV2 = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Sevone_v2 Adapter Test', () => {
  describe('SevoneV2 Class Tests', () => {
    const a = new SevoneV2(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const reportAttachmentsObjectsId = 555;
    const reportAttachmentsObjectsCreateObjectAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createObjectAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createObjectAttachment(reportAttachmentsObjectsId, reportAttachmentsObjectsCreateObjectAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'createObjectAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectAttachment(reportAttachmentsObjectsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'getObjectAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectsUpdateObjectAttachmentResourcesBodyParam = {
      deviceId: 8,
      ids: [
        5
      ],
      type: 'Object'
    };
    describe('#updateObjectAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectAttachmentResources(reportAttachmentsObjectsId, reportAttachmentsObjectsUpdateObjectAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'updateObjectAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectAttachmentResources(reportAttachmentsObjectsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.deviceId);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('Device', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'getObjectAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectsUpdateObjectAttachmentSettingsBodyParam = {
      resultLimit: {
        resultLimit: 1
      }
    };
    describe('#updateObjectAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectAttachmentSettings(reportAttachmentsObjectsId, reportAttachmentsObjectsUpdateObjectAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'updateObjectAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectAttachmentSettings(reportAttachmentsObjectsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resultLimit);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'getObjectAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectsUpdateObjectAttachmentVisualizationSettingsBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 3
        },
        table: {
          blockVis: true,
          hideHeaders: true,
          wrapCells: false
        }
      }
    };
    describe('#updateObjectAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectAttachmentVisualizationSettings(reportAttachmentsObjectsId, reportAttachmentsObjectsUpdateObjectAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'updateObjectAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectsPartiallyUpdateObjectAttachmentVisualizationSettingsBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 9
        },
        table: {
          blockVis: false,
          hideHeaders: true,
          wrapCells: false
        }
      }
    };
    describe('#partiallyUpdateObjectAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateObjectAttachmentVisualizationSettings(reportAttachmentsObjectsId, reportAttachmentsObjectsPartiallyUpdateObjectAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'partiallyUpdateObjectAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectAttachmentVisualizationSettings(reportAttachmentsObjectsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjects', 'getObjectAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceTypesCreateDeviceTypeBodyParam = {
      deviceType: {
        name: 'string',
        parentId: 5
      },
      objectTypes: [
        {
          extendedInfo: {},
          id: 1,
          indicatorTypes: [
            {
              allowMaximumValue: {},
              dataUnits: {},
              description: {},
              displayUnits: {},
              extendedInfo: {},
              format: {},
              isDefault: {},
              isEnabled: {},
              name: {},
              pluginId: {},
              pluginObjectTypeId: {},
              syntheticExpression: {},
              syntheticMaximumExpression: {}
            }
          ],
          isEditable: false,
          isEnabled: true,
          name: 'string',
          objectTypes: [
            {
              extendedInfo: {},
              id: {},
              indicatorTypes: {},
              isEditable: {},
              isEnabled: {},
              name: {},
              objectTypes: {},
              parentObjectTypeId: {},
              pluginId: {}
            }
          ],
          parentObjectTypeId: 5,
          pluginId: 5
        }
      ]
    };
    describe('#createDeviceType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceType(deviceTypesCreateDeviceTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.deviceType);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal(true, Array.isArray(data.response.objectTypes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'createDeviceType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceTypesFilterDeviceTypesBodyParam = {
      deviceId: 1,
      ids: [
        4
      ],
      name: 'string',
      parentIds: [
        4
      ]
    };
    describe('#filterDeviceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterDeviceTypes(null, null, null, null, null, null, null, deviceTypesFilterDeviceTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(3, data.response.pageNumber);
                assert.equal(5, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'filterDeviceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceTypesDeviceId = 555;
    const deviceTypesId = 555;
    describe('#addMemberByIdToType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addMemberByIdToType(deviceTypesId, deviceTypesDeviceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'addMemberByIdToType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTypes(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(5, data.response.pageNumber);
                assert.equal(5, data.response.pageSize);
                assert.equal(8, data.response.totalElements);
                assert.equal(2, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'getDeviceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceTypesIds = 'fakedata';
    describe('#getAncestorsForDeviceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAncestorsForDeviceTypes(deviceTypesIds, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response[0]));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'getAncestorsForDeviceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypeForDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTypeForDeviceById(deviceTypesDeviceId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'getDeviceTypeForDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTypeById(deviceTypesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.deviceType);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal(true, Array.isArray(data.response.objectTypes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'getDeviceTypeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAncestorsForDeviceType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAncestorsForDeviceType(deviceTypesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response[0]));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'getAncestorsForDeviceType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectsFilterObjectsBodyParam = {
      alternateName: 'string',
      deviceGroupIds: [
        5
      ],
      deviceIds: [
        5
      ],
      enabled: 'AUTO',
      isDeleted: true,
      isEnabled: true,
      isVisible: false,
      metadata: {},
      name: 'string',
      objectGroupIds: [
        4
      ],
      objectIds: [
        {
          deviceId: 7,
          objectId: 1
        }
      ],
      peerIds: [
        4
      ],
      pluginIds: [
        5
      ],
      pluginName: 'string',
      pluginObjectTypeIds: [
        3
      ]
    };
    describe('#filterObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterObjects(null, null, null, null, null, null, null, objectsFilterObjectsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(7, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(6, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Objects', 'filterObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let objectsDeviceId = 555;
    let objectsId = 'fakedata';
    const objectsCreateObjectBodyParam = {
      name: 'string',
      description: 'string',
      pluginId: 6,
      pluginObjectTypeId: 4,
      pollFrequency: 2
    };
    describe('#createObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createObject(objectsDeviceId, objectsCreateObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.alternateName);
                assert.equal(8, data.response.dateAdded);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.deviceId);
                assert.equal('AUTO', data.response.enabled);
                assert.equal('object', typeof data.response.extendedInfo);
                assert.equal(4, data.response.id);
                assert.equal(true, Array.isArray(data.response.indicators));
                assert.equal(true, data.response.isDeleted);
                assert.equal(false, data.response.isEnabled);
                assert.equal(false, data.response.isVisible);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.pluginId);
                assert.equal(10, data.response.pluginObjectTypeId);
                assert.equal(10, data.response.subtypeId);
              } else {
                runCommonAsserts(data, error);
              }
              objectsDeviceId = data.response.id;
              objectsId = data.response.id;
              saveMockData('Objects', 'createObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjects(objectsDeviceId, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(10, data.response.pageSize);
                assert.equal(7, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Objects', 'getObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectsUpdateObjectByIdBodyParam = {
      name: 'string',
      description: 'string',
      pollFrequency: 3
    };
    describe('#updateObjectById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectById(objectsDeviceId, objectsId, objectsUpdateObjectByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Objects', 'updateObjectById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectsPartiallyUpdateObjectByIdBodyParam = {
      name: 'string',
      description: 'string',
      pollFrequency: 6
    };
    describe('#partiallyUpdateObjectById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateObjectById(objectsDeviceId, objectsId, objectsPartiallyUpdateObjectByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Objects', 'partiallyUpdateObjectById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectById(objectsDeviceId, objectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.alternateName);
                assert.equal(7, data.response.dateAdded);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.deviceId);
                assert.equal('AUTO', data.response.enabled);
                assert.equal('object', typeof data.response.extendedInfo);
                assert.equal(4, data.response.id);
                assert.equal(true, Array.isArray(data.response.indicators));
                assert.equal(true, data.response.isDeleted);
                assert.equal(false, data.response.isEnabled);
                assert.equal(true, data.response.isVisible);
                assert.equal('string', data.response.name);
                assert.equal(7, data.response.pluginId);
                assert.equal(9, data.response.pluginObjectTypeId);
                assert.equal(1, data.response.subtypeId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Objects', 'getObjectById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsStatusMapId = 555;
    const reportAttachmentsStatusMapCreateStatusMapAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createStatusMapAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createStatusMapAttachment(reportAttachmentsStatusMapId, reportAttachmentsStatusMapCreateStatusMapAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsStatusMap', 'createStatusMapAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusAttachment(reportAttachmentsStatusMapId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsStatusMap', 'getStatusAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsStatusMapUpdateStatusMapAttachmentResourcesBodyParam = {
      mapId: 2
    };
    describe('#updateStatusMapAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateStatusMapAttachmentResources(reportAttachmentsStatusMapId, reportAttachmentsStatusMapUpdateStatusMapAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsStatusMap', 'updateStatusMapAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusMapAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusMapAttachmentResources(reportAttachmentsStatusMapId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.mapId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsStatusMap', 'getStatusMapAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let apiKeysApiKey = 'fakedata';
    const apiKeysCreateApiKeyBodyParam = {
      applicationName: 'string',
      expirationDate: 3
    };
    describe('#createApiKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApiKey(apiKeysCreateApiKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.apiKey);
                assert.equal('string', data.response.applicationName);
                assert.equal(8, data.response.creationDate);
                assert.equal(6, data.response.expirationDate);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              apiKeysApiKey = data.response.apiKey;
              saveMockData('ApiKeys', 'createApiKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiKeysId = 555;
    const apiKeysCreateApiKeyForUserBodyParam = {
      applicationName: 'string',
      expirationDate: 10
    };
    describe('#createApiKeyForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApiKeyForUser(apiKeysId, apiKeysCreateApiKeyForUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.apiKey);
                assert.equal('string', data.response.applicationName);
                assert.equal(7, data.response.creationDate);
                assert.equal(10, data.response.expirationDate);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'createApiKeyForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiKeys((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'getApiKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiKeysUpdateApiKeyBodyParam = {
      applicationName: 'string',
      expirationDate: 2
    };
    describe('#updateApiKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApiKey(apiKeysApiKey, apiKeysUpdateApiKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'updateApiKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiKeysForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiKeysForUser(apiKeysId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'getApiKeysForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiKeysUpdateApiKeyForUserBodyParam = {
      applicationName: 'string',
      expirationDate: 8
    };
    describe('#updateApiKeyForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApiKeyForUser(apiKeysId, apiKeysApiKey, apiKeysUpdateApiKeyForUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'updateApiKeyForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const discoveryFilterDevicesInDiscoveryBodyParam = {
      allowAutomatic: true,
      allowManual: true,
      deviceFilter: {
        allowDelete: false,
        alternateName: 'string',
        deleted: true,
        description: 'string',
        disableConcurrentPolling: false,
        disablePolling: true,
        disableThresholding: false,
        ids: [
          4
        ],
        inDeviceGroupIds: [
          6
        ],
        ipAddress: 'string',
        metadata: {},
        name: 'string',
        new: true,
        notInDeviceGroupIds: [
          9
        ],
        numElements: 8,
        peerIds: [
          1
        ],
        pluginManagerId: 2,
        pollFrequency: 9,
        snmpVersion: 3,
        timezone: 'string',
        workhoursGroupId: 7
      },
      isWorking: false,
      queueIn: [
        'string'
      ],
      timeCompletedBetween: {
        key: 2,
        value: 5
      },
      timeQueuedBetween: {
        key: 10,
        value: 10
      },
      timeStartedBetween: {
        key: 7,
        value: 9
      },
      timeUpdatedBetween: {
        key: 3,
        value: 3
      },
      timesDiscoveredBetween: {
        key: 9,
        value: 1
      }
    };
    describe('#filterDevicesInDiscovery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterDevicesInDiscovery(null, null, null, null, null, null, null, null, discoveryFilterDevicesInDiscoveryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'filterDevicesInDiscovery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runDiscoveryAllDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runDiscoveryAllDevices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(3, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'runDiscoveryAllDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const discoveryId = 555;
    describe('#runDiscoverDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runDiscoverDevice(discoveryId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(2, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'runDiscoverDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesInDiscovery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevicesInDiscovery(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(3, data.response.pageSize);
                assert.equal(8, data.response.totalElements);
                assert.equal(3, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'getDevicesInDiscovery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const discoveryUpdateDevicePriorityBodyParam = {
      allowAutomatic: true,
      allowManual: false,
      isWorking: true,
      queue: 'high'
    };
    describe('#updateDevicePriority - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDevicePriority(discoveryId, discoveryUpdateDevicePriorityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'updateDevicePriority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceStatusById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceStatusById(discoveryId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.allowAutomatic);
                assert.equal(true, data.response.allowManual);
                assert.equal('object', typeof data.response.device);
                assert.equal(8, data.response.deviceId);
                assert.equal(false, data.response.isWorking);
                assert.equal('low', data.response.queue);
                assert.equal(3, data.response.timeCompleted);
                assert.equal(6, data.response.timeQueued);
                assert.equal(9, data.response.timeStarted);
                assert.equal(4, data.response.timeUpdated);
                assert.equal(1, data.response.timesDiscovered);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'getDeviceStatusById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let topNViewsId = 'fakedata';
    const topNViewsCreateTopNViewBodyParam = {
      common: false,
      custom: true,
      direction: 'DESC',
      id: 8,
      name: 'string',
      orderNumber: 1
    };
    describe('#createTopNView - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTopNView(topNViewsCreateTopNViewBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.common);
                assert.equal(true, data.response.custom);
                assert.equal('DESC', data.response.direction);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(3, data.response.orderNumber);
              } else {
                runCommonAsserts(data, error);
              }
              topNViewsId = data.response.id;
              saveMockData('TopNViews', 'createTopNView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNViews - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNViews(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(8, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(10, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopNViews', 'getTopNViews', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topNViewsUpdateTopNViewByIdBodyParam = {
      common: false,
      custom: true,
      direction: 'ASC',
      id: 10,
      name: 'string',
      orderNumber: 9
    };
    describe('#updateTopNViewById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopNViewById(topNViewsId, topNViewsUpdateTopNViewByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopNViews', 'updateTopNViewById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNView - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNView(topNViewsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.common);
                assert.equal(true, data.response.custom);
                assert.equal('ASC', data.response.direction);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.orderNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopNViews', 'getTopNView', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let rolesId = 'fakedata';
    const rolesCreateRoleBodyParam = {
      description: 'string',
      id: 7,
      name: 'string',
      parentId: 3
    };
    describe('#createRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRole(rolesCreateRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(9, data.response.parentId);
              } else {
                runCommonAsserts(data, error);
              }
              rolesId = data.response.id;
              saveMockData('Roles', 'createRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesFilterRolesBodyParam = {
      childId: 1,
      description: 'string',
      ids: [
        4
      ],
      name: 'string',
      parentId: 8,
      userId: 9
    };
    describe('#filterRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterRoles(null, null, null, null, null, rolesFilterRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(2, data.response.pageNumber);
                assert.equal(10, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'filterRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRoles(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(8, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(5, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'getAllRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesPatchRoleBodyParam = {
      description: 'string',
      name: 'string'
    };
    describe('#patchRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchRole(rolesId, rolesPatchRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'patchRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRole(rolesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(9, data.response.parentId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'getRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopologyId = 555;
    const reportAttachmentsTopologyCreateTopologyAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createTopologyAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTopologyAttachment(reportAttachmentsTopologyId, reportAttachmentsTopologyCreateTopologyAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'createTopologyAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopologyAttachment(reportAttachmentsTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'getTopologyAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopologyUpdateTopologyAttachmentFiltersBodyParam = {
      deviceGroups: [
        8
      ],
      sourceTypes: [
        'string'
      ]
    };
    describe('#updateTopologyAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopologyAttachmentFilters(reportAttachmentsTopologyId, reportAttachmentsTopologyUpdateTopologyAttachmentFiltersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'updateTopologyAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopologyAttachmentFilters(reportAttachmentsTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.deviceGroups));
                assert.equal(true, Array.isArray(data.response.sourceTypes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'getTopologyAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopologyUpdateTopologyAttachmentResourcesBodyParam = {
      deviceId: 9
    };
    describe('#updateTopologyAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopologyAttachmentResources(reportAttachmentsTopologyId, reportAttachmentsTopologyUpdateTopologyAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'updateTopologyAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopologyAttachmentResources(reportAttachmentsTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.deviceId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'getTopologyAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopologyUpdateTopologyAttachmentSettingsBodyParam = {
      layout: {
        directionality: 'TopToBottom',
        graphHeight: 'Medium',
        graphWidth: 'Medium',
        groups: [
          5
        ],
        type: 'concentric'
      },
      numberOfHops: 10
    };
    describe('#updateTopologyAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopologyAttachmentSettings(reportAttachmentsTopologyId, reportAttachmentsTopologyUpdateTopologyAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'updateTopologyAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopologyPartialUpdateTopologyAttachmentSettingsBodyParam = {
      layout: {
        directionality: 'TopToBottom',
        graphHeight: 'Medium',
        graphWidth: 'Medium',
        groups: [
          10
        ],
        type: 'horizontal'
      },
      numberOfHops: 7
    };
    describe('#partialUpdateTopologyAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partialUpdateTopologyAttachmentSettings(reportAttachmentsTopologyId, reportAttachmentsTopologyPartialUpdateTopologyAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'partialUpdateTopologyAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopologyAttachmentSettings(reportAttachmentsTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.layout);
                assert.equal(6, data.response.numberOfHops);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'getTopologyAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopologyUpdateTopologyAttachmentVisualizationSettingsBodyParam = {
      doNotVisualize: false,
      showAllNodes: false
    };
    describe('#updateTopologyAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopologyAttachmentVisualizationSettings(reportAttachmentsTopologyId, reportAttachmentsTopologyUpdateTopologyAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'updateTopologyAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopologyAttachmentVisualizationSettings(reportAttachmentsTopologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.doNotVisualize);
                assert.equal(false, data.response.showAllNodes);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopology', 'getTopologyAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconId = 555;
    const reportAttachmentsFlowFalconCreateFlowFalconAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createFlowFalconAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFlowFalconAttachment(reportAttachmentsFlowFalconId, reportAttachmentsFlowFalconCreateFlowFalconAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'createFlowFalconAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentFilterSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentFilterSchema((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('object', typeof data.response.operations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentFilterSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachment(reportAttachmentsFlowFalconId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentFiltersBodyParam = {
      empty: true,
      expression: 'string',
      filters: [
        {
          id: 'string',
          name: 'string',
          operation: 'string',
          values: [
            'string'
          ]
        }
      ]
    };
    describe('#updateFlowFalconAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentFilters(reportAttachmentsFlowFalconId, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentFiltersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentFilters(reportAttachmentsFlowFalconId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.empty);
                assert.equal('string', data.response.expression);
                assert.equal(true, Array.isArray(data.response.filters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentResourcesBodyParam = {
      groups: {
        ids: [
          1
        ],
        type: 'DeviceGroup'
      },
      interfaces: [
        {
          direction: 3,
          interfaceNum: 10,
          sourceIp: 'string'
        }
      ]
    };
    describe('#updateFlowFalconAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentResources(reportAttachmentsFlowFalconId, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentResources(reportAttachmentsFlowFalconId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.groups);
                assert.equal(true, Array.isArray(data.response.interfaces));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentSettingsBodyParam = {
      flowFalcon: {
        canZoomInCb: 'string',
        granularity: 8,
        graphOther: false,
        isRate: false,
        isTotal: false,
        split: 1,
        subnetCategoryId: 4
      },
      flowFalconResolution: {
        showAs: 'string',
        showDns: 'string',
        showDscp: 'string',
        showPort: 'string',
        showProtocol: 'string'
      },
      flowFalconTemplate: {
        isAggregated: false,
        metricId: 6,
        viewId: 8
      },
      resultLimit: {
        resultLimit: 8
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      },
      units: {
        percentage: true,
        preferredUnits: 'bits'
      }
    };
    describe('#updateFlowFalconAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentSettings(reportAttachmentsFlowFalconId, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconPartiallyUpdateFlowFalconAttachmentSettingsBodyParam = {
      flowFalcon: {
        canZoomInCb: 'string',
        granularity: 8,
        graphOther: true,
        isRate: false,
        isTotal: true,
        split: 6,
        subnetCategoryId: 7
      },
      flowFalconResolution: {
        showAs: 'string',
        showDns: 'string',
        showDscp: 'string',
        showPort: 'string',
        showProtocol: 'string'
      },
      flowFalconTemplate: {
        isAggregated: true,
        metricId: 9,
        viewId: 7
      },
      resultLimit: {
        resultLimit: 7
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      },
      units: {
        percentage: false,
        preferredUnits: 'bits'
      }
    };
    describe('#partiallyUpdateFlowFalconAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateFlowFalconAttachmentSettings(reportAttachmentsFlowFalconId, reportAttachmentsFlowFalconPartiallyUpdateFlowFalconAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'partiallyUpdateFlowFalconAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentSettings(reportAttachmentsFlowFalconId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.flowFalcon);
                assert.equal('object', typeof data.response.flowFalconResolution);
                assert.equal('object', typeof data.response.flowFalconTemplate);
                assert.equal('object', typeof data.response.resultLimit);
                assert.equal('object', typeof data.response.sourceFields);
                assert.equal('object', typeof data.response.units);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        options: {},
        start: 'string',
        type: 'CustomWeek'
      },
      timezone: 'string'
    };
    describe('#updateFlowFalconAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentTimeSettings(reportAttachmentsFlowFalconId, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentTimeSettings(reportAttachmentsFlowFalconId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconUpdateFlowFalconAttachmentVisualizationSettingsBodyParam = {
      pie: {
        showAggregation: true,
        showAverage: false,
        showFrequency: false,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: false,
        showTitle: true,
        showValley: true
      },
      stackedLine: {
        drawGraphOutline: false,
        prettyXAxis: false,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: true,
        sendGraphCSV: true,
        showAggregation: true,
        showAverage: false,
        showFrequency: true,
        showLast: true,
        showLegend: false,
        showPeak: true,
        showTimespan: false,
        showTitle: true,
        showValley: true
      },
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: true,
          precision: 7
        },
        table: {
          blockVis: false,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#updateFlowFalconAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFlowFalconAttachmentVisualizationSettings(reportAttachmentsFlowFalconId, reportAttachmentsFlowFalconUpdateFlowFalconAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'updateFlowFalconAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsFlowFalconPartiallyUpdateFlowFalconAttachmentVisualizationSettingsBodyParam = {
      pie: {
        showAggregation: false,
        showAverage: true,
        showFrequency: true,
        showLast: false,
        showLegend: false,
        showPeak: true,
        showTimespan: true,
        showTitle: false,
        showValley: false
      },
      stackedLine: {
        drawGraphOutline: true,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: false,
        sendGraphCSV: true,
        showAggregation: false,
        showAverage: true,
        showFrequency: true,
        showLast: true,
        showLegend: true,
        showPeak: true,
        showTimespan: false,
        showTitle: false,
        showValley: false
      },
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 4
        },
        table: {
          blockVis: false,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#partiallyUpdateFlowFalconAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateFlowFalconAttachmentVisualizationSettings(reportAttachmentsFlowFalconId, reportAttachmentsFlowFalconPartiallyUpdateFlowFalconAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'partiallyUpdateFlowFalconAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconAttachmentVisualizationSettings(reportAttachmentsFlowFalconId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.pie);
                assert.equal('object', typeof data.response.stackedLine);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsFlowFalcon', 'getFlowFalconAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNId = 555;
    const reportAttachmentsTopNCreateTopNAttachmentBodyParam = {
      name: 'string',
      resource: {}
    };
    describe('#createTopNAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTopNAttachment(reportAttachmentsTopNId, reportAttachmentsTopNCreateTopNAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'createTopNAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNAttachment(reportAttachmentsTopNId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'getTopNAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNUpdateTopNAttachmentResourcesBodyParam = {
      ids: [
        {}
      ],
      memberOfType: 'ALL',
      type: 'Object'
    };
    describe('#updateTopNAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopNAttachmentResources(reportAttachmentsTopNId, reportAttachmentsTopNUpdateTopNAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'updateTopNAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNAttachmentResources(reportAttachmentsTopNId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('ANY', data.response.memberOfType);
                assert.equal('ObjectGroup', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'getTopNAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNUpdateTopNAttachmentSettingsBodyParam = {
      aggregation: {
        aggregationUnits: 'hourly'
      },
      resultLimit: {
        resultLimit: 3
      },
      topN: {
        capacityThreshold: {
          operator: 'NONE',
          units: 'string',
          value: 7
        },
        displayOnlyExceedingObjects: true,
        displayRValues: false,
        sortBy: 9,
        sortOrder: 'ASC',
        templateId: 7
      },
      units: {
        percentage: true,
        preferredUnits: 'bits'
      },
      workHours: {
        type: 'none'
      }
    };
    describe('#updateTopNAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopNAttachmentSettings(reportAttachmentsTopNId, reportAttachmentsTopNUpdateTopNAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'updateTopNAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNPartiallyUpdateTopNAttachmentSettingsBodyParam = {
      aggregation: {
        aggregationUnits: 'auto'
      },
      resultLimit: {
        resultLimit: 8
      },
      topN: {
        capacityThreshold: {
          operator: 'NONE',
          units: 'string',
          value: 7
        },
        displayOnlyExceedingObjects: true,
        displayRValues: true,
        sortBy: 9,
        sortOrder: 'ASC',
        templateId: 3
      },
      units: {
        percentage: false,
        preferredUnits: 'bytes'
      },
      workHours: {
        type: 'none'
      }
    };
    describe('#partiallyUpdateTopNAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateTopNAttachmentSettings(reportAttachmentsTopNId, reportAttachmentsTopNPartiallyUpdateTopNAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'partiallyUpdateTopNAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNAttachmentSettings(reportAttachmentsTopNId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.aggregation);
                assert.equal('object', typeof data.response.resultLimit);
                assert.equal('object', typeof data.response.topN);
                assert.equal('object', typeof data.response.units);
                assert.equal('object', typeof data.response.workHours);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'getTopNAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNUpdateTopNAttachmentTimeSettingsBodyParam = {
      ranges: [
        {
          end: 'string',
          options: {},
          start: 'string',
          type: 'SpecificInterval'
        }
      ],
      timezone: 'string'
    };
    describe('#updateTopNAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopNAttachmentTimeSettings(reportAttachmentsTopNId, reportAttachmentsTopNUpdateTopNAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'updateTopNAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNAttachmentTimeSettings(reportAttachmentsTopNId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ranges));
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'getTopNAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNUpdateTopNAttachmentVisualizationSettingsBodyParam = {
      pie: {
        showAggregation: true,
        showAverage: false,
        showFrequency: false,
        showLast: false,
        showLegend: true,
        showPeak: false,
        showTimespan: true,
        showTitle: true,
        showValley: false
      },
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: true,
          precision: 4
        },
        table: {
          blockVis: true,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#updateTopNAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTopNAttachmentVisualizationSettings(reportAttachmentsTopNId, reportAttachmentsTopNUpdateTopNAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'updateTopNAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTopNPartiallyUpdateTopNAttachmentVisualizationSettingsBodyParam = {
      pie: {
        showAggregation: true,
        showAverage: true,
        showFrequency: false,
        showLast: false,
        showLegend: true,
        showPeak: false,
        showTimespan: false,
        showTitle: true,
        showValley: false
      },
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 3
        },
        table: {
          blockVis: false,
          hideHeaders: false,
          wrapCells: true
        }
      }
    };
    describe('#partiallyUpdateTopNAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateTopNAttachmentVisualizationSettings(reportAttachmentsTopNId, reportAttachmentsTopNPartiallyUpdateTopNAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'partiallyUpdateTopNAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopNAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopNAttachmentVisualizationSettings(reportAttachmentsTopNId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.pie);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTopN', 'getTopNAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let workHoursId = 'fakedata';
    const workHoursCreateWorkHoursBodyParam = {
      id: 8,
      isDefault: false,
      name: 'string',
      relativeTimes: [
        {
          endHour: 6,
          endMin: 3,
          friday: false,
          monday: false,
          saturday: false,
          startHour: 5,
          startMin: 7,
          sunday: true,
          thursday: true,
          tuesday: true,
          wednesday: false
        }
      ]
    };
    describe('#createWorkHours - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createWorkHours(workHoursCreateWorkHoursBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal(true, data.response.isDefault);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.relativeTimes));
              } else {
                runCommonAsserts(data, error);
              }
              workHoursId = data.response.id;
              saveMockData('WorkHours', 'createWorkHours', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkHours - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkHours(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(7, data.response.pageNumber);
                assert.equal(1, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkHours', 'getWorkHours', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workHoursUpdateWorkHoursByGroupIdBodyParam = {
      id: 5,
      isDefault: true,
      name: 'string',
      relativeTimes: [
        {
          endHour: 7,
          endMin: 8,
          friday: false,
          monday: false,
          saturday: false,
          startHour: 10,
          startMin: 3,
          sunday: false,
          thursday: false,
          tuesday: true,
          wednesday: true
        }
      ]
    };
    describe('#updateWorkHoursByGroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateWorkHoursByGroupId(workHoursId, workHoursUpdateWorkHoursByGroupIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkHours', 'updateWorkHoursByGroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkHoursByGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkHoursByGroup(workHoursId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal(true, data.response.isDefault);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.relativeTimes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkHours', 'getWorkHoursByGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPeersUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPeersUsingGET(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(7, data.response.totalElements);
                assert.equal(10, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getPeersUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClusterSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getClusterSettings(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getClusterSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentPeerUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCurrentPeerUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('PRIMARY', data.response.activeAppliance);
                assert.equal(5, data.response.capacity);
                assert.equal(true, data.response.disabled);
                assert.equal(7, data.response.droppedFlowsPerSecond);
                assert.equal(5, data.response.flowLimit);
                assert.equal(2, data.response.flowLoad);
                assert.equal(7, data.response.flowsPerSecond);
                assert.equal(5, data.response.interfaceLimit);
                assert.equal('string', data.response.ip);
                assert.equal(false, data.response.master);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.netflowDeviceCount);
                assert.equal(7, data.response.netflowInterfaceCount);
                assert.equal('string', data.response.primaryIp);
                assert.equal(2, data.response.processedFlowsPerSecond);
                assert.equal('string', data.response.secondaryIp);
                assert.equal(6, data.response.serverId);
                assert.equal(8, data.response.serverLoad);
                assert.equal(6, data.response.telephonyActiveCallsLimit);
                assert.equal(9, data.response.telephonyPhones);
                assert.equal(9, data.response.telephonyPhonesLimit);
                assert.equal(6, data.response.totalCapacity);
                assert.equal(3, data.response.totalLoad);
                assert.equal('string', data.response.virtualIp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getCurrentPeerUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const peersEditIncorporateModeUsingPATCHBodyParam = {
      status: 'ACTIVE'
    };
    describe('#editIncorporateModeUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editIncorporateModeUsingPATCH(peersEditIncorporateModeUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'editIncorporateModeUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncorporateModeUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncorporateModeUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.endTime);
                assert.equal(1, data.response.startTime);
                assert.equal('ACTIVE', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getIncorporateModeUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const peersId = 555;
    describe('#getPeerUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPeerUsingGET(peersId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('SECONDARY', data.response.activeAppliance);
                assert.equal(7, data.response.capacity);
                assert.equal(true, data.response.disabled);
                assert.equal(9, data.response.droppedFlowsPerSecond);
                assert.equal(10, data.response.flowLimit);
                assert.equal(5, data.response.flowLoad);
                assert.equal(3, data.response.flowsPerSecond);
                assert.equal(2, data.response.interfaceLimit);
                assert.equal('string', data.response.ip);
                assert.equal(false, data.response.master);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.name);
                assert.equal(7, data.response.netflowDeviceCount);
                assert.equal(1, data.response.netflowInterfaceCount);
                assert.equal('string', data.response.primaryIp);
                assert.equal(3, data.response.processedFlowsPerSecond);
                assert.equal('string', data.response.secondaryIp);
                assert.equal(1, data.response.serverId);
                assert.equal(8, data.response.serverLoad);
                assert.equal(8, data.response.telephonyActiveCallsLimit);
                assert.equal(2, data.response.telephonyPhones);
                assert.equal(3, data.response.telephonyPhonesLimit);
                assert.equal(9, data.response.totalCapacity);
                assert.equal(6, data.response.totalLoad);
                assert.equal('string', data.response.virtualIp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getPeerUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSettings(peersId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Peers', 'getSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let thresholdsDeviceId = 555;
    let thresholdsThresholdId = 555;
    let thresholdsTriggerType = 'fakedata';
    let thresholdsConditionId = 555;
    const thresholdsCreateThresholdBodyParam = {
      absoluteTimes: [
        {
          deviceId: 3,
          endTime: 6,
          id: 7,
          onOff: false,
          startTime: 3,
          thresholdId: 3,
          timeZone: 'string'
        }
      ],
      appendConditionMessages: false,
      clearConditions: [
        {
          aggregation: 6,
          comparison: 3,
          deviceId: 10,
          duration: 3,
          flowDto: {
            elementId: 6,
            unit: 'T'
          },
          id: 3,
          message: 'string',
          objectId: 9,
          pluginId: 8,
          policyConditionId: 8,
          policyId: 10,
          pollId: 5,
          sigmaDirection: 3,
          thresholdId: 1,
          type: 3,
          unit: 'string',
          value: 2,
          value2: 4
        }
      ],
      clearExpression: 'string',
      clearMessage: 'string',
      description: 'string',
      deviceGroup: true,
      deviceId: 5,
      flowDto: {
        clearDuration: 7,
        deviceId: 8,
        directionId: 1,
        filterId: 5,
        interfaceId: 5,
        triggerDuration: 4,
        viewId: 10
      },
      groupId: 10,
      id: 2,
      lastUpdated: 4,
      mailOnce: false,
      mailPeriod: 6,
      mailTo: 'string',
      name: 'string',
      peerId: 5,
      policyEnabled: 3,
      policyId: 7,
      relativeTimes: [
        {
          deviceId: 8,
          endHour: 4,
          endMin: 4,
          friday: true,
          id: 3,
          monday: false,
          onOff: true,
          saturday: true,
          startHour: 4,
          startMin: 4,
          sunday: true,
          thresholdId: 3,
          thursday: true,
          timeZone: 'string',
          tuesday: false,
          wednesday: false
        }
      ],
      severity: 4,
      trapDestinations: [
        {
          enabled: true,
          id: 10,
          thresholdId: 2,
          trapDestinationId: 7
        }
      ],
      triggerConditions: [
        {
          aggregation: 6,
          comparison: 2,
          deviceId: 4,
          duration: 7,
          flowDto: {
            elementId: 8,
            unit: 'k'
          },
          id: 5,
          message: 'string',
          objectId: 10,
          pluginId: 4,
          policyConditionId: 1,
          policyId: 10,
          pollId: 7,
          sigmaDirection: 4,
          thresholdId: 8,
          type: 8,
          unit: 'string',
          value: 5,
          value2: 10
        }
      ],
      triggerExpression: 'string',
      triggerMessage: 'string',
      type: 'other',
      useCustomTraps: true,
      useDefaultTraps: false,
      useDeviceTraps: false,
      userEnabled: 7
    };
    describe('#createThreshold - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createThreshold(thresholdsDeviceId, thresholdsCreateThresholdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.absoluteTimes));
                assert.equal(false, data.response.appendConditionMessages);
                assert.equal(true, Array.isArray(data.response.clearConditions));
                assert.equal('string', data.response.clearExpression);
                assert.equal('string', data.response.clearMessage);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.deviceGroup);
                assert.equal(5, data.response.deviceId);
                assert.equal('object', typeof data.response.flowDto);
                assert.equal(8, data.response.groupId);
                assert.equal(4, data.response.id);
                assert.equal(10, data.response.lastUpdated);
                assert.equal(false, data.response.mailOnce);
                assert.equal(7, data.response.mailPeriod);
                assert.equal('string', data.response.mailTo);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.peerId);
                assert.equal(8, data.response.policyEnabled);
                assert.equal(9, data.response.policyId);
                assert.equal(true, Array.isArray(data.response.relativeTimes));
                assert.equal(2, data.response.severity);
                assert.equal(true, Array.isArray(data.response.trapDestinations));
                assert.equal(true, Array.isArray(data.response.triggerConditions));
                assert.equal('string', data.response.triggerExpression);
                assert.equal('string', data.response.triggerMessage);
                assert.equal('other', data.response.type);
                assert.equal(false, data.response.useCustomTraps);
                assert.equal(true, data.response.useDefaultTraps);
                assert.equal(true, data.response.useDeviceTraps);
                assert.equal(9, data.response.userEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              thresholdsDeviceId = data.response.id;
              thresholdsThresholdId = data.response.id;
              thresholdsTriggerType = data.response.type;
              thresholdsConditionId = data.response.id;
              saveMockData('Thresholds', 'createThreshold', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsCreateThresholdConditionBodyParam = {
      aggregation: 2,
      comparison: 2,
      deviceId: 4,
      duration: 1,
      flowDto: {
        elementId: 6,
        unit: 'M'
      },
      id: 6,
      message: 'string',
      objectId: 1,
      pluginId: 1,
      policyConditionId: 4,
      policyId: 1,
      pollId: 7,
      sigmaDirection: 7,
      thresholdId: 1,
      type: 7,
      unit: 'string',
      value: 3,
      value2: 5
    };
    describe('#createThresholdCondition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createThresholdCondition(thresholdsDeviceId, thresholdsThresholdId, thresholdsTriggerType, thresholdsCreateThresholdConditionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.aggregation);
                assert.equal(6, data.response.comparison);
                assert.equal(7, data.response.deviceId);
                assert.equal(10, data.response.duration);
                assert.equal('object', typeof data.response.flowDto);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.message);
                assert.equal(10, data.response.objectId);
                assert.equal(9, data.response.pluginId);
                assert.equal(6, data.response.policyConditionId);
                assert.equal(4, data.response.policyId);
                assert.equal(7, data.response.pollId);
                assert.equal(1, data.response.sigmaDirection);
                assert.equal(9, data.response.thresholdId);
                assert.equal(3, data.response.type);
                assert.equal('string', data.response.unit);
                assert.equal(4, data.response.value);
                assert.equal(7, data.response.value2);
              } else {
                runCommonAsserts(data, error);
              }
              thresholdsThresholdId = data.response.thresholdId;
              saveMockData('Thresholds', 'createThresholdCondition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThresholds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getThresholds(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(2, data.response.pageNumber);
                assert.equal(6, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(6, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'getThresholds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsUpdateThresholdBodyParam = {
      absoluteTimes: [
        {
          deviceId: 2,
          endTime: 10,
          id: 4,
          onOff: true,
          startTime: 7,
          thresholdId: 2,
          timeZone: 'string'
        }
      ],
      appendConditionMessages: false,
      clearConditions: [
        {
          aggregation: 3,
          comparison: 10,
          deviceId: 9,
          duration: 7,
          flowDto: {
            elementId: 3,
            unit: 'E'
          },
          id: 9,
          message: 'string',
          objectId: 6,
          pluginId: 7,
          policyConditionId: 4,
          policyId: 9,
          pollId: 7,
          sigmaDirection: 10,
          thresholdId: 5,
          type: 9,
          unit: 'string',
          value: 10,
          value2: 10
        }
      ],
      clearExpression: 'string',
      clearMessage: 'string',
      description: 'string',
      deviceGroup: false,
      deviceId: 3,
      flowDto: {
        clearDuration: 8,
        deviceId: 10,
        directionId: 1,
        filterId: 6,
        interfaceId: 5,
        triggerDuration: 5,
        viewId: 5
      },
      groupId: 9,
      id: 9,
      lastUpdated: 4,
      mailOnce: true,
      mailPeriod: 1,
      mailTo: 'string',
      name: 'string',
      peerId: 9,
      policyEnabled: 5,
      policyId: 10,
      relativeTimes: [
        {
          deviceId: 1,
          endHour: 5,
          endMin: 10,
          friday: false,
          id: 10,
          monday: true,
          onOff: false,
          saturday: false,
          startHour: 8,
          startMin: 5,
          sunday: true,
          thresholdId: 6,
          thursday: false,
          timeZone: 'string',
          tuesday: false,
          wednesday: false
        }
      ],
      severity: 10,
      trapDestinations: [
        {
          enabled: true,
          id: 4,
          thresholdId: 3,
          trapDestinationId: 9
        }
      ],
      triggerConditions: [
        {
          aggregation: 5,
          comparison: 10,
          deviceId: 2,
          duration: 3,
          flowDto: {
            elementId: 3,
            unit: 'G'
          },
          id: 1,
          message: 'string',
          objectId: 2,
          pluginId: 4,
          policyConditionId: 4,
          policyId: 2,
          pollId: 5,
          sigmaDirection: 10,
          thresholdId: 4,
          type: 7,
          unit: 'string',
          value: 5,
          value2: 3
        }
      ],
      triggerExpression: 'string',
      triggerMessage: 'string',
      type: 'other',
      useCustomTraps: false,
      useDefaultTraps: true,
      useDeviceTraps: false,
      userEnabled: 10
    };
    describe('#updateThreshold - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateThreshold(thresholdsDeviceId, thresholdsThresholdId, thresholdsUpdateThresholdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'updateThreshold', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThresholdById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getThresholdById(thresholdsDeviceId, thresholdsThresholdId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.absoluteTimes));
                assert.equal(true, data.response.appendConditionMessages);
                assert.equal(true, Array.isArray(data.response.clearConditions));
                assert.equal('string', data.response.clearExpression);
                assert.equal('string', data.response.clearMessage);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.deviceGroup);
                assert.equal(1, data.response.deviceId);
                assert.equal('object', typeof data.response.flowDto);
                assert.equal(6, data.response.groupId);
                assert.equal(3, data.response.id);
                assert.equal(8, data.response.lastUpdated);
                assert.equal(false, data.response.mailOnce);
                assert.equal(9, data.response.mailPeriod);
                assert.equal('string', data.response.mailTo);
                assert.equal('string', data.response.name);
                assert.equal(3, data.response.peerId);
                assert.equal(1, data.response.policyEnabled);
                assert.equal(5, data.response.policyId);
                assert.equal(true, Array.isArray(data.response.relativeTimes));
                assert.equal(3, data.response.severity);
                assert.equal(true, Array.isArray(data.response.trapDestinations));
                assert.equal(true, Array.isArray(data.response.triggerConditions));
                assert.equal('string', data.response.triggerExpression);
                assert.equal('string', data.response.triggerMessage);
                assert.equal('other', data.response.type);
                assert.equal(false, data.response.useCustomTraps);
                assert.equal(true, data.response.useDefaultTraps);
                assert.equal(true, data.response.useDeviceTraps);
                assert.equal(6, data.response.userEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'getThresholdById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thresholdsUpdateThresholdConditionByIdBodyParam = {
      aggregation: 7,
      comparison: 1,
      deviceId: 1,
      duration: 8,
      flowDto: {
        elementId: 10,
        unit: 'G'
      },
      id: 3,
      message: 'string',
      objectId: 10,
      pluginId: 1,
      policyConditionId: 7,
      policyId: 5,
      pollId: 10,
      sigmaDirection: 8,
      thresholdId: 5,
      type: 2,
      unit: 'string',
      value: 5,
      value2: 3
    };
    describe('#updateThresholdConditionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateThresholdConditionById(thresholdsDeviceId, thresholdsThresholdId, thresholdsTriggerType, thresholdsConditionId, thresholdsUpdateThresholdConditionByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'updateThresholdConditionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsId = 555;
    const reportAttachmentsUpdateReportAttachmentByIdBodyParam = {
      name: 'string'
    };
    describe('#updateReportAttachmentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReportAttachmentById(reportAttachmentsId, reportAttachmentsUpdateReportAttachmentByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'updateReportAttachmentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportAttachment(reportAttachmentsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'getReportAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsType = 'fakedata';
    describe('#getAllReportAttachmentEndpoints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllReportAttachmentEndpoints(reportAttachmentsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'getAllReportAttachmentEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReportAttachments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllReportAttachments(reportAttachmentsId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(3, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'getAllReportAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let objectGroupRulesId = 'fakedata';
    const objectGroupRulesCreateObjectGroupRuleBodyParam = {
      attributeId: 9,
      descriptionExpression: 'string',
      deviceGroup: 3,
      deviceType: 1,
      groupId: 9,
      id: 8,
      metadataValueExpression: 'string',
      nameExpression: 'string',
      namespaceId: 2,
      objectTypeId: 10,
      pluginId: 4,
      subtypeId: 1
    };
    describe('#createObjectGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createObjectGroupRule(objectGroupRulesCreateObjectGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.attributeId);
                assert.equal('string', data.response.descriptionExpression);
                assert.equal(9, data.response.deviceGroup);
                assert.equal(6, data.response.deviceType);
                assert.equal(5, data.response.groupId);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.metadataValueExpression);
                assert.equal('string', data.response.nameExpression);
                assert.equal(9, data.response.namespaceId);
                assert.equal(7, data.response.objectTypeId);
                assert.equal(10, data.response.pluginId);
                assert.equal(5, data.response.subtypeId);
              } else {
                runCommonAsserts(data, error);
              }
              objectGroupRulesId = data.response.id;
              saveMockData('ObjectGroupRules', 'createObjectGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyObjectGroupRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applyObjectGroupRules(objectGroupRulesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(4, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'applyObjectGroupRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupsRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupsRules(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(7, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'getObjectGroupsRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupRulesUpdateObjectGroupRuleBodyParam = {
      attributeId: 6,
      descriptionExpression: 'string',
      deviceGroup: 7,
      deviceType: 9,
      groupId: 4,
      id: 10,
      metadataValueExpression: 'string',
      nameExpression: 'string',
      namespaceId: 6,
      objectTypeId: 10,
      pluginId: 8,
      subtypeId: 9
    };
    describe('#updateObjectGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectGroupRule(objectGroupRulesId, objectGroupRulesUpdateObjectGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'updateObjectGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupRule(objectGroupRulesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.attributeId);
                assert.equal('string', data.response.descriptionExpression);
                assert.equal(6, data.response.deviceGroup);
                assert.equal(3, data.response.deviceType);
                assert.equal(8, data.response.groupId);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.metadataValueExpression);
                assert.equal('string', data.response.nameExpression);
                assert.equal(4, data.response.namespaceId);
                assert.equal(5, data.response.objectTypeId);
                assert.equal(4, data.response.pluginId);
                assert.equal(10, data.response.subtypeId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'getObjectGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupRules(objectGroupRulesId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(4, data.response.pageSize);
                assert.equal(8, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'getObjectGroupRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let usersId = 'fakedata';
    const usersCreateUserUsingPOSTBodyParam = {
      authentication: 'RADIUS',
      customTimeout: 8,
      dateFormat: 'string',
      email: 'string',
      firstName: 'string',
      forcePasswordChange: true,
      id: 9,
      isActive: true,
      lastName: 'string',
      password: 'string',
      passwordNeverExpires: false,
      roleIds: [
        2
      ],
      timezone: 'string',
      timezoneStartupCheck: false,
      username: 'string'
    };
    describe('#createUserUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUserUsingPOST(usersCreateUserUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('TACACS', data.response.authentication);
                assert.equal(10, data.response.customTimeout);
                assert.equal('string', data.response.dateFormat);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.firstName);
                assert.equal(true, data.response.forcePasswordChange);
                assert.equal(7, data.response.id);
                assert.equal(true, data.response.isActive);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.password);
                assert.equal(false, data.response.passwordNeverExpires);
                assert.equal(true, Array.isArray(data.response.roleIds));
                assert.equal('string', data.response.timezone);
                assert.equal(false, data.response.timezoneStartupCheck);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              usersId = data.response.id;
              saveMockData('Users', 'createUserUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersFilterUsersBodyParam = {
      active: false,
      email: 'string',
      firstName: 'string',
      ids: [
        4
      ],
      lastName: 'string',
      roleIds: [
        4
      ],
      username: 'string'
    };
    describe('#filterUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterUsers(null, null, null, null, null, usersFilterUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(3, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'filterUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersForcePasswordChangeBodyParam = {
      username: 'string',
      password: 'string',
      newPassword: 'string'
    };
    describe('#forcePasswordChange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.forcePasswordChange(usersForcePasswordChangeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(10, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'forcePasswordChange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUsers(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(10, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(3, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getAllUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateUserPreferencesUsingPATCHBodyParam = {
      dateFormat: 'string',
      email: 'string',
      firstName: 'string',
      lastName: 'string',
      timezone: 'string',
      timezoneStartupCheck: false
    };
    describe('#updateUserPreferencesUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateUserPreferencesUsingPATCH(usersUpdateUserPreferencesUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUserPreferencesUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCurrentUser((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('TACACS', data.response.authentication);
                assert.equal(8, data.response.customTimeout);
                assert.equal('string', data.response.dateFormat);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.firstName);
                assert.equal(false, data.response.forcePasswordChange);
                assert.equal(8, data.response.id);
                assert.equal(false, data.response.isActive);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.password);
                assert.equal(true, data.response.passwordNeverExpires);
                assert.equal(true, Array.isArray(data.response.roleIds));
                assert.equal('string', data.response.timezone);
                assert.equal(true, data.response.timezoneStartupCheck);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getCurrentUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersChangePasswordBodyParam = {
      currentPassword: 'string',
      newPassword: 'string'
    };
    describe('#changePassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changePassword(usersChangePasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'changePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserRoles((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPartiallyUpdateUserUsingPATCHBodyParam = {
      authentication: 'TACACS',
      customTimeout: 4,
      dateFormat: 'string',
      email: 'string',
      firstName: 'string',
      forcePasswordChange: false,
      id: 2,
      isActive: false,
      lastName: 'string',
      password: 'string',
      passwordNeverExpires: false,
      roleIds: [
        1
      ],
      timezone: 'string',
      timezoneStartupCheck: true,
      username: 'string'
    };
    describe('#partiallyUpdateUserUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateUserUsingPATCH(usersId, usersPartiallyUpdateUserUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'partiallyUpdateUserUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserById(usersId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('TACACS', data.response.authentication);
                assert.equal(4, data.response.customTimeout);
                assert.equal('string', data.response.dateFormat);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.firstName);
                assert.equal(true, data.response.forcePasswordChange);
                assert.equal(6, data.response.id);
                assert.equal(true, data.response.isActive);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.password);
                assert.equal(true, data.response.passwordNeverExpires);
                assert.equal(true, Array.isArray(data.response.roleIds));
                assert.equal('string', data.response.timezone);
                assert.equal(false, data.response.timezoneStartupCheck);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersAddSpecificUserRolesBodyParam = [
      9
    ];
    describe('#addSpecificUserRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSpecificUserRoles(usersId, usersAddSpecificUserRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'addSpecificUserRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpecificUserRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSpecificUserRoles(usersId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getSpecificUserRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let metadataNamespaceId = 'fakedata';
    const metadataNamespaceCreateNamespaceBodyParam = {
      id: 6,
      name: 'string'
    };
    describe('#createNamespace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNamespace(metadataNamespaceCreateNamespaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              metadataNamespaceId = data.response.id;
              saveMockData('MetadataNamespace', 'createNamespace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNamespaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNamespaces(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(1, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataNamespace', 'getNamespaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataNamespaceUpdateNamespaceByIdBodyParam = {
      id: 4,
      name: 'string'
    };
    describe('#updateNamespaceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNamespaceById(metadataNamespaceId, metadataNamespaceUpdateNamespaceByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataNamespace', 'updateNamespaceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNamespace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNamespace(metadataNamespaceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataNamespace', 'getNamespace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectGroupsId = 555;
    const reportAttachmentsObjectGroupsCreateObjectGroupAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createObjectGroupAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createObjectGroupAttachment(reportAttachmentsObjectGroupsId, reportAttachmentsObjectGroupsCreateObjectGroupAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'createObjectGroupAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupAttachment(reportAttachmentsObjectGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'getObjectGroupAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectGroupsUpdateObjectGroupAttachmentResourcesBodyParam = {
      deviceId: 5,
      ids: [
        8
      ],
      type: 'ObjectGroup'
    };
    describe('#updateObjectGroupAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectGroupAttachmentResources(reportAttachmentsObjectGroupsId, reportAttachmentsObjectGroupsUpdateObjectGroupAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'updateObjectGroupAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupAttachmentResources(reportAttachmentsObjectGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.deviceId);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('Everything', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'getObjectGroupAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectGroupsUpdateObjectGroupAttachmentVisualizationSettingsBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 9
        },
        table: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: true
        }
      }
    };
    describe('#updateObjectGroupAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectGroupAttachmentVisualizationSettings(reportAttachmentsObjectGroupsId, reportAttachmentsObjectGroupsUpdateObjectGroupAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'updateObjectGroupAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsObjectGroupsPartiallyUpdateObjectGroupAttachmentVisualizationSettingsBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 7
        },
        table: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#partiallyUpdateObjectGroupAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateObjectGroupAttachmentVisualizationSettings(reportAttachmentsObjectGroupsId, reportAttachmentsObjectGroupsPartiallyUpdateObjectGroupAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'partiallyUpdateObjectGroupAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupAttachmentVisualizationSettings(reportAttachmentsObjectGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsObjectGroups', 'getObjectGroupAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let statusMapsId = 'fakedata';
    let statusMapsConnectionId = 555;
    let statusMapsNodeId = 555;
    const statusMapsCreateStatusMapBodyParam = {
      imageId: 8,
      name: 'string'
    };
    describe('#createStatusMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createStatusMap(statusMapsCreateStatusMapBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.connections));
                assert.equal(1, data.response.id);
                assert.equal(2, data.response.imageId);
                assert.equal('string', data.response.imageMimeType);
                assert.equal('string', data.response.imageName);
                assert.equal(4, data.response.imageSize);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.nodes));
              } else {
                runCommonAsserts(data, error);
              }
              statusMapsId = data.response.id;
              statusMapsConnectionId = data.response.id;
              statusMapsNodeId = data.response.id;
              saveMockData('StatusMaps', 'createStatusMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusMapsCreateConnectionBodyParam = {
      data: [
        {}
      ],
      nodeAId: 5,
      nodeBId: 7,
      type: 'Object'
    };
    describe('#createConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createConnection(statusMapsId, statusMapsCreateConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(8, data.response.id);
                assert.equal(10, data.response.nodeAId);
                assert.equal(5, data.response.nodeBId);
                assert.equal('Device', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'createConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusMapsCreateNodeBodyParam = {
      data: [
        {}
      ],
      name: 'string',
      type: 'StatusMap',
      x: 1,
      y: 2
    };
    describe('#createNode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNode(statusMapsId, statusMapsCreateNodeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('Device', data.response.type);
                assert.equal(3, data.response.x);
                assert.equal(1, data.response.y);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'createNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusMaps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusMaps(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(4, data.response.pageSize);
                assert.equal(7, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'getStatusMaps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusMapsUpdateStatusMapByIdBodyParam = {
      imageId: 1,
      name: 'string'
    };
    describe('#updateStatusMapById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateStatusMapById(statusMapsId, statusMapsUpdateStatusMapByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'updateStatusMapById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusMapsPartiallyUpdateStatusMapByIdBodyParam = {
      imageId: 3,
      name: 'string'
    };
    describe('#partiallyUpdateStatusMapById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateStatusMapById(statusMapsId, statusMapsPartiallyUpdateStatusMapByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'partiallyUpdateStatusMapById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusMapById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusMapById(statusMapsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.connections));
                assert.equal(4, data.response.id);
                assert.equal(6, data.response.imageId);
                assert.equal('string', data.response.imageMimeType);
                assert.equal('string', data.response.imageName);
                assert.equal(6, data.response.imageSize);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.nodes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'getStatusMapById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnectionAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnectionAlerts(statusMapsId, statusMapsConnectionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'getConnectionAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnections(statusMapsId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(3, data.response.pageNumber);
                assert.equal(9, data.response.pageSize);
                assert.equal(3, data.response.totalElements);
                assert.equal(10, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'getConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusMapsUpdateConnectionByStatusMapIdBodyParam = {
      data: [
        {}
      ],
      nodeAId: 1,
      nodeBId: 7,
      type: 'Object'
    };
    describe('#updateConnectionByStatusMapId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateConnectionByStatusMapId(statusMapsId, statusMapsConnectionId, statusMapsUpdateConnectionByStatusMapIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'updateConnectionByStatusMapId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodeAlerts(statusMapsId, statusMapsNodeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'getNodeAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusMapNodes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusMapNodes(statusMapsId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(7, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(8, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'getStatusMapNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusMapsUpdateNodeByIdBodyParam = {
      data: [
        {}
      ],
      name: 'string',
      type: 'Device',
      x: 9,
      y: 9
    };
    describe('#updateNodeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNodeById(statusMapsId, statusMapsNodeId, statusMapsUpdateNodeByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'updateNodeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const indicatorsCreateDeviceIndicatorDataBodyParam = [
      {
        deviceId: 1,
        indicatorDataDtos: [
          {
            indicatorId: 3,
            value: 'string'
          },
          {
            indicatorId: 1,
            value: 'string'
          },
          {
            indicatorId: 10,
            value: 'string'
          },
          {
            indicatorId: 2,
            value: 'string'
          },
          {
            indicatorId: 3,
            value: 'string'
          },
          {
            indicatorId: 2,
            value: 'string'
          },
          {
            indicatorId: 10,
            value: 'string'
          }
        ],
        objectId: 9,
        timestamp: 1
      }
    ];
    describe('#createDeviceIndicatorData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceIndicatorData(indicatorsCreateDeviceIndicatorDataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(8, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Indicators', 'createDeviceIndicatorData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const indicatorsDeviceId = 555;
    const indicatorsObjectId = 555;
    describe('#getIndicators - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIndicators(indicatorsDeviceId, indicatorsObjectId, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(1, data.response.pageSize);
                assert.equal(10, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Indicators', 'getIndicators', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const indicatorsIndicatorId = 555;
    describe('#getIndicator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIndicator(indicatorsDeviceId, indicatorsObjectId, indicatorsIndicatorId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dataUnits);
                assert.equal('string', data.response.description);
                assert.equal(7, data.response.deviceId);
                assert.equal('string', data.response.displayUnits);
                assert.equal(2, data.response.evaluationOrder);
                assert.equal('object', typeof data.response.extendedInfo);
                assert.equal(4, data.response.firstSeen);
                assert.equal('COUNTER32', data.response.format);
                assert.equal(3, data.response.id);
                assert.equal(true, data.response.isBaselining);
                assert.equal(true, data.response.isDeleted);
                assert.equal(true, data.response.isEnabled);
                assert.equal(2, data.response.lastInvalidationTime);
                assert.equal(8, data.response.lastSeen);
                assert.equal(10, data.response.maxValue);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.objectId);
                assert.equal(1, data.response.pluginId);
                assert.equal(3, data.response.pluginIndicatorTypeId);
                assert.equal('string', data.response.syntheticExpression);
                assert.equal(false, data.response.systemIsBaselining);
                assert.equal(false, data.response.systemIsEnabled);
                assert.equal(5, data.response.systemMaxValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Indicators', 'getIndicator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const indicatorsStartTime = 555;
    const indicatorsEndTime = 555;
    describe('#getIndicatorData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIndicatorData(indicatorsDeviceId, indicatorsObjectId, indicatorsIndicatorId, indicatorsStartTime, indicatorsEndTime, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Indicators', 'getIndicatorData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesId = 555;
    const reportAttachmentsDevicesCreateDeviceAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createDeviceAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceAttachment(reportAttachmentsDevicesId, reportAttachmentsDevicesCreateDeviceAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'createDeviceAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentFilterSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachmentFilterSchema((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('object', typeof data.response.operations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachmentFilterSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachment(reportAttachmentsDevicesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesUpdateDeviceAttachmentFiltersBodyParam = {
      empty: true,
      expression: 'string',
      filters: [
        {
          id: 'string',
          name: 'string',
          operation: 'string',
          values: [
            'string'
          ]
        }
      ]
    };
    describe('#updateDeviceAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceAttachmentFilters(reportAttachmentsDevicesId, reportAttachmentsDevicesUpdateDeviceAttachmentFiltersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'updateDeviceAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachmentFilters(reportAttachmentsDevicesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.empty);
                assert.equal('string', data.response.expression);
                assert.equal(true, Array.isArray(data.response.filters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesUpdateDeviceAttachmentBodyParam = {
      ids: [
        6
      ],
      type: 'Everything'
    };
    describe('#updateDeviceAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceAttachment(reportAttachmentsDevicesId, reportAttachmentsDevicesUpdateDeviceAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'updateDeviceAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiV2ReportsAttachmentsDevicesIdResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiV2ReportsAttachmentsDevicesIdResources(reportAttachmentsDevicesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('Everything', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getApiV2ReportsAttachmentsDevicesIdResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesUpdateDeviceAttachmentSettingsBodyParam = {
      resultLimit: {
        resultLimit: 3
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#updateDeviceAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceAttachmentSettings(reportAttachmentsDevicesId, reportAttachmentsDevicesUpdateDeviceAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'updateDeviceAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachmentSettings(reportAttachmentsDevicesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resultLimit);
                assert.equal('object', typeof data.response.sourceFields);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDevicesUpdateDeviceAttachmentVisualizationBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: true,
          precision: 1
        },
        table: {
          blockVis: false,
          hideHeaders: false,
          wrapCells: true
        }
      }
    };
    describe('#updateDeviceAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceAttachmentVisualization(reportAttachmentsDevicesId, reportAttachmentsDevicesUpdateDeviceAttachmentVisualizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'updateDeviceAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAttachmentVisualization(reportAttachmentsDevicesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDevices', 'getDeviceAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnitInfos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUnitInfos((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Utils', 'getUnitInfos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const permissionsRoleId = 555;
    const permissionsUpdateAccessPermissionsBodyParam = [
      'SYSTEM_ADMIN'
    ];
    describe('#updateAccessPermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAccessPermissions(permissionsRoleId, permissionsUpdateAccessPermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'updateAccessPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessPermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccessPermissions(permissionsRoleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('MANAGE_OBJGROUPS', data.response[0]);
                assert.equal('VIEW_THRESHOLD', data.response[1]);
                assert.equal('VIEW_LOGDATA', data.response[2]);
                assert.equal('VIEW_NETFLOW', data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'getAccessPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceGroupPermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupPermissions(permissionsRoleId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'updateDeviceGroupPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const permissionsId = 555;
    const permissionsPatchApiV2PermissionsRoleIdDevicegroupIdBodyParam = {
      canEditGroup: true,
      canEditGroupMembers: false,
      canViewGroup: true,
      canViewGroupMembers: false,
      deviceGroupId: 9,
      roleId: 8
    };
    describe('#patchApiV2PermissionsRoleIdDevicegroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchApiV2PermissionsRoleIdDevicegroupId(permissionsRoleId, permissionsId, permissionsPatchApiV2PermissionsRoleIdDevicegroupIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'patchApiV2PermissionsRoleIdDevicegroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiV2PermissionsRoleIdDevicegroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiV2PermissionsRoleIdDevicegroupId(permissionsRoleId, permissionsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.canEditGroup);
                assert.equal(false, data.response.canEditGroupMembers);
                assert.equal(true, data.response.canViewGroup);
                assert.equal(false, data.response.canViewGroupMembers);
                assert.equal(3, data.response.deviceGroupId);
                assert.equal(4, data.response.roleId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'getApiV2PermissionsRoleIdDevicegroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const permissionsUpdatePagePermissionsBodyParam = [
      'NBAR_PAGE'
    ];
    describe('#updatePagePermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePagePermissions(permissionsRoleId, permissionsUpdatePagePermissionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'updatePagePermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPagePermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPagePermissions(permissionsRoleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('THRESHOLD_MGMT', data.response[0]);
                assert.equal('FLOWMON_CONFIG', data.response[1]);
                assert.equal('METADATA_SCHEMA', data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'getPagePermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRolePermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRolePermissions(permissionsRoleId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(1, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'getRolePermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const permissionsPutApiV2PermissionsRoleIdRoleTargetRoleIdBodyParam = {
      roleEdit: true,
      roleId: 2,
      roleView: true,
      targetRoleId: 1,
      userEdit: false,
      userView: true
    };
    describe('#putApiV2PermissionsRoleIdRoleTargetRoleId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putApiV2PermissionsRoleIdRoleTargetRoleId(permissionsRoleId, 555, permissionsPutApiV2PermissionsRoleIdRoleTargetRoleIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'putApiV2PermissionsRoleIdRoleTargetRoleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiV2PermissionsRoleIdRoleTargetRoleId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiV2PermissionsRoleIdRoleTargetRoleId(permissionsRoleId, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.roleEdit);
                assert.equal(4, data.response.roleId);
                assert.equal(true, data.response.roleView);
                assert.equal(1, data.response.targetRoleId);
                assert.equal(false, data.response.userEdit);
                assert.equal(true, data.response.userView);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'getApiV2PermissionsRoleIdRoleTargetRoleId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDeviceGroupsId = 555;
    const reportAttachmentsDeviceGroupsCreateDeviceGroupsAttachmentBodyParam = {
      name: 'string',
      resource: {}
    };
    describe('#createDeviceGroupsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceGroupsAttachment(reportAttachmentsDeviceGroupsId, reportAttachmentsDeviceGroupsCreateDeviceGroupsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'createDeviceGroupsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupAttachment(reportAttachmentsDeviceGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'getDeviceGroupAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDeviceGroupsUpdateDeviceGroupsAttachmentBodyParam = {
      ids: [
        2
      ],
      type: 'Device'
    };
    describe('#updateDeviceGroupsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupsAttachment(reportAttachmentsDeviceGroupsId, reportAttachmentsDeviceGroupsUpdateDeviceGroupsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'updateDeviceGroupsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupsAttachment(reportAttachmentsDeviceGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('DeviceGroup', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'getDeviceGroupsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsDeviceGroupsUpdateDeviceGroupsAttachmentVisualizationBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: true,
          precision: 2
        },
        table: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#updateDeviceGroupsAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupsAttachmentVisualization(reportAttachmentsDeviceGroupsId, reportAttachmentsDeviceGroupsUpdateDeviceGroupsAttachmentVisualizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'updateDeviceGroupsAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupsAttachmentVisualization(reportAttachmentsDeviceGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsDeviceGroups', 'getDeviceGroupsAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let netFlowNetflowDeviceId = 'fakedata';
    let netFlowId = 'fakedata';
    let netFlowMappingId = 555;
    const netFlowCreateDeviceMappingsBodyParam = {
      allow: false,
      automatic: true,
      deviceId: 9,
      id: 5,
      netflowDeviceId: 9
    };
    describe('#createDeviceMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceMappings(netFlowCreateDeviceMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.allow);
                assert.equal(false, data.response.automatic);
                assert.equal(10, data.response.deviceId);
                assert.equal(1, data.response.id);
                assert.equal(9, data.response.netflowDeviceId);
              } else {
                runCommonAsserts(data, error);
              }
              netFlowNetflowDeviceId = data.response.netflowDeviceId;
              netFlowId = data.response.id;
              netFlowMappingId = data.response.id;
              saveMockData('NetFlow', 'createDeviceMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowFilterNetFlowDevicesBodyParam = {
      allowed: false,
      ids: [
        10
      ],
      name: 'string',
      originIp: 'string',
      overrideName: 2,
      peers: [
        1
      ],
      searchText: 'string',
      systemName: 'string',
      versions: 'string',
      visible: 10
    };
    describe('#filterNetFlowDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterNetFlowDevices(null, null, null, null, null, netFlowFilterNetFlowDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(10, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(10, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'filterNetFlowDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowFilterNetFlowDeviceInterfacesBodyParam = {
      allowed: false,
      description: 'string',
      ids: [
        9
      ],
      interfaceNumbers: [
        8
      ],
      name: 'string',
      netflowDeviceIds: [
        8
      ],
      originIps: [
        'string'
      ],
      overrideDescription: 10,
      overrideName: 1,
      overrideSpeed: 6,
      peers: [
        2
      ],
      reason: 'normal',
      searchText: 'string',
      speed: 7,
      systemDescription: 'string',
      systemName: 'string',
      systemSpeed: 2,
      visible: 2
    };
    describe('#filterNetFlowDeviceInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterNetFlowDeviceInterfaces(null, null, null, null, null, netFlowFilterNetFlowDeviceInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(8, data.response.pageNumber);
                assert.equal(7, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(3, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'filterNetFlowDeviceInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowFilterNetflowFieldsBodyParam = {
      byteSize: 5,
      custom: true,
      dataUnits: 'string',
      databaseType: 'string',
      defaultAggregation: 'Sum',
      description: 'string',
      displayUnits: 'string',
      elementIds: [
        5
      ],
      fieldType: 'string',
      ids: [
        10
      ],
      key: false,
      name: 'string',
      supported: true,
      synthetic: false
    };
    describe('#filterNetflowFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterNetflowFields(null, null, null, null, null, netFlowFilterNetflowFieldsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(2, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'filterNetflowFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let netFlowRuleId = 555;
    const netFlowCreateFilterBodyParam = {
      name: 'string',
      reportOnly: true
    };
    describe('#createFilter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFilter(netFlowCreateFilterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.reportOnly);
              } else {
                runCommonAsserts(data, error);
              }
              netFlowRuleId = data.response.id;
              saveMockData('NetFlow', 'createFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowCreateFilterEntitiesBodyParam = {
      fieldId: 7,
      isNegated: false,
      operation: 'string',
      values: [
        'string'
      ]
    };
    describe('#createFilterEntities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFilterEntities(netFlowId, netFlowCreateFilterEntitiesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.fieldId);
                assert.equal(2, data.response.filterId);
                assert.equal(2, data.response.id);
                assert.equal(false, data.response.isNegated);
                assert.equal('string', data.response.operation);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'createFilterEntities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let netFlowViewId = 'fakedata';
    const netFlowCreateMappingsBodyParam = {
      filterId: 6,
      id: 10,
      indicator: {
        deviceId: 6,
        indicatorId: 4,
        objectId: 2
      },
      interface: {
        direction: 2,
        interfaceId: 6,
        netflowDeviceId: 6
      },
      isAllowed: false,
      isAutomatic: true,
      viewId: 6
    };
    describe('#createMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMappings(netFlowCreateMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.filterId);
                assert.equal(2, data.response.id);
                assert.equal('object', typeof data.response.indicator);
                assert.equal('object', typeof data.response.interface);
                assert.equal(true, data.response.isAllowed);
                assert.equal(true, data.response.isAutomatic);
                assert.equal(4, data.response.viewId);
              } else {
                runCommonAsserts(data, error);
              }
              netFlowViewId = data.response.viewId;
              saveMockData('NetFlow', 'createMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowGetMappingByIndicatorBodyParam = [
      {
        deviceId: 9,
        indicatorId: 7,
        objectId: 5
      }
    ];
    describe('#getMappingByIndicator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMappingByIndicator(null, null, null, null, null, null, netFlowGetMappingByIndicatorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(3, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(6, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getMappingByIndicator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowGetMappingByInterfacesBodyParam = [
      {
        direction: 2,
        interfaceId: 4,
        netflowDeviceId: 9
      }
    ];
    describe('#getMappingByInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMappingByInterfaces(null, null, null, null, null, null, netFlowGetMappingByInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(8, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getMappingByInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowCreateSubnetCategoryBodyParam = {
      name: 'string'
    };
    describe('#createSubnetCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSubnetCategory(netFlowCreateSubnetCategoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'createSubnetCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowCreateSubnetBodyParam = {
      ip: 'string',
      name: 'string',
      prefix: 5
    };
    describe('#createSubnet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSubnet(netFlowId, netFlowCreateSubnetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.ip);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.prefix);
                assert.equal(7, data.response.segmentId);
                assert.equal('string', data.response.segmentName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'createSubnet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowCreateApplicationBodyParam = {
      description: 'string',
      destinationIp: 'string',
      destinationPort: 1,
      destinationPortMax: 2,
      id: 10,
      ip: 'string',
      name: 'string',
      port: 7,
      portMax: 6,
      protocol: 1
    };
    describe('#createApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApplication(netFlowCreateApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.destinationIp);
                assert.equal(7, data.response.destinationPort);
                assert.equal(2, data.response.destinationPortMax);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.ip);
                assert.equal('string', data.response.name);
                assert.equal(3, data.response.port);
                assert.equal(5, data.response.portMax);
                assert.equal(4, data.response.protocol);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'createApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowFilterNetflowViewsBodyParam = {
      category: 'string',
      ids: [
        3
      ],
      isAggregated: false,
      isEnabled: false,
      lastUpdate: 4,
      name: 'string',
      searchText: 'string'
    };
    describe('#filterNetflowViews - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterNetflowViews(null, null, null, null, null, netFlowFilterNetflowViewsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(6, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'filterNetflowViews', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowFilterViewIndicatorsBodyParam = {
      byteSize: 4,
      custom: false,
      dataUnits: 'string',
      databaseType: 'string',
      defaultAggregation: 'Max',
      description: 'string',
      displayUnits: 'string',
      elementIds: [
        4
      ],
      fieldType: 'string',
      ids: [
        3
      ],
      key: false,
      name: 'string',
      supported: true,
      synthetic: false
    };
    describe('#filterViewIndicators - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterViewIndicators(netFlowViewId, null, null, null, null, null, netFlowFilterViewIndicatorsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(2, data.response.pageNumber);
                assert.equal(7, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(3, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'filterViewIndicators', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceMappings(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(2, data.response.pageNumber);
                assert.equal(6, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(8, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getDeviceMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowUpdateDeviceMappingsBodyParam = {
      allow: false,
      automatic: true,
      deviceId: 3,
      id: 2,
      netflowDeviceId: 8
    };
    describe('#updateDeviceMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceMappings(netFlowMappingId, netFlowUpdateDeviceMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'updateDeviceMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetFlowDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetFlowDevices(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(1, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getNetFlowDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaces(netFlowNetflowDeviceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowInterfaceId = 555;
    describe('#getDirections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirections(netFlowNetflowDeviceId, netFlowInterfaceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getDirections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowNetFlowInterfaceId = 555;
    const netFlowUpdateInterfaceBodyParam = {
      allowed: true,
      description: 'string',
      id: 1,
      interface: 1,
      name: 'string',
      nameResolved: 'string',
      netflowDeviceId: 1,
      originIp: 'string',
      overrideDescription: 4,
      overrideName: 4,
      overrideSpeed: 2,
      peer: 2,
      reason: 'exceeded_capacity',
      speed: 3,
      systemDescription: 'string',
      systemName: 'string',
      systemSpeed: 7,
      visible: 10
    };
    describe('#updateInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateInterface(netFlowNetflowDeviceId, netFlowNetFlowInterfaceId, netFlowUpdateInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'updateInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetflowFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetflowFields(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(6, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getNetflowFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFilters((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilterById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFilterById(netFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getFilterById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilterEntitiesById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFilterEntitiesById(netFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getFilterEntitiesById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMappings(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(6, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(2, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProtocols - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProtocols((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getProtocols', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSegments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSegments((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getNetworkSegments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubnets((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getSubnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubnetCategoryById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubnetCategoryById(netFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getSubnetCategoryById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubnetsByCategoryId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubnetsByCategoryId(netFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getSubnetsByCategoryId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesByPort - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesByPort((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getServicesByPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowUpdateApplicationBodyParam = {
      description: 'string',
      destinationIp: 'string',
      destinationPort: 5,
      destinationPortMax: 10,
      id: 7,
      ip: 'string',
      name: 'string',
      port: 8,
      portMax: 9,
      protocol: 8
    };
    describe('#updateApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateApplication(netFlowId, netFlowUpdateApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'updateApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetFlowModes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetFlowModes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.aggregatedNetFlowEnabled);
                assert.equal(true, data.response.rawNetFlowEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getNetFlowModes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getViews - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getViews(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(3, data.response.pageNumber);
                assert.equal(7, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(6, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getViews', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetflowCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetflowCategories(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(5, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(10, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getNetflowCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getViewIndicators - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getViewIndicators(netFlowViewId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.defaultSortOrder);
                assert.equal(true, Array.isArray(data.response.keys));
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal(true, Array.isArray(data.response.metrics));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getViewIndicators', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReportColumnsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReportColumnsUsingGET(netFlowViewId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getReportColumnsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let maintenanceWindowsId = 'fakedata';
    const maintenanceWindowsCreateMaintenanceWindowBodyParam = {
      name: 'string',
      scheduleInstance: {}
    };
    describe('#createMaintenanceWindow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMaintenanceWindow(maintenanceWindowsCreateMaintenanceWindowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.actions));
                assert.equal(true, Array.isArray(data.response.deviceIds));
                assert.equal('438038f2-49b0-4b77-8460-6917decdbe7c', data.response.id);
                assert.equal('PLANNED', data.response.maintenanceType);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.notes);
                assert.equal('object', typeof data.response.scheduleInstance);
              } else {
                runCommonAsserts(data, error);
              }
              maintenanceWindowsId = data.response.id;
              saveMockData('MaintenanceWindows', 'createMaintenanceWindow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsPostApiV2MaintenancewindowsDeviceGroupBodyParam = {
      name: 'string',
      scheduleInstance: {}
    };
    describe('#postApiV2MaintenancewindowsDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postApiV2MaintenancewindowsDeviceGroup(maintenanceWindowsPostApiV2MaintenancewindowsDeviceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.actions));
                assert.equal('438038f2-49b0-4b77-8460-6917decdbe7c', data.response.id);
                assert.equal('PLANNED', data.response.maintenanceType);
                assert.equal(true, Array.isArray(data.response.mappedEntities));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.notes);
                assert.equal('object', typeof data.response.scheduleInstance);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'postApiV2MaintenancewindowsDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsFilterMaintenanceWindowsBodyParam = {
      actions: [
        'EXCLUDE_DATA_FROM_BASELINES'
      ],
      beginDateAfter: '1761150600000',
      beginDateBefore: '1761150600000',
      createDateAfter: '1761149700000',
      createDateBefore: '1761149700000',
      deviceIds: [
        6
      ],
      endDateAfter: '1761149700000',
      endDateBefore: '1761149700000',
      isRetroactive: true,
      name: 'string'
    };
    describe('#filterMaintenanceWindows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterMaintenanceWindows(null, null, null, maintenanceWindowsFilterMaintenanceWindowsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(9, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'filterMaintenanceWindows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsPostApiV2MaintenancewindowsFilterallBodyParam = {
      actions: [
        'CATEGORIZE_ALERTS'
      ],
      beginDateAfter: '1761150600000',
      beginDateBefore: '1761150600000',
      createDateAfter: '1761149700000',
      createDateBefore: '1761149700000',
      deviceIds: [
        8
      ],
      endDateAfter: '1761149700000',
      endDateBefore: '1761149700000',
      isRetroactive: true,
      name: 'string'
    };
    describe('#postApiV2MaintenancewindowsFilterall - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postApiV2MaintenancewindowsFilterall(null, null, null, maintenanceWindowsPostApiV2MaintenancewindowsFilterallBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(2, data.response.pageNumber);
                assert.equal(6, data.response.pageSize);
                assert.equal(7, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'postApiV2MaintenancewindowsFilterall', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaintenanceWindows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMaintenanceWindows(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(9, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(3, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'getMaintenanceWindows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsUpdateGroupMaintenanceWindowUsingPUTBodyParam = {
      name: 'string',
      scheduleInstance: {}
    };
    describe('#updateGroupMaintenanceWindowUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupMaintenanceWindowUsingPUT(maintenanceWindowsId, maintenanceWindowsUpdateGroupMaintenanceWindowUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'updateGroupMaintenanceWindowUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceWindowsUpdateMaintenanceWindowByIdBodyParam = {
      name: 'string',
      scheduleInstance: {}
    };
    describe('#updateMaintenanceWindowById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMaintenanceWindowById(maintenanceWindowsId, maintenanceWindowsUpdateMaintenanceWindowByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'updateMaintenanceWindowById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaintenanceWindowById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMaintenanceWindowById(maintenanceWindowsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(6, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'getMaintenanceWindowById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsMetadataId = 555;
    const reportAttachmentsMetadataCreateMetadataAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createMetadataAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMetadataAttachment(reportAttachmentsMetadataId, reportAttachmentsMetadataCreateMetadataAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'createMetadataAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetadataAttachment(reportAttachmentsMetadataId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'getMetadataAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsMetadataUpdateMetadataAttachmentResourcesBodyParam = {
      deviceId: 5,
      ids: [
        9
      ],
      type: 'ObjectGroup'
    };
    describe('#updateMetadataAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMetadataAttachmentResources(reportAttachmentsMetadataId, reportAttachmentsMetadataUpdateMetadataAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'updateMetadataAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetadataAttachmentResources(reportAttachmentsMetadataId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.deviceId);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal('Object', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'getMetadataAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsMetadataUpdateMetadataAttachmentVisualizationSettingsBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 7
        },
        table: {
          blockVis: false,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#updateMetadataAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMetadataAttachmentVisualizationSettings(reportAttachmentsMetadataId, reportAttachmentsMetadataUpdateMetadataAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'updateMetadataAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsMetadataPartiallyUpdateMetadataAttachmentVisualizationSettingsBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 6
        },
        table: {
          blockVis: true,
          hideHeaders: true,
          wrapCells: false
        }
      }
    };
    describe('#partiallyUpdateMetadataAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateMetadataAttachmentVisualizationSettings(reportAttachmentsMetadataId, reportAttachmentsMetadataPartiallyUpdateMetadataAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'partiallyUpdateMetadataAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetadataAttachmentVisualizationSettings(reportAttachmentsMetadataId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsMetadata', 'getMetadataAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let metadataAttributeId = 'fakedata';
    const metadataAttributeCreateAttributeBodyParam = {
      id: 1,
      name: 'string',
      type: 'integer',
      entityTypes: [
        'indicatortype'
      ],
      singleton: true,
      validationExpression: 'string',
      namespaceId: 5,
      namespaceName: 'string'
    };
    describe('#createAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAttribute(metadataAttributeCreateAttributeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('latLong', data.response.type);
                assert.equal(true, Array.isArray(data.response.entityTypes));
                assert.equal(true, data.response.singleton);
                assert.equal('string', data.response.validationExpression);
                assert.equal(2, data.response.namespaceId);
                assert.equal('string', data.response.namespaceName);
              } else {
                runCommonAsserts(data, error);
              }
              metadataAttributeId = data.response.id;
              saveMockData('MetadataAttribute', 'createAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataAttributeFilterAttributesBodyParam = {
      entityType: 'indicatortype',
      ids: [
        9
      ],
      name: 'string',
      namespaceIds: [
        2
      ],
      singleton: false,
      type: [
        'ip'
      ],
      validationExpression: 'string'
    };
    describe('#filterAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterAttributes(null, null, null, null, null, metadataAttributeFilterAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(5, data.response.pageNumber);
                assert.equal(7, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(2, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataAttribute', 'filterAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAttributes(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(2, data.response.pageNumber);
                assert.equal(10, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataAttribute', 'getAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataAttributeUpdateAttributeByIdBodyParam = {
      id: 8,
      name: 'string',
      type: 'integer',
      entityTypes: [
        'objectgroup'
      ],
      singleton: false,
      validationExpression: 'string',
      namespaceId: 9,
      namespaceName: 'string'
    };
    describe('#updateAttributeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAttributeById(metadataAttributeId, metadataAttributeUpdateAttributeByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataAttribute', 'updateAttributeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAttribute(metadataAttributeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('integer', data.response.type);
                assert.equal(true, Array.isArray(data.response.entityTypes));
                assert.equal(false, data.response.singleton);
                assert.equal('string', data.response.validationExpression);
                assert.equal(2, data.response.namespaceId);
                assert.equal('string', data.response.namespaceName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataAttribute', 'getAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let topologyId = 'fakedata';
    const topologyCreateConstraintBodyParam = {
      forwardCardinality: 'ONE',
      forwardRelationshipType: 'LAYERED_OVER',
      id: 3,
      localObjectTypeId: 3,
      remoteObjectTypeId: 1,
      reverseCardinality: 'ONE',
      reverseRelationshipType: 'PEER'
    };
    describe('#createConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createConstraint(topologyCreateConstraintBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ONE', data.response.forwardCardinality);
                assert.equal('LAYERED_OVER', data.response.forwardRelationshipType);
                assert.equal(10, data.response.id);
                assert.equal(6, data.response.localObjectTypeId);
                assert.equal(2, data.response.remoteObjectTypeId);
                assert.equal('ONE', data.response.reverseCardinality);
                assert.equal('UNDERLYING', data.response.reverseRelationshipType);
              } else {
                runCommonAsserts(data, error);
              }
              topologyId = data.response.id;
              saveMockData('Topology', 'createConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyFilterTopologyLinksBodyParam = {
      connectionId: 9,
      deviceIds: [
        5
      ],
      objectIds: [
        {
          deviceId: 2,
          objectId: 9
        }
      ],
      relationshipType: 'COMPOSED_OF',
      sourceName: 'STP'
    };
    describe('#filterTopologyLinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterTopologyLinks(null, null, null, null, null, topologyFilterTopologyLinksBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(9, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(10, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'filterTopologyLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let topologyConnectionId = 'fakedata';
    const topologyCreateTopologyLinkBodyParam = {
      aDeviceId: 6,
      aObjectId: 5,
      connectionId: 8,
      isAutomatic: true,
      isDiscovered: false,
      relationType: 'CONNECTED_VIA',
      sourceName: 'CISCOACI_PHYSICAL',
      zDeviceId: 2,
      zObjectId: 7
    };
    describe('#createTopologyLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTopologyLink(topologyCreateTopologyLinkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.aDeviceId);
                assert.equal(1, data.response.aObjectId);
                assert.equal(5, data.response.connectionId);
                assert.equal(true, data.response.isAutomatic);
                assert.equal(true, data.response.isDiscovered);
                assert.equal('MEMBER_OF', data.response.relationType);
                assert.equal('CISCOACI_PHYSICAL', data.response.sourceName);
                assert.equal(5, data.response.zDeviceId);
                assert.equal(7, data.response.zObjectId);
              } else {
                runCommonAsserts(data, error);
              }
              topologyConnectionId = data.response.connectionId;
              saveMockData('Topology', 'createTopologyLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConstraints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConstraints(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(4, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(5, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getConstraints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyUpdateConstraintByIdBodyParam = {
      forwardCardinality: 'ONE',
      forwardRelationshipType: 'PEER',
      id: 10,
      localObjectTypeId: 4,
      remoteObjectTypeId: 2,
      reverseCardinality: 'ONE',
      reverseRelationshipType: 'CONNECTED_VIA'
    };
    describe('#updateConstraintById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateConstraintById(topologyId, topologyUpdateConstraintByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'updateConstraintById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConstraint(topologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ONE', data.response.forwardCardinality);
                assert.equal('UNDERLYING', data.response.forwardRelationshipType);
                assert.equal(4, data.response.id);
                assert.equal(4, data.response.localObjectTypeId);
                assert.equal(2, data.response.remoteObjectTypeId);
                assert.equal('ONE', data.response.reverseCardinality);
                assert.equal('PEER', data.response.reverseRelationshipType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyDeviceId = 555;
    describe('#getTopologyFullLinkByDeviceId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopologyFullLinkByDeviceId(topologyDeviceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyFullLinkByDeviceId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyFullLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopologyFullLink(topologyConnectionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyFullLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runReportAttachmentsRunAlertReportBodyParam = {
      name: 'string'
    };
    describe('#runAlertReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runAlertReport(runReportAttachmentsRunAlertReportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.result);
                assert.equal(true, Array.isArray(data.response.timeRanges));
                assert.equal('TOPOLOGY', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runAlertReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runReportAttachmentsCreateFlowFalconPMBodyParam = {
      dataColumns: {
        columns: [
          {
            key: 7,
            value: 3
          }
        ],
        order: 'string',
        sortCol: {
          key: 1,
          value: 6
        }
      },
      endTime: 2,
      filters: [
        {
          field: 9,
          not: true,
          operator: 'Subnet',
          value: {
            value1: 'string',
            value2: 'string'
          }
        }
      ],
      flowFalcon: {
        canZoomInCb: 'string',
        granularity: 2,
        graphOther: true,
        isRate: false,
        isTotal: false,
        split: 2,
        subnetCategoryId: 7
      },
      keyFields: [
        10
      ],
      keyLines: [
        [
          'string'
        ]
      ],
      resource: {
        groups: {
          ids: [
            4
          ],
          type: 'ObjectGroup'
        },
        interfaces: [
          {
            direction: 3,
            interfaceNum: 5,
            sourceIp: 'string'
          }
        ]
      },
      startTime: 4,
      template: {
        isAggregated: false,
        metricId: 1,
        viewId: 4
      },
      units: {
        percentage: false,
        preferredUnits: 'bits'
      }
    };
    describe('#createFlowFalconPM - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFlowFalconPM(runReportAttachmentsCreateFlowFalconPMBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.keyHeaders));
                assert.equal(10, data.response.maximumSampleRate);
                assert.equal(false, data.response.noTemplateMatch);
                assert.equal(true, Array.isArray(data.response.nodes));
                assert.equal('object', typeof data.response.otherTraffic);
                assert.equal('object', typeof data.response.totalTraffic);
                assert.equal(true, Array.isArray(data.response.valueHeaders));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'createFlowFalconPM', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runReportAttachmentsRunFlowFalconReportBodyParam = {
      name: 'string',
      resource: {},
      settings: {},
      time: {}
    };
    describe('#runFlowFalconReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runFlowFalconReport(runReportAttachmentsRunFlowFalconReportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.performanceMetrics);
                assert.equal(true, Array.isArray(data.response.timeRanges));
                assert.equal('object', typeof data.response.topN);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runFlowFalconReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runReportAttachmentsCreateFlowFalconTopNBodyParam = {
      dataColumns: {
        columns: [
          {
            key: 5,
            value: 8
          }
        ],
        order: 'string',
        sortCol: {
          key: 10,
          value: 5
        }
      },
      endTime: 7,
      filters: [
        {
          field: 1,
          not: false,
          operator: 'Gt',
          value: {
            value1: 'string',
            value2: 'string'
          }
        }
      ],
      flowFalcon: {
        canZoomInCb: 'string',
        granularity: 9,
        graphOther: false,
        isRate: false,
        isTotal: false,
        split: 9,
        subnetCategoryId: 2
      },
      keyFields: [
        8
      ],
      resolution: {
        showAs: 'string',
        showDns: 'string',
        showDscp: 'string',
        showPort: 'string',
        showProtocol: 'string'
      },
      resource: {
        groups: {
          ids: [
            9
          ],
          type: 'DeviceGroup'
        },
        interfaces: [
          {
            direction: 2,
            interfaceNum: 1,
            sourceIp: 'string'
          }
        ]
      },
      resultLimit: {
        resultLimit: 1
      },
      startTime: 3,
      template: {
        isAggregated: false,
        metricId: 1,
        viewId: 2
      },
      units: {
        percentage: false,
        preferredUnits: 'bits'
      }
    };
    describe('#createFlowFalconTopN - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFlowFalconTopN(runReportAttachmentsCreateFlowFalconTopNBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.keyHeaders));
                assert.equal(7, data.response.maximumSampleRate);
                assert.equal(true, data.response.noTemplateMatch);
                assert.equal(true, Array.isArray(data.response.nodes));
                assert.equal('object', typeof data.response.otherTraffic);
                assert.equal('object', typeof data.response.totalTraffic);
                assert.equal(true, Array.isArray(data.response.valueHeaders));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'createFlowFalconTopN', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runReportAttachmentsRunGroupMetricsAttachmentBodyParam = {
      resource: {
        ids: [
          9
        ],
        indicatorTypes: [
          {
            aggregations: [
              'max'
            ],
            ids: [
              7
            ],
            objectTypeId: 9,
            pluginId: 8
          }
        ],
        type: 'DeviceGroupChildren'
      },
      settings: {
        childrenReport: true,
        preferredUnits: 'bytes',
        sourceFields: {
          fields: [
            'string'
          ],
          sort: [
            [
              'string'
            ]
          ]
        }
      },
      time: {
        ranges: {
          end: 'string',
          options: {},
          start: 'string',
          type: 'RelativePeriod'
        },
        timezone: 'string'
      },
      visualization: {
        table: {
          column: {
            exclude: [
              'string'
            ],
            include: [
              'string'
            ]
          },
          dataPresentation: {
            formatNumbers: false,
            precision: 2
          },
          table: {
            blockVis: false,
            hideHeaders: false,
            wrapCells: false
          }
        }
      }
    };
    describe('#runGroupMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runGroupMetricsAttachment(runReportAttachmentsRunGroupMetricsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('NETFLOW', data.response.attachmentType);
                assert.equal(true, Array.isArray(data.response.result));
                assert.equal(true, Array.isArray(data.response.timeRanges));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runGroupMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runReportAttachmentsRunPerformanceMetricsAttachmentBodyParam = {
      name: 'string'
    };
    describe('#runPerformanceMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runPerformanceMetricsAttachment(null, null, null, runReportAttachmentsRunPerformanceMetricsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('PERFORMANCE_METRICS', data.response.attachmentType);
                assert.equal(true, Array.isArray(data.response.result));
                assert.equal(true, Array.isArray(data.response.timeRanges));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runPerformanceMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runReportAttachmentsRunTopNAttachmentBodyParam = {
      resource: {
        ids: [
          {}
        ],
        memberOfType: 'ANY',
        type: 'Object'
      },
      settings: {
        aggregation: {
          aggregationUnits: 'weekly'
        },
        resultLimit: {
          resultLimit: 7
        },
        topN: {
          capacityThreshold: {
            operator: 'GREATERTHAN',
            units: 'string',
            value: 5
          },
          displayOnlyExceedingObjects: false,
          displayRValues: true,
          sortBy: 8,
          sortOrder: 'ASC',
          templateId: 8
        },
        units: {
          percentage: true,
          preferredUnits: 'bits'
        },
        workHours: {
          type: 'none'
        }
      },
      time: {
        ranges: [
          {
            end: 'string',
            options: {},
            start: 'string',
            type: 'RelativePeriod'
          }
        ],
        timezone: 'string'
      }
    };
    describe('#runTopNAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runTopNAttachment(null, runReportAttachmentsRunTopNAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('PERFORMANCE_METRICS', data.response.attachmentType);
                assert.equal(true, Array.isArray(data.response.result));
                assert.equal(true, Array.isArray(data.response.timeRanges));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runTopNAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runReportAttachmentsRunTopologyReportBodyParam = {
      name: 'string'
    };
    describe('#runTopologyReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runTopologyReport(null, null, runReportAttachmentsRunTopologyReportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.result);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runTopologyReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const runReportAttachmentsId = 555;
    describe('#runAlertAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runAlertAttachment(runReportAttachmentsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.result);
                assert.equal(true, Array.isArray(data.response.timeRanges));
                assert.equal('PERFORMANCE_METRICS', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runAlertAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runFlowFalconAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runFlowFalconAttachment(runReportAttachmentsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runFlowFalconAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runGroupMetricsAttachmentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runGroupMetricsAttachmentById(runReportAttachmentsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('NETFLOW', data.response.attachmentType);
                assert.equal(true, Array.isArray(data.response.result));
                assert.equal(true, Array.isArray(data.response.timeRanges));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runGroupMetricsAttachmentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runPerformanceMetricsAttachmentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runPerformanceMetricsAttachmentById(runReportAttachmentsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('TOPOLOGY', data.response.attachmentType);
                assert.equal(true, Array.isArray(data.response.result));
                assert.equal(true, Array.isArray(data.response.timeRanges));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runPerformanceMetricsAttachmentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiV2ReportsAttachmentsTopnIdRun - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiV2ReportsAttachmentsTopnIdRun(runReportAttachmentsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('PERFORMANCE_METRICS', data.response.attachmentType);
                assert.equal(true, Array.isArray(data.response.result));
                assert.equal(true, Array.isArray(data.response.timeRanges));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'getApiV2ReportsAttachmentsTopnIdRun', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runTopologyAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runTopologyAttachment(runReportAttachmentsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.result);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RunReportAttachments', 'runTopologyAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsId = 555;
    const reportAttachmentsPerformanceMetricsCreatePerformanceMetricsAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createPerformanceMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPerformanceMetricsAttachment(reportAttachmentsPerformanceMetricsId, reportAttachmentsPerformanceMetricsCreatePerformanceMetricsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.resources));
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'createPerformanceMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformanceMetricsAttachment(reportAttachmentsPerformanceMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.resources));
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'getPerformanceMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentResourcesBodyParam = [
      {
        group: {
          id: 3,
          indicatorTypes: {
            ids: [
              10
            ],
            objectTypeId: 1,
            pluginId: 8
          },
          type: 'ObjectGroup'
        },
        indicators: [
          {
            deviceId: 2,
            ids: [
              3,
              2,
              10,
              4,
              8,
              9,
              3,
              8,
              4
            ],
            objectId: 6,
            pluginId: 8
          },
          {
            deviceId: 8,
            ids: [
              8
            ],
            objectId: 3,
            pluginId: 3
          },
          {
            deviceId: 8,
            ids: [
              9,
              10,
              7,
              5,
              8
            ],
            objectId: 6,
            pluginId: 8
          },
          {
            deviceId: 4,
            ids: [
              7,
              3,
              6,
              5,
              9
            ],
            objectId: 7,
            pluginId: 4
          },
          {
            deviceId: 3,
            ids: [
              5,
              1
            ],
            objectId: 4,
            pluginId: 5
          },
          {
            deviceId: 7,
            ids: [
              5,
              8,
              7,
              4,
              5,
              8,
              4,
              7,
              2,
              9
            ],
            objectId: 5,
            pluginId: 10
          },
          {
            deviceId: 1,
            ids: [
              10,
              5,
              6,
              10,
              1,
              5,
              9,
              7
            ],
            objectId: 6,
            pluginId: 3
          },
          {
            deviceId: 3,
            ids: [
              2,
              10,
              5,
              4,
              4,
              7,
              10
            ],
            objectId: 5,
            pluginId: 7
          }
        ]
      }
    ];
    describe('#updatePerformanceMetricsAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentResources(reportAttachmentsPerformanceMetricsId, reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentResourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'updatePerformanceMetricsAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformanceMetricsAttachmentResources(reportAttachmentsPerformanceMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'getPerformanceMetricsAttachmentResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentSettingsBodyParam = {
      rawData: {
        csv: {
          expandTimeseries: false,
          formatTimestamp: false
        },
        dataAggregation: {
          aggregationType: {},
          aggregationUnits: {},
          aggregationValue: 2,
          useAggregation: false
        },
        rawData: {
          fitTimeSpanTo: {},
          percentiles: 8,
          projectionTime: 4,
          reduceData: true,
          standardDeviation: 6,
          timeOverTimeOnly: true,
          timeOverTimeType: {},
          timeOverTimeUnits: {},
          timeOverTimeValue: 7,
          trend: {},
          trendType: {},
          useBaseline: false,
          usePercentiles: true,
          useTimeOverTime: false
        },
        units: {
          percentage: true,
          preferredUnits: 'bytes'
        },
        workHours: {
          customWorkhours: [
            {
              days: {},
              startHour: {},
              startMin: {},
              endHour: {},
              endMin: {}
            }
          ],
          type: 'system',
          workHoursGroupId: 3
        }
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#updatePerformanceMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentSettings(reportAttachmentsPerformanceMetricsId, reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'updatePerformanceMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsPartiallyUpdatePerformanceMetricsAttachmentSettingsBodyParam = {
      rawData: {
        csv: {
          expandTimeseries: false,
          formatTimestamp: true
        },
        dataAggregation: {
          aggregationType: {},
          aggregationUnits: {},
          aggregationValue: 9,
          useAggregation: true
        },
        rawData: {
          fitTimeSpanTo: {},
          percentiles: 3,
          projectionTime: 7,
          reduceData: false,
          standardDeviation: 6,
          timeOverTimeOnly: true,
          timeOverTimeType: {},
          timeOverTimeUnits: {},
          timeOverTimeValue: 9,
          trend: {},
          trendType: {},
          useBaseline: true,
          usePercentiles: true,
          useTimeOverTime: true
        },
        units: {
          percentage: true,
          preferredUnits: 'bytes'
        },
        workHours: {
          customWorkhours: [
            {
              days: {},
              startHour: {},
              startMin: {},
              endHour: {},
              endMin: {}
            }
          ],
          type: 'group',
          workHoursGroupId: 7
        }
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#partiallyUpdatePerformanceMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdatePerformanceMetricsAttachmentSettings(reportAttachmentsPerformanceMetricsId, reportAttachmentsPerformanceMetricsPartiallyUpdatePerformanceMetricsAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'partiallyUpdatePerformanceMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformanceMetricsAttachmentSettings(reportAttachmentsPerformanceMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.rawData);
                assert.equal('object', typeof data.response.sourceFields);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'getPerformanceMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        options: {},
        start: 'string',
        type: 'CustomWeek'
      },
      timezone: 'string'
    };
    describe('#updatePerformanceMetricsAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentTimeSettings(reportAttachmentsPerformanceMetricsId, reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'updatePerformanceMetricsAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformanceMetricsAttachmentTimeSettings(reportAttachmentsPerformanceMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'getPerformanceMetricsAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentVisualizationSettingsBodyParam = {
      bar: {
        drawGraphOutline: true,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: false,
        showAggregation: false,
        showAverage: true,
        showFrequency: true,
        showLast: false,
        showLegend: true,
        showPeak: false,
        showTimespan: true,
        showTitle: false,
        showValley: false
      },
      csv: {
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: false
        }
      },
      line: {
        drawGraphOutline: false,
        dualYAxes: false,
        logScale: false,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: false,
        scaled: true,
        showAggregation: false,
        showAverage: true,
        showFrequency: true,
        showLast: false,
        showLegend: false,
        showPeak: true,
        showTimespan: true,
        showTitle: true,
        showValley: false
      },
      pie: {
        showAggregation: true,
        showAverage: true,
        showFrequency: false,
        showLast: true,
        showLegend: false,
        showPeak: true,
        showTimespan: false,
        showTitle: false,
        showValley: true
      },
      radial: {
        drawGraphOutline: false,
        scaled: true,
        showAggregation: false,
        showAverage: false,
        showFrequency: false,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: true,
        showTitle: true,
        showValley: false
      },
      stackedBar: {
        drawGraphOutline: true,
        prettyXAxis: false,
        prettyYAxis: true,
        scaled: true,
        showAggregation: false,
        showAverage: true,
        showFrequency: true,
        showLast: true,
        showLegend: true,
        showPeak: true,
        showTimespan: false,
        showTitle: false,
        showValley: true
      },
      stackedLine: {
        drawGraphOutline: false,
        prettyXAxis: false,
        prettyYAxis: true,
        scaleToMinimumValue: true,
        scaled: true,
        sendGraphCSV: false,
        showAggregation: true,
        showAverage: true,
        showFrequency: true,
        showLast: false,
        showLegend: true,
        showPeak: false,
        showTimespan: true,
        showTitle: false,
        showValley: true
      },
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 4
        },
        table: {
          blockVis: true,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#updatePerformanceMetricsAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePerformanceMetricsAttachmentVisualizationSettings(reportAttachmentsPerformanceMetricsId, reportAttachmentsPerformanceMetricsUpdatePerformanceMetricsAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'updatePerformanceMetricsAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsPerformanceMetricsPartiallyUpdatePerformanceMetricsAttachmentVisualizationSettingsBodyParam = {
      bar: {
        drawGraphOutline: true,
        prettyXAxis: false,
        prettyYAxis: false,
        scaleToMinimumValue: false,
        scaled: true,
        showAggregation: false,
        showAverage: true,
        showFrequency: true,
        showLast: false,
        showLegend: false,
        showPeak: false,
        showTimespan: true,
        showTitle: true,
        showValley: true
      },
      csv: {
        csvSetting: {
          expandTimeseries: false,
          formatTimestamp: true
        }
      },
      line: {
        drawGraphOutline: false,
        dualYAxes: false,
        logScale: true,
        prettyXAxis: true,
        prettyYAxis: true,
        scaleToMinimumValue: false,
        scaled: true,
        showAggregation: true,
        showAverage: true,
        showFrequency: false,
        showLast: false,
        showLegend: false,
        showPeak: true,
        showTimespan: true,
        showTitle: false,
        showValley: true
      },
      pie: {
        showAggregation: false,
        showAverage: false,
        showFrequency: false,
        showLast: false,
        showLegend: false,
        showPeak: false,
        showTimespan: false,
        showTitle: false,
        showValley: false
      },
      radial: {
        drawGraphOutline: false,
        scaled: false,
        showAggregation: false,
        showAverage: false,
        showFrequency: true,
        showLast: true,
        showLegend: true,
        showPeak: false,
        showTimespan: true,
        showTitle: false,
        showValley: true
      },
      stackedBar: {
        drawGraphOutline: true,
        prettyXAxis: false,
        prettyYAxis: false,
        scaled: true,
        showAggregation: true,
        showAverage: true,
        showFrequency: true,
        showLast: false,
        showLegend: true,
        showPeak: true,
        showTimespan: false,
        showTitle: true,
        showValley: true
      },
      stackedLine: {
        drawGraphOutline: false,
        prettyXAxis: false,
        prettyYAxis: true,
        scaleToMinimumValue: false,
        scaled: false,
        sendGraphCSV: false,
        showAggregation: true,
        showAverage: false,
        showFrequency: false,
        showLast: true,
        showLegend: false,
        showPeak: false,
        showTimespan: false,
        showTitle: false,
        showValley: true
      },
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 9
        },
        table: {
          blockVis: true,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings(reportAttachmentsPerformanceMetricsId, reportAttachmentsPerformanceMetricsPartiallyUpdatePerformanceMetricsAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'partiallyUpdatePerformanceMetricsAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPerformanceMetricsAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPerformanceMetricsAttachmentVisualizationSettings(reportAttachmentsPerformanceMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.bar);
                assert.equal('object', typeof data.response.csv);
                assert.equal('object', typeof data.response.line);
                assert.equal('object', typeof data.response.pie);
                assert.equal('object', typeof data.response.radial);
                assert.equal('object', typeof data.response.stackedBar);
                assert.equal('object', typeof data.response.stackedLine);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsPerformanceMetrics', 'getPerformanceMetricsAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsId = 555;
    const reportAttachmentsGroupMetricsCreateGroupMetricsAttachmentBodyParam = {
      name: 'string',
      resource: {}
    };
    describe('#createGroupMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGroupMetricsAttachment(reportAttachmentsGroupMetricsId, reportAttachmentsGroupMetricsCreateGroupMetricsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'createGroupMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupMetricsAttachment(reportAttachmentsGroupMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'getGroupMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentBodyParam = {
      indicatorTypes: [
        {}
      ],
      type: 'ObjectGroup'
    };
    describe('#updateGroupMetricsAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupMetricsAttachment(reportAttachmentsGroupMetricsId, reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'updateGroupMetricsAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiV2ReportsAttachmentsGroupMetricsIdResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiV2ReportsAttachmentsGroupMetricsIdResources(reportAttachmentsGroupMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ids));
                assert.equal(true, Array.isArray(data.response.indicatorTypes));
                assert.equal('DeviceGroupChildren', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'getApiV2ReportsAttachmentsGroupMetricsIdResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentSettingsBodyParam = {
      childrenReport: false,
      preferredUnits: 'bits',
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#updateGroupMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupMetricsAttachmentSettings(reportAttachmentsGroupMetricsId, reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'updateGroupMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupMetricsAttachmentSettings(reportAttachmentsGroupMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.childrenReport);
                assert.equal('bytes', data.response.preferredUnits);
                assert.equal('object', typeof data.response.sourceFields);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'getGroupMetricsAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        options: {},
        start: 'string',
        type: 'CustomWeek'
      },
      timezone: 'string'
    };
    describe('#updateGroupMetricsAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupMetricsAttachmentTimeSettings(reportAttachmentsGroupMetricsId, reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'updateGroupMetricsAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupMetricsAttachmentTimeSettings(reportAttachmentsGroupMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'getGroupMetricsAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentVisualizationBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: true,
          precision: 6
        },
        table: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: true
        }
      }
    };
    describe('#updateGroupMetricsAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupMetricsAttachmentVisualization(reportAttachmentsGroupMetricsId, reportAttachmentsGroupMetricsUpdateGroupMetricsAttachmentVisualizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'updateGroupMetricsAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupMetricsAttachmentVisualization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupMetricsAttachmentVisualization(reportAttachmentsGroupMetricsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsGroupMetrics', 'getGroupMetricsAttachmentVisualization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationSignInBodyParam = {
      name: 'string',
      password: 'string'
    };
    describe('#signIn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.signIn(null, authenticationSignInBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.token);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'signIn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveSessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActiveSessions(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'getActiveSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#keepAlive - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.keepAlive((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'keepAlive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tagsName = 'fakedata';
    describe('#getTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTags(tagsName, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(1, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'getTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiV2TagsNameIndicatortypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiV2TagsNameIndicatortypes(tagsName, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(3, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(10, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'getApiV2TagsNameIndicatortypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let deviceGroupsId = 'fakedata';
    let deviceGroupsDeviceId = 555;
    const deviceGroupsCreateDeviceGroupBodyParam = {
      name: 'string',
      parentId: 7
    };
    describe('#createDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceGroup(deviceGroupsCreateDeviceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.parentId);
                assert.equal(true, Array.isArray(data.response.devices));
              } else {
                runCommonAsserts(data, error);
              }
              deviceGroupsId = data.response.id;
              deviceGroupsDeviceId = data.response.id;
              saveMockData('DeviceGroups', 'createDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsFilterDeviceGroupBodyParam = {
      deviceId: 4,
      ids: [
        9
      ],
      name: 'string',
      parentIds: [
        10
      ]
    };
    describe('#filterDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterDeviceGroup(null, null, null, null, null, deviceGroupsFilterDeviceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(5, data.response.pageSize);
                assert.equal(3, data.response.totalElements);
                assert.equal(8, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'filterDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addMemberByIdToGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addMemberByIdToGroup(deviceGroupsId, deviceGroupsDeviceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'addMemberByIdToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroups(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(4, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(2, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsIds = 'fakedata';
    describe('#getAncestorsForDeviceGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAncestorsForDeviceGroups(deviceGroupsIds, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response[0]));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getAncestorsForDeviceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsUpdateDeviceGroupByIdBodyParam = {
      name: 'string',
      parentId: 5
    };
    describe('#updateDeviceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupById(deviceGroupsId, deviceGroupsUpdateDeviceGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'updateDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsPartiallyUpdateDeviceGroupByIdBodyParam = {
      name: 'string',
      parentId: 2
    };
    describe('#partiallyUpdateDeviceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateDeviceGroupById(deviceGroupsId, deviceGroupsPartiallyUpdateDeviceGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'partiallyUpdateDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupById(deviceGroupsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.parentId);
                assert.equal(true, Array.isArray(data.response.devices));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAncestorsForDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAncestorsForDeviceGroup(deviceGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response[0]));
                assert.equal(true, Array.isArray(data.response[1]));
                assert.equal(true, Array.isArray(data.response[2]));
                assert.equal(true, Array.isArray(data.response[3]));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getAncestorsForDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTagsById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTagsById(deviceGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(7, data.response.parentId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceTagsById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataDeviceId = 555;
    const metadataObjectId = 555;
    const metadataUpdateMetadataBodyParam = {};
    describe('#updateMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMetadata(metadataDeviceId, metadataObjectId, metadataUpdateMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'updateMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataPartialUpdateMetadataBodyParam = {};
    describe('#partialUpdateMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partialUpdateMetadata(metadataDeviceId, metadataObjectId, metadataPartialUpdateMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'partialUpdateMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectMetadataValue - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectMetadataValue(metadataDeviceId, metadataObjectId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.attributeId);
                assert.equal('mac', data.response.attributeType);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'getObjectMetadataValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataAttributeKey = 'fakedata';
    const metadataUpdateMetadataByAttributeBodyParam = {
      attributeId: 5,
      attributeType: 'ip',
      values: [
        {}
      ]
    };
    describe('#updateMetadataByAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMetadataByAttribute(metadataDeviceId, metadataObjectId, metadataAttributeKey, metadataUpdateMetadataByAttributeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'updateMetadataByAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataEntityType = 'fakedata';
    const metadataEntityId = 555;
    const metadataPutApiV2EntityTypeEntityIdMetadataBodyParam = {};
    describe('#putApiV2EntityTypeEntityIdMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putApiV2EntityTypeEntityIdMetadata(metadataEntityType, metadataEntityId, metadataPutApiV2EntityTypeEntityIdMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'putApiV2EntityTypeEntityIdMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataPatchApiV2EntityTypeEntityIdMetadataBodyParam = {};
    describe('#patchApiV2EntityTypeEntityIdMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchApiV2EntityTypeEntityIdMetadata(metadataEntityType, metadataEntityId, metadataPatchApiV2EntityTypeEntityIdMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'patchApiV2EntityTypeEntityIdMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetadataValue - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetadataValue(metadataEntityType, metadataEntityId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.attributeId);
                assert.equal('regex', data.response.attributeType);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'getMetadataValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataPutApiV2EntityTypeEntityIdMetadataAttributeKeyBodyParam = {
      attributeId: 4,
      attributeType: 'mac',
      values: [
        {}
      ]
    };
    describe('#putApiV2EntityTypeEntityIdMetadataAttributeKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putApiV2EntityTypeEntityIdMetadataAttributeKey(metadataEntityType, metadataEntityId, metadataAttributeKey, metadataPutApiV2EntityTypeEntityIdMetadataAttributeKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'putApiV2EntityTypeEntityIdMetadataAttributeKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const backgroundTasksEndpoint = 'fakedata';
    const backgroundTasksMethod = 'fakedata';
    let backgroundTasksTaskId = 'fakedata';
    describe('#createBackgroundTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createBackgroundTask(backgroundTasksEndpoint, backgroundTasksMethod, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
                assert.equal(false, data.response.returned);
                assert.equal('PENDING', data.response.status);
                assert.equal('string', data.response.taskId);
                assert.equal(10, data.response.timeElapsed);
              } else {
                runCommonAsserts(data, error);
              }
              backgroundTasksTaskId = data.response.taskId;
              saveMockData('BackgroundTasks', 'createBackgroundTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackgroundTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBackgroundTasks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackgroundTasks', 'getBackgroundTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBackgroundTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBackgroundTask(backgroundTasksTaskId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
                assert.equal(true, data.response.returned);
                assert.equal('PENDING', data.response.status);
                assert.equal('string', data.response.taskId);
                assert.equal(7, data.response.timeElapsed);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BackgroundTasks', 'getBackgroundTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsId = 555;
    const reportAttachmentsAlertsCreateAlertAttachmentBodyParam = {
      name: 'string',
      resources: [
        {}
      ]
    };
    describe('#createAlertAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAlertAttachment(reportAttachmentsAlertsId, reportAttachmentsAlertsCreateAlertAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('DeviceGroup', data.response.aggregation);
                assert.equal('object', typeof data.response.filters);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.resources));
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'createAlertAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentFilterSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentFilterSchema((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.filters);
                assert.equal('object', typeof data.response.operations);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentFilterSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachment(reportAttachmentsAlertsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('None', data.response.aggregation);
                assert.equal('object', typeof data.response.filters);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.resources));
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentAggregationBodyParam = {
      aggregation: 'ObjectGroup'
    };
    describe('#updateAlertAttachmentAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentAggregation(reportAttachmentsAlertsId, reportAttachmentsAlertsUpdateAlertAttachmentAggregationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentAggregation(reportAttachmentsAlertsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('DeviceGroup', data.response.aggregation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentFiltersBodyParam = {
      empty: true,
      expression: 'string',
      filters: [
        {
          id: 'string',
          name: 'string',
          operation: 'string',
          values: [
            'string'
          ]
        }
      ]
    };
    describe('#updateAlertAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentFilters(reportAttachmentsAlertsId, reportAttachmentsAlertsUpdateAlertAttachmentFiltersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentFilters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentFilters(reportAttachmentsAlertsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.empty);
                assert.equal('string', data.response.expression);
                assert.equal(true, Array.isArray(data.response.filters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentFilters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentResourceBodyParam = [
      {
        ids: [
          {},
          {},
          {},
          {},
          {},
          {}
        ],
        type: 'ObjectGroup'
      }
    ];
    describe('#updateAlertAttachmentResource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentResource(reportAttachmentsAlertsId, reportAttachmentsAlertsUpdateAlertAttachmentResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentResource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentResource(reportAttachmentsAlertsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentSettingsBodyParam = {
      alert: {
        defaultView: 'groups',
        eventSummary: false,
        inlcudeMaintenanceWindowAlerts: true
      },
      resultLimit: {
        resultLimit: 4
      },
      sourceFields: {
        fields: [
          'string'
        ],
        sort: [
          [
            'string'
          ]
        ]
      }
    };
    describe('#updateAlertAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentSettings(reportAttachmentsAlertsId, reportAttachmentsAlertsUpdateAlertAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentSettings(reportAttachmentsAlertsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.alert);
                assert.equal('object', typeof data.response.resultLimit);
                assert.equal('object', typeof data.response.sourceFields);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentTimeSettingsBodyParam = {
      ranges: [
        {
          end: 'string',
          options: {},
          start: 'string',
          type: 'SpecificInterval'
        }
      ],
      timezone: 'string'
    };
    describe('#updateAlertAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentTimeSettings(reportAttachmentsAlertsId, reportAttachmentsAlertsUpdateAlertAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentTimeSettings(reportAttachmentsAlertsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ranges));
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsUpdateAlertAttachmentVisualizationSettingsBodyParam = {
      bar: {
        drawGraphOutline: false,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: true,
        showAggregation: true,
        showAverage: false,
        showFrequency: false,
        showLast: true,
        showLegend: true,
        showPeak: false,
        showTimespan: false,
        showTitle: true,
        showValley: false
      },
      pie: {
        showAggregation: true,
        showAverage: false,
        showFrequency: true,
        showLast: true,
        showLegend: true,
        showPeak: true,
        showTimespan: true,
        showTitle: false,
        showValley: true
      },
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: false,
          precision: 2
        },
        table: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#updateAlertAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlertAttachmentVisualizationSettings(reportAttachmentsAlertsId, reportAttachmentsAlertsUpdateAlertAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'updateAlertAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsAlertsPartiallyUpdateAlertAttachmentVisualizationSettingsBodyParam = {
      bar: {
        drawGraphOutline: false,
        prettyXAxis: true,
        prettyYAxis: false,
        scaleToMinimumValue: true,
        scaled: false,
        showAggregation: false,
        showAverage: false,
        showFrequency: true,
        showLast: false,
        showLegend: false,
        showPeak: true,
        showTimespan: true,
        showTitle: true,
        showValley: false
      },
      pie: {
        showAggregation: false,
        showAverage: false,
        showFrequency: true,
        showLast: false,
        showLegend: true,
        showPeak: true,
        showTimespan: false,
        showTitle: true,
        showValley: false
      },
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: true,
          precision: 3
        },
        table: {
          blockVis: true,
          hideHeaders: false,
          wrapCells: false
        }
      }
    };
    describe('#partiallyUpdateAlertAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateAlertAttachmentVisualizationSettings(reportAttachmentsAlertsId, reportAttachmentsAlertsPartiallyUpdateAlertAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'partiallyUpdateAlertAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlertAttachmentVisualizationSettings(reportAttachmentsAlertsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.bar);
                assert.equal('object', typeof data.response.pie);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsAlerts', 'getAlertAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let pluginsPluginId = 'fakedata';
    let pluginsId = 'fakedata';
    const pluginsCreatePluginIndicatorTypeBodyParam = {
      allowMaximumValue: false,
      dataUnits: 'string',
      description: 'string',
      displayUnits: 'string',
      extendedInfo: {},
      format: 'COUNTER32',
      id: 9,
      isDefault: true,
      isEnabled: false,
      name: 'string',
      pluginObjectTypeId: 2,
      syntheticExpression: 'string',
      syntheticMaximumExpression: 'string'
    };
    describe('#createPluginIndicatorType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPluginIndicatorType(pluginsCreatePluginIndicatorTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.allowMaximumValue);
                assert.equal('string', data.response.dataUnits);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.displayUnits);
                assert.equal('object', typeof data.response.extendedInfo);
                assert.equal('COUNTER32', data.response.format);
                assert.equal(7, data.response.id);
                assert.equal(true, data.response.isDefault);
                assert.equal(true, data.response.isEnabled);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.pluginId);
                assert.equal(1, data.response.pluginObjectTypeId);
                assert.equal('string', data.response.syntheticExpression);
                assert.equal('string', data.response.syntheticMaximumExpression);
              } else {
                runCommonAsserts(data, error);
              }
              pluginsPluginId = data.response.pluginId;
              pluginsId = data.response.id;
              saveMockData('Plugins', 'createPluginIndicatorType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsFilterPluginIndicatorTypesBodyParam = {
      allowMaximumValue: false,
      dataUnits: 'string',
      description: 'string',
      displayUnits: 'string',
      format: 'GAUGE',
      id: 8,
      ids: [
        2
      ],
      isDefault: true,
      isEnabled: true,
      name: 'string',
      pluginId: 1,
      pluginObjectTypeId: 2,
      syntheticExpression: 'string',
      syntheticMaximumExpression: 'string'
    };
    describe('#filterPluginIndicatorTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterPluginIndicatorTypes(null, null, null, null, null, null, pluginsFilterPluginIndicatorTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(8, data.response.pageNumber);
                assert.equal(9, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'filterPluginIndicatorTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsCreatePluginObjectTypeBodyParam = {
      extendedInfo: {},
      isEnabled: true,
      name: 'string',
      parentObjectTypeId: 3,
      pluginId: 3
    };
    describe('#createPluginObjectType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPluginObjectType(pluginsCreatePluginObjectTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.extendedInfo);
                assert.equal(9, data.response.id);
                assert.equal(false, data.response.isEditable);
                assert.equal(false, data.response.isEnabled);
                assert.equal('string', data.response.name);
                assert.equal(3, data.response.parentObjectTypeId);
                assert.equal(10, data.response.pluginId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'createPluginObjectType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsFilterPluginObjectTypesBodyParam = {
      id: 6,
      ids: [
        10
      ],
      isEditable: false,
      isEnabled: false,
      name: 'string',
      parentObjectTypeId: 8,
      pluginId: 4
    };
    describe('#filterPluginObjectTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterPluginObjectTypes(null, null, null, null, null, null, pluginsFilterPluginObjectTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(9, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(10, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'filterPluginObjectTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPlugins - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPlugins(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(1, data.response.pageSize);
                assert.equal(7, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getAllPlugins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicePluginInfoSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDevicePluginInfoSchema((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getDevicePluginInfoSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIndicatorExtendedInfoSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIndicatorExtendedInfoSchema(pluginsPluginId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getIndicatorExtendedInfoSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPluginIndicatorTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPluginIndicatorTypes(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(3, data.response.pageSize);
                assert.equal(3, data.response.totalElements);
                assert.equal(9, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getAllPluginIndicatorTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchemaForAllPluginIndicatorTypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSchemaForAllPluginIndicatorTypes(pluginsPluginId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getSchemaForAllPluginIndicatorTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsUpdatePluginIndicatorTypeBodyParam = {
      allowMaximumValue: true,
      dataUnits: 'string',
      description: 'string',
      displayUnits: 'string',
      extendedInfo: {},
      format: 'COUNTER32',
      id: 5,
      isDefault: true,
      isEnabled: false,
      name: 'string',
      pluginObjectTypeId: 2,
      syntheticExpression: 'string',
      syntheticMaximumExpression: 'string'
    };
    describe('#updatePluginIndicatorType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePluginIndicatorType(pluginsId, pluginsUpdatePluginIndicatorTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'updatePluginIndicatorType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectExtendedInfoSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getObjectExtendedInfoSchema(pluginsPluginId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getObjectExtendedInfoSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPluginObjectTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPluginObjectTypes(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(6, data.response.pageSize);
                assert.equal(8, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getAllPluginObjectTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchemaForAllPluginObjectTypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSchemaForAllPluginObjectTypes(pluginsPluginId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getSchemaForAllPluginObjectTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pluginsUpdatePluginObjectTypeBodyParam = {
      extendedInfo: {},
      isEnabled: true,
      name: 'string',
      parentObjectTypeId: 9,
      pluginId: 5
    };
    describe('#updatePluginObjectType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePluginObjectType(pluginsId, pluginsUpdatePluginObjectTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'updatePluginObjectType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsCreateAlertBodyParam = {
      alertFlowFalcon: {
        alertId: 4,
        directionId: 6,
        filterId: 1,
        id: 10,
        interfaceId: 8,
        netflowDeviceId: 6,
        viewId: 8
      },
      clearTime: 1,
      deviceId: 7,
      endTime: 8,
      id: 8,
      ignoreComment: 'string',
      ignoreUid: 9,
      ignoreUntil: 8,
      message: 'string',
      number: 1,
      objectId: 7,
      origin: 'system',
      pluginName: 'string',
      pollId: 10,
      severity: 9,
      startTime: 1,
      thresholdId: 4
    };
    describe('#createAlert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAlert(alertsCreateAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'createAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsFilterAlertsBodyParam = {
      acknowledgedBy: 'string',
      assignedTo: [
        4
      ],
      clearMessage: 'string',
      clearTime: 7,
      closed: 1,
      closedKey: 10,
      comments: 'string',
      deviceId: [
        6
      ],
      endTime: 3,
      id: 7,
      ids: [
        7
      ],
      ignoreComment: 'string',
      ignoreUid: 7,
      ignoreUntil: 3,
      indicatorId: [
        7
      ],
      isMaintenanceAlert: false,
      lastProcessed: 3,
      message: 'string',
      number: 6,
      objectId: [
        9
      ],
      origin: 'flow',
      pluginName: 'string',
      pollId: [
        7
      ],
      severity: 10,
      startTime: 6,
      thresholdId: [
        3
      ],
      timespanBetween: {
        startTime: 1,
        endTime: 4
      }
    };
    describe('#filterAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterAlerts(null, null, null, null, null, alertsFilterAlertsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(7, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'filterAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let alertsId = 'fakedata';
    const alertsCreateAlertForcedBodyParam = {
      alertFlowFalcon: {
        alertId: 5,
        directionId: 9,
        filterId: 4,
        id: 10,
        interfaceId: 2,
        netflowDeviceId: 8,
        viewId: 1
      },
      clearTime: 4,
      deviceId: 8,
      endTime: 1,
      id: 6,
      ignoreComment: 'string',
      ignoreUid: 9,
      ignoreUntil: 7,
      message: 'string',
      number: 3,
      objectId: 7,
      origin: 'system',
      pluginName: 'string',
      pollId: 9,
      severity: 10,
      startTime: 5,
      thresholdId: 5
    };
    describe('#createAlertForced - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAlertForced(alertsCreateAlertForcedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.alertFlowFalcon);
                assert.equal(6, data.response.clearTime);
                assert.equal(1, data.response.deviceId);
                assert.equal(4, data.response.endTime);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.ignoreComment);
                assert.equal(9, data.response.ignoreUid);
                assert.equal(3, data.response.ignoreUntil);
                assert.equal('string', data.response.message);
                assert.equal(9, data.response.number);
                assert.equal(1, data.response.objectId);
                assert.equal('flow', data.response.origin);
                assert.equal('string', data.response.pluginName);
                assert.equal(8, data.response.pollId);
                assert.equal(6, data.response.severity);
                assert.equal(1, data.response.startTime);
                assert.equal(2, data.response.thresholdId);
              } else {
                runCommonAsserts(data, error);
              }
              alertsId = data.response.id;
              saveMockData('Alerts', 'createAlertForced', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsGetMaxSeverityAlertForObjectsBodyParam = [
      {
        deviceId: 4,
        objectId: 7
      }
    ];
    describe('#getMaxSeverityAlertForObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMaxSeverityAlertForObjects(alertsGetMaxSeverityAlertForObjectsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getMaxSeverityAlertForObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAlerts(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(2, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(10, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAllAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAlerts(null, alertsId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alerts));
                assert.equal(true, data.response.allowDelete);
                assert.equal('string', data.response.alternateName);
                assert.equal(2, data.response.dateAdded);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.disableConcurrentPolling);
                assert.equal(false, data.response.disablePolling);
                assert.equal(true, data.response.disableThresholding);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.ipAddress);
                assert.equal(true, data.response.isDeleted);
                assert.equal(false, data.response.isNew);
                assert.equal(8, data.response.lastDiscovery);
                assert.equal(false, data.response.manualIP);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.numElements);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(7, data.response.peerId);
                assert.equal('object', typeof data.response.pluginInfo);
                assert.equal(10, data.response.pluginManagerId);
                assert.equal(8, data.response.pollFrequency);
                assert.equal('string', data.response.timezone);
                assert.equal(5, data.response.workhoursGroupId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getDeviceAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowFalconDeviceAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowFalconDeviceAlerts(alertsId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alerts));
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.originIp);
                assert.equal(2, data.response.overrideName);
                assert.equal(10, data.response.peer);
                assert.equal('string', data.response.systemName);
                assert.equal('string', data.response.versions);
                assert.equal(5, data.response.visible);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getFlowFalconDeviceAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsUpdateAlertBodyParam = {
      acknowledgedBy: 'string',
      alertFlowFalcon: {
        alertId: 2,
        directionId: 3,
        filterId: 7,
        id: 5,
        interfaceId: 10,
        netflowDeviceId: 3,
        viewId: 6
      },
      assignedTo: 6,
      clearMessage: 'string',
      clearTime: 5,
      closed: 4,
      closedKey: 1,
      comments: 'string',
      deviceId: 10,
      endTime: 7,
      id: 1,
      ignoreComment: 'string',
      ignoreUid: 7,
      ignoreUntil: 9,
      indicatorId: 8,
      isMaintenanceAlert: false,
      lastProcessed: 9,
      message: 'string',
      number: 3,
      objectId: 4,
      origin: 'trap',
      pluginName: 'string',
      pollId: 3,
      severity: 3,
      startTime: 2,
      thresholdId: 8
    };
    describe('#updateAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlert(alertsId, alertsUpdateAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'updateAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsPatchAlertBodyParam = {
      acknowledgedBy: 'string',
      alertFlowFalcon: {
        alertId: 10,
        directionId: 4,
        filterId: 7,
        id: 1,
        interfaceId: 5,
        netflowDeviceId: 4,
        viewId: 10
      },
      assignedTo: 6,
      clearMessage: 'string',
      clearTime: 2,
      closed: 5,
      closedKey: 4,
      comments: 'string',
      deviceId: 7,
      endTime: 9,
      id: 7,
      ignoreComment: 'string',
      ignoreUid: 4,
      ignoreUntil: 3,
      indicatorId: 3,
      isMaintenanceAlert: true,
      lastProcessed: 5,
      message: 'string',
      number: 9,
      objectId: 10,
      origin: 'flow',
      pluginName: 'string',
      pollId: 6,
      severity: 7,
      startTime: 5,
      thresholdId: 1
    };
    describe('#patchAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchAlert(alertsId, alertsPatchAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'patchAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlert(alertsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.acknowledgedBy);
                assert.equal('object', typeof data.response.alertFlowFalcon);
                assert.equal(9, data.response.assignedTo);
                assert.equal('string', data.response.clearMessage);
                assert.equal(4, data.response.clearTime);
                assert.equal(9, data.response.closed);
                assert.equal(8, data.response.closedKey);
                assert.equal('string', data.response.comments);
                assert.equal(4, data.response.deviceId);
                assert.equal(4, data.response.endTime);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.ignoreComment);
                assert.equal(6, data.response.ignoreUid);
                assert.equal(5, data.response.ignoreUntil);
                assert.equal(8, data.response.indicatorId);
                assert.equal(false, data.response.isMaintenanceAlert);
                assert.equal(5, data.response.lastProcessed);
                assert.equal('string', data.response.message);
                assert.equal(3, data.response.number);
                assert.equal(2, data.response.objectId);
                assert.equal('flow', data.response.origin);
                assert.equal('string', data.response.pluginName);
                assert.equal(7, data.response.pollId);
                assert.equal(1, data.response.severity);
                assert.equal(2, data.response.startTime);
                assert.equal(3, data.response.thresholdId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsUsername = 'fakedata';
    describe('#assignAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.assignAlert(alertsId, alertsUsername, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'assignAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsClearAlertBodyParam = {
      message: 'string'
    };
    describe('#clearAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.clearAlert(alertsId, alertsClearAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'clearAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsIgnoreTime = 555;
    const alertsIgnoreAlertBodyParam = {
      message: 'string'
    };
    describe('#ignoreAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ignoreAlert(alertsId, alertsIgnoreTime, alertsIgnoreAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'ignoreAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimezonesByCountries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimezonesByCountries((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CountriesAndTimezones', 'getTimezonesByCountries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let policiesId = 'fakedata';
    let policiesPolicyId = 555;
    let policiesConditionId = 555;
    const policiesCreatePolicyBodyParam = {
      appendConditionMessages: true,
      clearConditions: [
        {
          aggregation: 4,
          comparison: 3,
          duration: 9,
          id: 5,
          indicatorTypeId: 6,
          isTrigger: false,
          message: 'string',
          policyId: 10,
          sigmaDirection: 7,
          type: 3,
          unit: 'string',
          value: 5
        }
      ],
      clearExpression: 'string',
      clearMessage: 'string',
      description: 'string',
      flow: {
        direction: 5,
        filterId: 6,
        id: 1,
        viewId: 2
      },
      folderId: 8,
      groupId: 8,
      groupIdList: [
        4
      ],
      id: 6,
      isDeviceGroup: false,
      isMemberOfAny: true,
      lastUpdated: 10,
      mailOnce: true,
      mailPeriod: 8,
      mailTo: 'string',
      name: 'string',
      objectSubTypeId: 5,
      objectTypeId: 9,
      pluginId: 8,
      severity: 10,
      triggerConditions: [
        {
          aggregation: 2,
          comparison: 9,
          duration: 8,
          id: 5,
          indicatorTypeId: 1,
          isTrigger: true,
          message: 'string',
          policyId: 3,
          sigmaDirection: 6,
          type: 7,
          unit: 'string',
          value: 7
        }
      ],
      triggerExpression: 'string',
      triggerMessage: 'string',
      type: 'flow',
      useCustomTraps: false,
      useDefaultTraps: true,
      useDeviceTraps: true,
      userEnabled: 2
    };
    describe('#createPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicy(policiesCreatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.appendConditionMessages);
                assert.equal(true, Array.isArray(data.response.clearConditions));
                assert.equal('string', data.response.clearExpression);
                assert.equal('string', data.response.clearMessage);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.flow);
                assert.equal(8, data.response.folderId);
                assert.equal(2, data.response.groupId);
                assert.equal(true, Array.isArray(data.response.groupIdList));
                assert.equal(2, data.response.id);
                assert.equal(false, data.response.isDeviceGroup);
                assert.equal(true, data.response.isMemberOfAny);
                assert.equal(10, data.response.lastUpdated);
                assert.equal(true, data.response.mailOnce);
                assert.equal(7, data.response.mailPeriod);
                assert.equal('string', data.response.mailTo);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.objectSubTypeId);
                assert.equal(7, data.response.objectTypeId);
                assert.equal(8, data.response.pluginId);
                assert.equal(10, data.response.severity);
                assert.equal(true, Array.isArray(data.response.triggerConditions));
                assert.equal('string', data.response.triggerExpression);
                assert.equal('string', data.response.triggerMessage);
                assert.equal('other', data.response.type);
                assert.equal(false, data.response.useCustomTraps);
                assert.equal(false, data.response.useDefaultTraps);
                assert.equal(false, data.response.useDeviceTraps);
                assert.equal(5, data.response.userEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              policiesId = data.response.id;
              policiesPolicyId = data.response.id;
              policiesConditionId = data.response.id;
              saveMockData('Policies', 'createPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesFilterPoliciesBodyParam = {
      description: 'string',
      folderId: 3,
      groupId: 10,
      ids: [
        10
      ],
      isDeviceGroup: false,
      name: 'string'
    };
    describe('#filterPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterPolicies(null, null, null, null, null, policiesFilterPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(2, data.response.pageNumber);
                assert.equal(4, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'filterPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesCreatePolicyFolderBodyParam = {
      name: 'string',
      parentId: 10
    };
    describe('#createPolicyFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicyFolder(policiesCreatePolicyFolderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.parentId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'createPolicyFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesCreatePolicyConditionBodyParam = {
      aggregation: 10,
      comparison: 5,
      duration: 4,
      id: 7,
      indicatorTypeId: 8,
      isTrigger: false,
      message: 'string',
      policyId: 6,
      sigmaDirection: 9,
      type: 7,
      unit: 'string',
      value: 7
    };
    describe('#createPolicyCondition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicyCondition(policiesPolicyId, policiesCreatePolicyConditionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.aggregation);
                assert.equal(1, data.response.comparison);
                assert.equal(3, data.response.duration);
                assert.equal(3, data.response.id);
                assert.equal(9, data.response.indicatorTypeId);
                assert.equal(false, data.response.isTrigger);
                assert.equal('string', data.response.message);
                assert.equal(6, data.response.policyId);
                assert.equal(10, data.response.sigmaDirection);
                assert.equal(2, data.response.type);
                assert.equal('string', data.response.unit);
                assert.equal(7, data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              policiesPolicyId = data.response.policyId;
              saveMockData('Policies', 'createPolicyCondition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicies(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(5, data.response.pageSize);
                assert.equal(1, data.response.totalElements);
                assert.equal(5, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyFolders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyFolders(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(5, data.response.pageNumber);
                assert.equal(3, data.response.pageSize);
                assert.equal(3, data.response.totalElements);
                assert.equal(10, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicyFolders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdatePolicyFolderBodyParam = {
      name: 'string'
    };
    describe('#updatePolicyFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicyFolder(policiesId, policiesUpdatePolicyFolderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicyFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdatePolicyBodyParam = {
      appendConditionMessages: true,
      clearConditions: [
        {
          aggregation: 1,
          comparison: 8,
          duration: 8,
          id: 2,
          indicatorTypeId: 5,
          isTrigger: true,
          message: 'string',
          policyId: 10,
          sigmaDirection: 9,
          type: 6,
          unit: 'string',
          value: 3
        }
      ],
      clearExpression: 'string',
      clearMessage: 'string',
      description: 'string',
      flow: {
        direction: 2,
        filterId: 6,
        id: 5,
        viewId: 7
      },
      folderId: 1,
      groupId: 5,
      groupIdList: [
        4
      ],
      id: 5,
      isDeviceGroup: true,
      isMemberOfAny: true,
      lastUpdated: 8,
      mailOnce: false,
      mailPeriod: 8,
      mailTo: 'string',
      name: 'string',
      objectSubTypeId: 5,
      objectTypeId: 7,
      pluginId: 5,
      severity: 4,
      triggerConditions: [
        {
          aggregation: 4,
          comparison: 5,
          duration: 7,
          id: 7,
          indicatorTypeId: 4,
          isTrigger: true,
          message: 'string',
          policyId: 3,
          sigmaDirection: 5,
          type: 5,
          unit: 'string',
          value: 8
        }
      ],
      triggerExpression: 'string',
      triggerMessage: 'string',
      type: 'flow',
      useCustomTraps: true,
      useDefaultTraps: true,
      useDeviceTraps: true,
      userEnabled: 7
    };
    describe('#updatePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicy(policiesId, policiesUpdatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicy(policiesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.appendConditionMessages);
                assert.equal(true, Array.isArray(data.response.clearConditions));
                assert.equal('string', data.response.clearExpression);
                assert.equal('string', data.response.clearMessage);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.flow);
                assert.equal(1, data.response.folderId);
                assert.equal(8, data.response.groupId);
                assert.equal(true, Array.isArray(data.response.groupIdList));
                assert.equal(5, data.response.id);
                assert.equal(false, data.response.isDeviceGroup);
                assert.equal(false, data.response.isMemberOfAny);
                assert.equal(9, data.response.lastUpdated);
                assert.equal(false, data.response.mailOnce);
                assert.equal(6, data.response.mailPeriod);
                assert.equal('string', data.response.mailTo);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.objectSubTypeId);
                assert.equal(5, data.response.objectTypeId);
                assert.equal(1, data.response.pluginId);
                assert.equal(4, data.response.severity);
                assert.equal(true, Array.isArray(data.response.triggerConditions));
                assert.equal('string', data.response.triggerExpression);
                assert.equal('string', data.response.triggerMessage);
                assert.equal('flow', data.response.type);
                assert.equal(false, data.response.useCustomTraps);
                assert.equal(true, data.response.useDefaultTraps);
                assert.equal(true, data.response.useDeviceTraps);
                assert.equal(8, data.response.userEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdatePolicyActionBodyParam = {
      mailAction: {
        mails: [
          'string'
        ],
        repeated: true,
        repeatedPeriod: 9,
        roleIds: [
          2
        ],
        userIds: [
          3
        ]
      },
      trapAction: {
        trapDestinations: [
          1
        ],
        useCustomTraps: false,
        useDefaultTraps: true,
        useDeviceTraps: false
      }
    };
    describe('#updatePolicyAction - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicyAction(policiesPolicyId, policiesUpdatePolicyActionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicyAction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActionsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActionsUsingGET(policiesPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.mailAction);
                assert.equal('object', typeof data.response.trapAction);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getActionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findAllUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findAllUsingGET(null, null, null, null, null, policiesPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(6, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'findAllUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdatePolicyConditionBodyParam = {
      aggregation: 7,
      comparison: 9,
      duration: 8,
      id: 7,
      indicatorTypeId: 4,
      isTrigger: true,
      message: 'string',
      policyId: 5,
      sigmaDirection: 5,
      type: 9,
      unit: 'string',
      value: 5
    };
    describe('#updatePolicyCondition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicyCondition(policiesPolicyId, policiesConditionId, policiesUpdatePolicyConditionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicyCondition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyConditionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyConditionById(policiesPolicyId, policiesConditionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.aggregation);
                assert.equal(10, data.response.comparison);
                assert.equal(2, data.response.duration);
                assert.equal(2, data.response.id);
                assert.equal(8, data.response.indicatorTypeId);
                assert.equal(false, data.response.isTrigger);
                assert.equal('string', data.response.message);
                assert.equal(6, data.response.policyId);
                assert.equal(9, data.response.sigmaDirection);
                assert.equal(1, data.response.type);
                assert.equal('string', data.response.unit);
                assert.equal(4, data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicyConditionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let reportsId = 'fakedata';
    const reportsCreateReportBodyParam = {
      description: 'string',
      emailTo: 'string',
      folderId: 8,
      id: 4,
      isEmailed: false,
      isFtped: false,
      isPrivate: true,
      isReadOnly: false,
      isTemporary: true,
      lastAccessed: 3,
      lastModified: 8,
      message: 'string',
      migrated: 4,
      name: 'string',
      nextRun: 2,
      numberOfColumns: 10,
      runUnit: 'days',
      runValue: 7,
      templateType: 'device',
      timezone: 'string',
      userId: 7,
      userRoleId: 6
    };
    describe('#createReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createReport(reportsCreateReportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.emailTo);
                assert.equal(1, data.response.folderId);
                assert.equal(4, data.response.id);
                assert.equal(true, data.response.isEmailed);
                assert.equal(true, data.response.isFtped);
                assert.equal(false, data.response.isPrivate);
                assert.equal(false, data.response.isReadOnly);
                assert.equal(false, data.response.isTemporary);
                assert.equal(5, data.response.lastAccessed);
                assert.equal(10, data.response.lastModified);
                assert.equal('string', data.response.message);
                assert.equal(3, data.response.migrated);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.nextRun);
                assert.equal(4, data.response.numberOfColumns);
                assert.equal('string', data.response.runUnit);
                assert.equal(5, data.response.runValue);
                assert.equal('string', data.response.templateType);
                assert.equal('string', data.response.timezone);
                assert.equal(9, data.response.userId);
                assert.equal(2, data.response.userRoleId);
              } else {
                runCommonAsserts(data, error);
              }
              reportsId = data.response.id;
              saveMockData('Reports', 'createReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsCreateReportFolderBodyParam = {
      id: 6,
      name: 'string',
      parentId: 7
    };
    describe('#createReportFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createReportFolder(reportsCreateReportFolderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(3, data.response.parentId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'createReportFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReports - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllReports(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(9, data.response.pageSize);
                assert.equal(4, data.response.totalElements);
                assert.equal(6, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getAllReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllReportFolders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllReportFolders(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(2, data.response.pageNumber);
                assert.equal(3, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(4, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getAllReportFolders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsUpdateReportFolderByIdBodyParam = {
      id: 7,
      name: 'string',
      parentId: 3
    };
    describe('#updateReportFolderById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReportFolderById(reportsId, reportsUpdateReportFolderByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'updateReportFolderById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportsUpdateReportByIdBodyParam = {
      description: 'string',
      emailTo: 'string',
      folderId: 10,
      id: 2,
      isEmailed: true,
      isFtped: true,
      isPrivate: false,
      isReadOnly: false,
      isTemporary: false,
      lastAccessed: 4,
      lastModified: 8,
      message: 'string',
      migrated: 2,
      name: 'string',
      nextRun: 3,
      numberOfColumns: 4,
      runUnit: 'weeks',
      runValue: 9,
      templateType: 'object',
      timezone: 'string',
      userId: 7,
      userRoleId: 3
    };
    describe('#updateReportById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReportById(reportsId, reportsUpdateReportByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'updateReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReport(reportsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.emailTo);
                assert.equal(9, data.response.folderId);
                assert.equal(8, data.response.id);
                assert.equal(true, data.response.isEmailed);
                assert.equal(true, data.response.isFtped);
                assert.equal(true, data.response.isPrivate);
                assert.equal(true, data.response.isReadOnly);
                assert.equal(false, data.response.isTemporary);
                assert.equal(6, data.response.lastAccessed);
                assert.equal(10, data.response.lastModified);
                assert.equal('string', data.response.message);
                assert.equal(6, data.response.migrated);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.nextRun);
                assert.equal(8, data.response.numberOfColumns);
                assert.equal('string', data.response.runUnit);
                assert.equal(1, data.response.runValue);
                assert.equal('string', data.response.templateType);
                assert.equal('string', data.response.timezone);
                assert.equal(2, data.response.userId);
                assert.equal(6, data.response.userRoleId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'getReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyId = 555;
    const reportAttachmentsTelephonyCreateTelephonyAttachmentBodyParam = {
      name: 'string'
    };
    describe('#createTelephonyAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTelephonyAttachment(reportAttachmentsTelephonyId, reportAttachmentsTelephonyCreateTelephonyAttachmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'createTelephonyAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelephonyAttachment(reportAttachmentsTelephonyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.settings);
                assert.equal('object', typeof data.response.time);
                assert.equal('object', typeof data.response.visualization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'getTelephonyAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyUpdateTelephonyAttachmentAggregationBodyParam = {
      aggregation: 'DestinationCodec'
    };
    describe('#updateTelephonyAttachmentAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTelephonyAttachmentAggregation(reportAttachmentsTelephonyId, reportAttachmentsTelephonyUpdateTelephonyAttachmentAggregationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'updateTelephonyAttachmentAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelephonyAttachmentAggregation(reportAttachmentsTelephonyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('DestinationPhone', data.response.aggregation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'getTelephonyAttachmentAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyUpdateTelephonyAttachmentSettingsBodyParam = {
      resultLimit: {
        resultLimit: 4
      },
      telephony: {
        sortBy: 'r',
        sortOrder: 'ASC'
      }
    };
    describe('#updateTelephonyAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTelephonyAttachmentSettings(reportAttachmentsTelephonyId, reportAttachmentsTelephonyUpdateTelephonyAttachmentSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'updateTelephonyAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelephonyAttachmentSettings(reportAttachmentsTelephonyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resultLimit);
                assert.equal('object', typeof data.response.telephony);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'getTelephonyAttachmentSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyUpdateTelephonyAttachmentTimeSettingsBodyParam = {
      ranges: {
        end: 'string',
        options: {},
        start: 'string',
        type: 'CustomWeek'
      },
      timezone: 'string'
    };
    describe('#updateTelephonyAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTelephonyAttachmentTimeSettings(reportAttachmentsTelephonyId, reportAttachmentsTelephonyUpdateTelephonyAttachmentTimeSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'updateTelephonyAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentTimeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelephonyAttachmentTimeSettings(reportAttachmentsTelephonyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ranges);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'getTelephonyAttachmentTimeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reportAttachmentsTelephonyUpdateTelephonyAttachmentVisualizationSettingsBodyParam = {
      table: {
        column: {
          exclude: [
            'string'
          ],
          include: [
            'string'
          ]
        },
        dataPresentation: {
          formatNumbers: true,
          precision: 9
        },
        table: {
          blockVis: true,
          hideHeaders: true,
          wrapCells: true
        }
      }
    };
    describe('#updateTelephonyAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTelephonyAttachmentVisualizationSettings(reportAttachmentsTelephonyId, reportAttachmentsTelephonyUpdateTelephonyAttachmentVisualizationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'updateTelephonyAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTelephonyAttachmentVisualizationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTelephonyAttachmentVisualizationSettings(reportAttachmentsTelephonyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.table);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachmentsTelephony', 'getTelephonyAttachmentVisualizationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let dynamicPluginId = 'fakedata';
    const dynamicPluginRegisterDynamicPluginManagerBodyParam = {
      pluginManagerName: 'string',
      pluginManagerIdentifier: 'string'
    };
    describe('#registerDynamicPluginManager - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerDynamicPluginManager(dynamicPluginRegisterDynamicPluginManagerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.identifier);
                assert.equal(3, data.response.lastSeen);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              dynamicPluginId = data.response.id;
              saveMockData('DynamicPlugin', 'registerDynamicPluginManager', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dynamicPluginRegisterDynamicPluginBodyParam = {
      fields: [
        {
          defaultEntry: 'string',
          description: 'string',
          enumList: 'string',
          maxLength: 5,
          name: 'string',
          type: 'TEXT',
          valueRange: 'string'
        }
      ],
      name: 'string',
      technology: 'string'
    };
    describe('#registerDynamicPlugin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerDynamicPlugin(dynamicPluginId, dynamicPluginRegisterDynamicPluginBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.fields));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.technology);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DynamicPlugin', 'registerDynamicPlugin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let objectGroupId = 'fakedata';
    const objectGroupCreateObjectGroupBodyParam = {
      name: 'string',
      parentId: 1
    };
    describe('#createObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createObjectGroup(objectGroupCreateObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal('string', data.response.name);
                assert.equal(6, data.response.parentId);
                assert.equal(false, data.response.readOnly);
              } else {
                runCommonAsserts(data, error);
              }
              objectGroupId = data.response.id;
              saveMockData('ObjectGroup', 'createObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupFindObjectGroupBodyParam = {
      deviceObjectId: {
        deviceId: 6,
        objectId: 9
      },
      ids: [
        5
      ],
      name: 'string',
      parentIds: [
        1
      ]
    };
    describe('#findObjectGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findObjectGroup(null, null, null, null, null, objectGroupFindObjectGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(8, data.response.pageNumber);
                assert.equal(6, data.response.pageSize);
                assert.equal(8, data.response.totalElements);
                assert.equal(7, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'findObjectGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupFilterObjectGroupMappingsBodyParam = {
      automatic: false,
      deviceIds: [
        8
      ],
      deviceObjectIds: [
        {
          deviceId: 7,
          objectId: 3
        }
      ],
      ids: [
        9
      ],
      objectGroupIds: [
        1
      ],
      pluginIds: [
        5
      ]
    };
    describe('#filterObjectGroupMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterObjectGroupMappings(null, null, null, null, null, objectGroupFilterObjectGroupMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(4, data.response.pageNumber);
                assert.equal(4, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(1, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'filterObjectGroupMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupAddObjectGroupMemberBodyParam = {
      deviceId: 7,
      objectId: 2
    };
    describe('#addObjectGroupMember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addObjectGroupMember(objectGroupId, objectGroupAddObjectGroupMemberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'addObjectGroupMember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroups(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(7, data.response.pageNumber);
                assert.equal(5, data.response.pageSize);
                assert.equal(7, data.response.totalElements);
                assert.equal(8, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'getObjectGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupIds = 'fakedata';
    describe('#getAncestors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAncestors(objectGroupIds, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response[0]));
                assert.equal(true, Array.isArray(data.response[1]));
                assert.equal(true, Array.isArray(data.response[2]));
                assert.equal(true, Array.isArray(data.response[3]));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'getAncestors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupUpdateObjectGroupByIdBodyParam = {
      name: 'string',
      parentId: 4
    };
    describe('#updateObjectGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateObjectGroupById(objectGroupId, objectGroupUpdateObjectGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'updateObjectGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupPartiallyUpdateObjectGroupByIdBodyParam = {
      name: 'string',
      parentId: 4
    };
    describe('#partiallyUpdateObjectGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateObjectGroupById(objectGroupId, objectGroupPartiallyUpdateObjectGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'partiallyUpdateObjectGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupById(objectGroupId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal(true, Array.isArray(data.response.members));
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.parentId);
                assert.equal(true, data.response.readOnly);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'getObjectGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiV2ObjectgroupsIdAncestors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiV2ObjectgroupsIdAncestors(objectGroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response[0]));
                assert.equal(true, Array.isArray(data.response[1]));
                assert.equal(true, Array.isArray(data.response[2]));
                assert.equal(true, Array.isArray(data.response[3]));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'getApiV2ObjectgroupsIdAncestors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectGroupMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectGroupMappings(objectGroupId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(7, data.response.pageNumber);
                assert.equal(3, data.response.pageSize);
                assert.equal(9, data.response.totalElements);
                assert.equal(3, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'getObjectGroupMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let devicesId = 'fakedata';
    const devicesCreateDeviceBodyParam = {
      name: 'string',
      alternateName: 'string',
      description: 'string',
      ipAddress: 'string',
      peerId: 9,
      pollFrequency: 1,
      allowDelete: false,
      disablePolling: false,
      disableConcurrentPolling: true,
      disableThresholding: true,
      timezone: 'string',
      workhoursGroupId: 9,
      objects: [
        {
          id: 5,
          name: 'string',
          alternateName: 'string',
          description: 'string',
          pluginId: 3,
          pluginObjectTypeId: 9,
          pollFrequency: 1,
          subtypeId: 1,
          extendedInfo: {},
          enabled: 'DISABLED',
          isDeleted: true,
          isEnabled: true,
          isVisible: false,
          indicators: [
            {
              id: {},
              evaluationOrder: {},
              format: {},
              isBaselining: {},
              lastInvalidationTime: {},
              maxValue: {},
              pluginIndicatorTypeId: {},
              syntheticExpression: {},
              systemMaxValue: {},
              extendedInfo: {},
              isDeleted: {},
              isEnabled: {}
            }
          ]
        }
      ],
      pluginInfo: {}
    };
    describe('#createDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDevice(devicesCreateDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.allowDelete);
                assert.equal('string', data.response.alternateName);
                assert.equal(1, data.response.dateAdded);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.disableConcurrentPolling);
                assert.equal(true, data.response.disablePolling);
                assert.equal(false, data.response.disableThresholding);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.ipAddress);
                assert.equal(true, data.response.isDeleted);
                assert.equal(false, data.response.isNew);
                assert.equal(5, data.response.lastDiscovery);
                assert.equal(true, data.response.manualIP);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.numElements);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(10, data.response.peerId);
                assert.equal('object', typeof data.response.pluginInfo);
                assert.equal(7, data.response.pluginManagerId);
                assert.equal(10, data.response.pollFrequency);
                assert.equal('string', data.response.timezone);
                assert.equal(3, data.response.workhoursGroupId);
              } else {
                runCommonAsserts(data, error);
              }
              devicesId = data.response.id;
              saveMockData('Devices', 'createDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCreateDeviceDataBodyParam = {
      name: 'string',
      type: 'string',
      oldTs: 6,
      newTs: 8
    };
    describe('#createDeviceData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDeviceData(devicesCreateDeviceDataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'createDeviceData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesFilterDeviceBodyParam = {
      allowDelete: true,
      alternateName: 'string',
      deleted: true,
      description: 'string',
      disableConcurrentPolling: true,
      disablePolling: true,
      disableThresholding: true,
      ids: [
        3
      ],
      inDeviceGroupIds: [
        10
      ],
      ipAddress: 'string',
      metadata: {},
      name: 'string',
      new: false,
      notInDeviceGroupIds: [
        8
      ],
      numElements: 4,
      peerIds: [
        10
      ],
      pluginManagerId: 10,
      pollFrequency: 1,
      snmpVersion: 6,
      timezone: 'string',
      workhoursGroupId: 8
    };
    describe('#filterDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterDevice(null, null, null, null, null, null, null, null, null, devicesFilterDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(10, data.response.pageNumber);
                assert.equal(7, data.response.pageSize);
                assert.equal(10, data.response.totalElements);
                assert.equal(2, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'filterDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyDeviceRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applyDeviceRules(devicesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(4, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'applyDeviceRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevices(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(8, data.response.pageNumber);
                assert.equal(2, data.response.pageSize);
                assert.equal(7, data.response.totalElements);
                assert.equal(2, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesUpdateDeviceByIdBodyParam = {
      name: 'string',
      alternateName: 'string',
      description: 'string',
      ipAddress: 'string',
      pollFrequency: 6,
      allowDelete: true,
      disablePolling: false,
      disableConcurrentPolling: false,
      disableThresholding: false,
      timezone: 'string',
      workhoursGroupId: 8,
      objects: [
        {
          id: 3,
          name: 'string',
          alternateName: 'string',
          description: 'string',
          pluginId: 5,
          pluginObjectTypeId: 5,
          pollFrequency: 9,
          subtypeId: 6,
          extendedInfo: {},
          enabled: 'ENABLED',
          isDeleted: false,
          isEnabled: false,
          isVisible: false,
          indicators: [
            {
              id: 5,
              evaluationOrder: 4,
              format: 'COUNTER64',
              isBaselining: false,
              lastInvalidationTime: 9,
              maxValue: 2,
              pluginIndicatorTypeId: 1,
              syntheticExpression: 'string',
              systemMaxValue: 4,
              extendedInfo: {},
              isDeleted: false,
              isEnabled: true
            }
          ]
        }
      ],
      pluginInfo: {}
    };
    describe('#updateDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceById(devicesId, devicesUpdateDeviceByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'updateDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesPartiallyUpdateDeviceByIdBodyParam = {
      name: 'string',
      alternateName: 'string',
      description: 'string',
      ipAddress: 'string',
      pollFrequency: 3,
      allowDelete: true,
      disablePolling: true,
      disableConcurrentPolling: false,
      disableThresholding: true,
      timezone: 'string',
      workhoursGroupId: 7,
      objects: [
        {
          id: 2,
          name: 'string',
          alternateName: 'string',
          description: 'string',
          pluginId: 1,
          pluginObjectTypeId: 1,
          pollFrequency: 10,
          subtypeId: 3,
          extendedInfo: {},
          enabled: 'ENABLED',
          isDeleted: true,
          isEnabled: false,
          isVisible: true,
          indicators: [
            {
              id: 7,
              evaluationOrder: 3,
              format: 'GAUGE',
              isBaselining: true,
              lastInvalidationTime: 3,
              maxValue: 6,
              pluginIndicatorTypeId: 3,
              syntheticExpression: 'string',
              systemMaxValue: 2,
              extendedInfo: {},
              isDeleted: true,
              isEnabled: true
            }
          ]
        }
      ],
      pluginInfo: {}
    };
    describe('#partiallyUpdateDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.partiallyUpdateDeviceById(devicesId, devicesPartiallyUpdateDeviceByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'partiallyUpdateDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceById(devicesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.allowDelete);
                assert.equal('string', data.response.alternateName);
                assert.equal(9, data.response.dateAdded);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.disableConcurrentPolling);
                assert.equal(false, data.response.disablePolling);
                assert.equal(false, data.response.disableThresholding);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.ipAddress);
                assert.equal(true, data.response.isDeleted);
                assert.equal(false, data.response.isNew);
                assert.equal(7, data.response.lastDiscovery);
                assert.equal(true, data.response.manualIP);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.numElements);
                assert.equal(true, Array.isArray(data.response.objects));
                assert.equal(10, data.response.peerId);
                assert.equal('object', typeof data.response.pluginInfo);
                assert.equal(6, data.response.pluginManagerId);
                assert.equal(3, data.response.pollFrequency);
                assert.equal('string', data.response.timezone);
                assert.equal(5, data.response.workhoursGroupId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'getDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMapImages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMapImages(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(6, data.response.pageNumber);
                assert.equal(8, data.response.pageSize);
                assert.equal(5, data.response.totalElements);
                assert.equal(2, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMapImages', 'getMapImages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusMapImagesId = 555;
    describe('#getMapImageById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMapImageById(statusMapImagesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMapImages', 'getMapImageById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationPostLoggingLevelBodyParam = {
      level: 'WARN',
      logger: 'string'
    };
    describe('#postLoggingLevel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postLoggingLevel(applicationPostLoggingLevelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(5, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'postLoggingLevel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPublicMetrics - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPublicMetrics(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getPublicMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVersion((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.buildTime);
                assert.equal('string', data.response.buildVersion);
                assert.equal('string', data.response.commitIdAbbrev);
                assert.equal('string', data.response.commitTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'getVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let deviceGroupRulesId = 'fakedata';
    const deviceGroupRulesCreateDeviceGroupRuleBodyParam = {
      attributeId: 10,
      descriptionExpression: 'string',
      groupId: 5,
      id: 3,
      metadataValueExpression: 'string',
      mgtIpExpression: 'string',
      nameExpression: 'string',
      namespaceId: 3,
      sysContactExpression: 'string',
      sysDescrExpression: 'string',
      sysLocationExpression: 'string',
      sysNameExpression: 'string',
      sysObjectIdExpression: 'string',
      walkCheckOid: 'string'
    };
    describe('#createDeviceGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceGroupRule(deviceGroupRulesCreateDeviceGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.attributeId);
                assert.equal('string', data.response.descriptionExpression);
                assert.equal(1, data.response.groupId);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.metadataValueExpression);
                assert.equal('string', data.response.mgtIpExpression);
                assert.equal('string', data.response.nameExpression);
                assert.equal(8, data.response.namespaceId);
                assert.equal('string', data.response.sysContactExpression);
                assert.equal('string', data.response.sysDescrExpression);
                assert.equal('string', data.response.sysLocationExpression);
                assert.equal('string', data.response.sysNameExpression);
                assert.equal('string', data.response.sysObjectIdExpression);
                assert.equal('string', data.response.walkCheckOid);
              } else {
                runCommonAsserts(data, error);
              }
              deviceGroupRulesId = data.response.id;
              saveMockData('DeviceGroupRules', 'createDeviceGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyDeviceGroupRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applyDeviceGroupRules((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(5, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'applyDeviceGroupRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyDeviceGroupRulesByGroupId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applyDeviceGroupRulesByGroupId(deviceGroupRulesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal(100, data.response.statusCode);
                assert.equal(6, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'applyDeviceGroupRulesByGroupId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupsRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupsRules(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(9, data.response.pageNumber);
                assert.equal(4, data.response.pageSize);
                assert.equal(10, data.response.totalElements);
                assert.equal(5, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'getDeviceGroupsRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupRulesUpdateDeviceGroupRuleBodyParam = {
      attributeId: 9,
      descriptionExpression: 'string',
      groupId: 1,
      id: 6,
      metadataValueExpression: 'string',
      mgtIpExpression: 'string',
      nameExpression: 'string',
      namespaceId: 2,
      sysContactExpression: 'string',
      sysDescrExpression: 'string',
      sysLocationExpression: 'string',
      sysNameExpression: 'string',
      sysObjectIdExpression: 'string',
      walkCheckOid: 'string'
    };
    describe('#updateDeviceGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroupRule(deviceGroupRulesId, deviceGroupRulesUpdateDeviceGroupRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'updateDeviceGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupRule(deviceGroupRulesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.attributeId);
                assert.equal('string', data.response.descriptionExpression);
                assert.equal(7, data.response.groupId);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.metadataValueExpression);
                assert.equal('string', data.response.mgtIpExpression);
                assert.equal('string', data.response.nameExpression);
                assert.equal(6, data.response.namespaceId);
                assert.equal('string', data.response.sysContactExpression);
                assert.equal('string', data.response.sysDescrExpression);
                assert.equal('string', data.response.sysLocationExpression);
                assert.equal('string', data.response.sysNameExpression);
                assert.equal('string', data.response.sysObjectIdExpression);
                assert.equal('string', data.response.walkCheckOid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'getDeviceGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupRules(deviceGroupRulesId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.content));
                assert.equal(1, data.response.pageNumber);
                assert.equal(4, data.response.pageSize);
                assert.equal(6, data.response.totalElements);
                assert.equal(3, data.response.totalPages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'getDeviceGroupRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceTypeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceTypeById(deviceTypesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'deleteDeviceTypeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceTypeMemberById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceTypeMemberById(deviceTypesId, deviceTypesDeviceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceTypes', 'deleteDeviceTypeMemberById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObjectById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteObjectById(objectsDeviceId, objectsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Objects', 'deleteObjectById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKeys - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiKeys((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'deleteApiKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiKey(apiKeysApiKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'deleteApiKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKeysForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiKeysForUser(apiKeysId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'deleteApiKeysForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiKeyForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApiKeyForUser(apiKeysApiKey, apiKeysId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiKeys', 'deleteApiKeyForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopNViewById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTopNViewById(topNViewsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TopNViews', 'deleteTopNViewById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRoleById(rolesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'deleteRoleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkHoursGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWorkHoursGroupById(workHoursId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkHours', 'deleteWorkHoursGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteThresholdById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteThresholdById(thresholdsDeviceId, thresholdsThresholdId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'deleteThresholdById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteThresholdConditionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteThresholdConditionById(thresholdsDeviceId, thresholdsThresholdId, thresholdsTriggerType, thresholdsConditionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Thresholds', 'deleteThresholdConditionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteReportAttachment(reportAttachmentsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ReportAttachments', 'deleteReportAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObjectGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteObjectGroupRule(objectGroupRulesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroupRules', 'deleteObjectGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteUserUsingDELETE(usersId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUserUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersRemoveSpecificUserRolesBodyParam = [
      5
    ];
    describe('#removeSpecificUserRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.removeSpecificUserRoles(usersId, usersRemoveSpecificUserRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'removeSpecificUserRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNamespaceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteNamespaceById(metadataNamespaceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataNamespace', 'deleteNamespaceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStatusMapById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteStatusMapById(statusMapsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'deleteStatusMapById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConnectionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteConnectionById(statusMapsId, statusMapsConnectionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'deleteConnectionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteNodeById(statusMapsId, statusMapsNodeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StatusMaps', 'deleteNodeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceMappingById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceMappingById(netFlowMappingId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'deleteDeviceMappingById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteInterface(netFlowNetflowDeviceId, netFlowNetFlowInterfaceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'deleteInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFilter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFilter(netFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'deleteFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFilterEntity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFilterEntity(netFlowId, netFlowRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'deleteFilterEntity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObjectMappingById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteObjectMappingById(netFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'deleteObjectMappingById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletSubnetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletSubnetById(netFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'deletSubnetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletSubnetCategoryById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletSubnetCategoryById(netFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'deletSubnetCategoryById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteApplication(netFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'deleteApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMaintenanceWindowById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteMaintenanceWindowById(maintenanceWindowsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceWindows', 'deleteMaintenanceWindowById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttributeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAttributeById(metadataAttributeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MetadataAttribute', 'deleteAttributeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConstraintById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteConstraintById(topologyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteConstraintById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyFullLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTopologyFullLink(topologyConnectionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyFullLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyHalfLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTopologyHalfLink(topologyConnectionId, topologyDeviceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyHalfLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signOut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.signOut((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'signOut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#signOutOthers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.signOutOthers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'signOutOthers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationUserId = 555;
    describe('#signOutUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.signOutUser(authenticationUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'signOutUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceGroupById(deviceGroupsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'deleteDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroupMemberById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceGroupMemberById(deviceGroupsId, deviceGroupsDeviceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'deleteDeviceGroupMemberById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlertById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAlertById(alertsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'deleteAlertById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyFolderById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePolicyFolderById(policiesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicyFolderById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePolicyById(policiesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicyById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyActionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePolicyActionById(policiesPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicyActionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyConditionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePolicyConditionById(policiesPolicyId, policiesConditionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicyConditionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportFolderById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteReportFolderById(reportsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'deleteReportFolderById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReportById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteReportById(reportsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reports', 'deleteReportById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObjectGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteObjectGroupById(objectGroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'deleteObjectGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const objectGroupRemoveObjectGroupMemberBodyParam = {
      deviceId: 6,
      objectId: 7
    };
    describe('#removeObjectGroupMember - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeObjectGroupMember(objectGroupId, objectGroupRemoveObjectGroupMemberBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-sevone_v2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectGroup', 'removeObjectGroupMember', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceById(devicesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'deleteDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroupRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceGroupRule(deviceGroupRulesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroupRules', 'deleteDeviceGroupRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
